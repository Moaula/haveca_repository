<?php 
	include_once('config/annonse-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	<!-- ====================================
	#	SEARCHING FOR USERS
	===================================== -->

	


	<!-- ====================================
	#	ADDING NEW VISNING
	===================================== -->
	


		<!-- Creating a visning for the selected user -->
		<div class="container">
			<div class="inner-container">
				<div class="medium-form form-center">

					<!-- Error outputting -->
					<?php 
						if(!empty($errors)) { 
							echo writeerrors($errors, '', 'Feil.'); 
						} 
					?>

					<!-- Flashing sessions with infromation -->
					<?php 
						if(Session::exists('annonse')) { 
							echo writealerts(Session::flash('annonse'), 'success'); 
						} 
					?>

					<div class="page-header"><h1><i class="fa fa-tv"></i> Opprett visning</h1></div>
					<div class="form-information">
						<p>
							<strong>Felt markert med <i class="fa fa-star"></i> kreves. </strong>
							<hr />
						</p>
					</div>

					<form action="" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>Visningstype: <i class="fa fa-star"></i></label>
									<select name="visning_type" class="form-control">
										<?php 
											if(!empty(IO::get('visning_type'))) { 
												echo '<option selected="selected" value="'. IO::get('visning_type') .'">'. IO::get('visning_type') .'</option>'; 
											} else {
												echo '<option selected="selected" value="">Velg</option>'; 
											}
										?>
										
										<option value="sale">Salg - Målet er å selge filemt objekt</option>
										<option value="show">Visning - Målet er å vise filmet objekt</option>
									</select>
								</div>
							</div>


							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label>Overskrift: <i class="fa fa-star"></i></label>
									<input type="text" name="title" placeholder="Skriv inn en overskrift for visningen" class="form-control" value="<?php echo IO::get('title');  ?>" />
								</div>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label>Beskrivelse: <i class="fa fa-star"></i></label>
										<div class="bbcode-controls">
										<ul>
											<li><button onclick="settag('b', 'description')" type="button" class="btn bbcode-btn btn-default">B</button></li>
											<li><button onclick="settag('u', 'description')" type="button" class="btn bbcode-btn btn-default">U</button></li>
											<li><button onclick="settag('i', 'description')" type="button" class="btn bbcode-btn btn-default"><em>i</em></button></li>
											<li><button onclick="settag('url', 'description')" type="button" class="btn bbcode-btn btn-default"><i class="fa fa-link"></i> </button></li>
										</ul>
									</div>
									<textarea name="description" id="description" rows="6" placeholder="Skriv in noe mer om visningen." class="form-control"><?php echo IO::get('description');  ?></textarea>
								</div>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>Link til video: <i class="fa fa-star"></i></label>
									<input type="text" name="video" placeholder="Kopier og lim inn video fra youtube" class="form-control" value="<?php echo IO::get('video');  ?>" />
								</div>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>Adresse: <i class="fa fa-star"></i></label>
									<input type="text" name="address" placeholder="Kinogata 19. 2815 Gjøvik." class="form-control" value="<?php echo IO::get('address');  ?>" />
								</div>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>Primært bilde: <i class="fa fa-star"></i></label>
									<input type="file" name="image" accept="image/*" />
								</div>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>Tileggsbilder: <i class="fa fa-star"></i></label>
									<input type="file" name="images[]" multiple accept="image/*" />
								</div>
							</div>
						</div>

						<div class="form-information">
							<p>
								<hr />
									<strong>Fivillige tilleggs felt &nbsp; <i class="fa fa-arrow-down"></i> </strong>
								<hr />
							</p>
						</div>

						<div class="row">
							
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Boligtype:</label>
									<select name="buildingtype" class="form-control">
										<option selected="selected" value="">&dArr; Boligtype &dArr;</option>
										<option value="">Ikke relevant</option>
										<option value="enebolig">Enebolig</option>
										<option value="leilighet">Leilighet</option>
										<option value="rekkehus">Rekkehus</option>
										<option value="borettslag">Borettslag</option>
										<option value="blokk">blokk</option>
									</select>
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Eierform:</label>
									<select name="ownership" class="form-control">
										<option selected="selected" value="">&dArr; Boligtype &dArr;</option>
										<option value="">Ikke relevant</option>
										<option value="selveier">Selveier</option>
										<option value="borettslag">Borettslag</option>
										<option value="andelsleilighet">Andelsleilighet</option>
										<option value="andelsbolig">Andelsbolig</option>
										<option value="aksjebolig">Aksjebolig</option>
									</select>
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Pris:</label>
									<input type="number" name="price" value="<?php echo IO::get('price'); ?>" placeholder="Hva er prisen...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Totalpris:</label>
									<input type="number" name="totalprice" value="<?php echo IO::get('totalprice'); ?>" placeholder="Hva er total prisen...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Byggeår:</label>
									<input type="number" name="buildyear" value="<?php echo IO::get('buildyear'); ?>" placeholder="Når ble eiendomen laget...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Verditakt:</label>
									<input type="number" name="appraisedvalue" value="<?php echo IO::get('appraisedvalue'); ?>" placeholder="Verditakst...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Fellesgjeld:</label>
									<input type="number" name="commondept" value="<?php echo IO::get('commondept'); ?>" placeholder="Fellesgjeld...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Fellesformue:</label>
									<input type="number" name="commonwealth" value="<?php echo IO::get('commonwealth'); ?>" placeholder="Fellesformue...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Felleskostnader:</label>
									<input type="number" name="sharedcosts" value="<?php echo IO::get('sharedcosts'); ?>" placeholder="Felleskostnader...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Lånetakst:</label>
									<input type="number" name="loanvalue" value="<?php echo IO::get('loanvalue'); ?>" placeholder="Lånetakst...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Areal:</label>
									<input type="number" name="area" value="<?php echo IO::get('area'); ?>" placeholder="Areal...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Bruttoareal:</label>
									<input type="number" name="bruttoareal" value="<?php echo IO::get('bruttoareal'); ?>" placeholder="Bruttoareal...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Bruksareal:</label>
									<input type="number" name="bruksareal" value="<?php echo IO::get('bruksareal'); ?>" placeholder="Bruksareal...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Primærrom:</label>
									<input type="number" name="areapeimary" value="<?php echo IO::get('areapeimary'); ?>" placeholder="Primærrom...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Tomt:</label>
									<input type="number" name="plot" value="<?php echo IO::get('plot'); ?>" placeholder="Tomt...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Totalt antall rom:</label>
									<input type="number" name="roms" value="<?php echo IO::get('roms'); ?>" placeholder="Antall rom...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Soverom:</label>
									<input type="number" name="bedroms" value="<?php echo IO::get('bedroms'); ?>" placeholder="Soverom...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Bad:</label>
									<input type="number" name="bathroms" value="<?php echo IO::get('bathroms'); ?>" placeholder="Bad...." class="form-control">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group bottom-group">
									<input type="hidden" name="new_visning_token" value="<?php echo Token::create(); ?>">
									<button class="btn btn-success"> <i class="fa fa-plus"></i> &nbsp; Opprett visning </button>
									<a href="/" class="btn btn-default"> <i class="fa fa-remove"></i> &nbsp; Avbryt </a>
								</div>
							</div>
						</div>

					</form>
					
				</div>
			</div>
		</div>


	<!-- ====================================
	#	EDETING VISNING
	===================================== -->

	<?php if(IO::exists('get') && IO::get('action') == "edit") { ?>

		<!-- Creating a visning for the selected user -->
		<div class="container">
			<div class="inner-container">
				<div class="medium-form form-center">
					<!-- Error outputting -->
					<?php 
						if(!empty($errors)) { 
							echo writeerrors($errors, '', 'Feil.'); 
						} 
					?>

					<!-- Flashing sessions with infromation -->
					<?php 
						if(Session::exists('annonse')) { 
							echo writealerts(Session::flash('annonse'), 'success'); 
						} 
					?>

					<div class="page-header"><h1><i class="fa fa-tv"></i> Opprett visning</h1></div>
					<div class="form-information">
						<p>
							<strong>Felt markert med <i class="fa fa-star"></i> kreves. </strong>
							<hr />
						</p>
					</div>

					<form action="" method="post" enctype="multipart/form-data">

						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>Kunde: <i class="fa fa-star"></i> </label>
									<input type="text" name="dummy" class="form-control" disabled 
									value="<?php echo ucfirst($user_info->FirstName), ' ', 
									ucfirst($user_info->LastName), ' (', date("d/m/Y", strtotime($user_info->BirthDay)), ')'; ?>" />
									<input type="hidden" name="customer" value="<?php echo $user_info->UserID; ?>" />
								</div>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>Visningstype: <i class="fa fa-star"></i></label>
									<select name="visning_type" class="form-control">
										<?php 
											if($annonse_edit->Type) { 
												echo '<option selected="selected" value="'. $annonse_edit->Type .'">'.$annonse_edit->Type.'</option>'; 
											}
										?>
										<option value="sale">Salg - Målet er å selge filemt objekt</option>
										<option value="show">Visning - Målet er å vise filmet objekt</option>
									</select>
								</div>
							</div>


							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label>Overskrift: <i class="fa fa-star"></i></label>
									<input type="text" name="title" placeholder="Skriv inn en overskrift for visningen" class="form-control" value="<?php if(empty(IO::get('title'))) { echo $annonse_edit->Title; } else { echo IO::get('title'); }  ?>" />
								</div>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label>Beskrivelse: <i class="fa fa-star"></i></label>
										<div class="bbcode-controls">
										<ul>
											<li><button onclick="settag('b', 'description')" type="button" class="btn bbcode-btn btn-default">B</button></li>
											<li><button onclick="settag('u', 'description')" type="button" class="btn bbcode-btn btn-default">U</button></li>
											<li><button onclick="settag('i', 'description')" type="button" class="btn bbcode-btn btn-default"><em>i</em></button></li>
											<li><button onclick="settag('url', 'description')" type="button" class="btn bbcode-btn btn-default"><i class="fa fa-link"></i> </button></li>
										</ul>
									</div>
									<textarea name="description" id="description" rows="6" placeholder="Skriv in noe mer om visningen." class="form-control"><?php if(empty(IO::get('description'))) { echo $annonse_edit->Description; } else { echo IO::get('description'); }  ?></textarea>
								</div>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>Link til video: <i class="fa fa-star"></i></label>
									<input type="text" name="video" placeholder="Kopier og lim inn video fra youtube" class="form-control" value="<?php if(empty(IO::get('video'))) { echo $annonse_edit->Video; } else { echo IO::get('video'); }  ?>" />
								</div>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>Adresse: <i class="fa fa-star"></i></label>
									<input type="text" name="address" placeholder="Kinogata 19. 2815 Gjøvik." class="form-control" value="<?php if(empty(IO::get('address'))) { echo $annonse_edit->Address; } else { echo IO::get('address'); }  ?>" />
								</div>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>Primært bilde: <i class="fa fa-star"></i></label>
									<input type="file" name="image" accept="image/*" />
								</div>
								<br /><br /><br /><br />

								<label>Gjeldende primært bilde:</label>
								<div class="main-image righthand">
									<?php if(!empty($annonse_img)) { ?>
										<?php foreach ($annonse_img as $img) {  if($img->Prim == 1) { ?>
											<input type="hidden" name="old_image" value="<?php echo $img->Path; ?>" />
											<input type="hidden" name="old_image_id" value="<?php echo $img->ImageID; ?>" />
											<img src="res/uploads/visning/t_<?php echo $img->Path; ?>" class="imgitem">
										<?php } } ?>
									<?php } ?>
									<strong class="red">Velg et nytt primært bilde og oppdater visningen for å erstatte primært bilde.</strong>
									<br /><br /><br /><br />
								</div>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>Tileggsbilder: <i class="fa fa-star"></i></label>
									<input type="file" name="images[]" multiple accept="image/*" />
								</div>
								<br /><br /><br /><br />

								<label>Gjeldende tileggsbilder:</label>
								<div class="visning-images">
									<?php if(!empty($annonse_img)) { ?>
										<?php foreach ($annonse_img as $img) {  if($img->Prim != 1) { ?>
											<div class="img-holder-details imgid_<?php echo $img->ImageID; ?>">
												<img src="res/uploads/visning/t_<?php echo $img->Path; ?>" class="imgitem">
												<div class="the-button delSelImg" data-id="<?php echo $img->ImageID; ?>" data-img="<?php echo $img->Path; ?>" data-thumb="t_<?php echo $img->Path; ?>"><i class="fa fa-remove"></i> </div>
											</div>
										<?php } } ?>
									<?php } ?>
								</div>


							</div>
						</div>

						<div class="form-information">
							<p>
								<hr />
									<strong>Fivillige tilleggs felt &nbsp; <i class="fa fa-arrow-down"></i> </strong>
								<hr />
							</p>
						</div>

						<div class="row">
							
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Boligtype:</label>
									<select name="buildingtype" class="form-control">
										<option selected="selected" value="">&dArr; Boligtype &dArr;</option>
										<option value="">Ikke relevant</option>
										<option value="enebolig">Enebolig</option>
										<option value="leilighet">Leilighet</option>
										<option value="rekkehus">Rekkehus</option>
										<option value="borettslag">Borettslag</option>
										<option value="blokk">blokk</option>
									</select>
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Eierform:</label>
									<select name="ownership" class="form-control">
										<option selected="selected" value="">&dArr; Boligtype &dArr;</option>
										<option value="">Ikke relevant</option>
										<option value="selveier">Selveier</option>
										<option value="borettslag">Borettslag</option>
										<option value="andelsleilighet">Andelsleilighet</option>
										<option value="andelsbolig">Andelsbolig</option>
										<option value="aksjebolig">Aksjebolig</option>
									</select>
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Pris:</label>
									<input type="number" name="price" value="<?php if(empty(IO::get('price'))) { echo $annonse_edit->Price; } else { echo IO::get('price'); }  ?>" placeholder="Hva er prisen...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Totalpris:</label>
									<input type="number" name="totalprice" value="<?php if(empty(IO::get('totalprice'))) { echo $annonse_edit->TotalPrice; } else { echo IO::get('totalprice'); }  ?>" placeholder="Hva er total prisen...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Byggeår:</label>
									<input type="number" name="buildyear" value="<?php if(empty(IO::get('buildyear'))) { echo $annonse_edit->BuildYear; } else { echo IO::get('buildyear'); }  ?>" placeholder="Når ble eiendomen laget...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Verditakt:</label>
									<input type="number" name="appraisedvalue" value="<?php if(empty(IO::get('appraisedvalue'))) { echo $annonse_edit->AppraisedValue; } else { echo IO::get('appraisedvalue'); }  ?>" placeholder="Verditakst...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Fellesgjeld:</label>
									<input type="number" name="commondept" value="<?php if(empty(IO::get('commondept'))) { echo $annonse_edit->CommonDept; } else { echo IO::get('commondept'); }  ?>" placeholder="Fellesgjeld...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Fellesformue:</label>
									<input type="number" name="commonwealth" value="<?php if(empty(IO::get('commonwealth'))) { echo $annonse_edit->CommonWealth; } else { echo IO::get('commonwealth'); }  ?>" placeholder="Fellesformue...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Felleskostnader:</label>
									<input type="number" name="sharedcosts" value="<?php if(empty(IO::get('sharedcosts'))) { echo $annonse_edit->SharedCosts; } else { echo IO::get('sharedcosts'); }  ?>" placeholder="Felleskostnader...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Lånetakst:</label>
									<input type="number" name="loanvalue" value="<?php if(empty(IO::get('loanvalue'))) { echo $annonse_edit->LoanValue; } else { echo IO::get('loanvalue'); }  ?>" placeholder="Lånetakst...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Areal:</label>
									<input type="number" name="area" value="<?php if(empty(IO::get('area'))) { echo $annonse_edit->Area; } else { echo IO::get('area'); }  ?>" placeholder="Areal...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Bruttoareal:</label>
									<input type="number" name="bruttoareal" value="<?php if(empty(IO::get('bruttoareal'))) { echo $annonse_edit->AreaOfGross; } else { echo IO::get('bruttoareal'); }  ?>" placeholder="Bruttoareal...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Bruksareal:</label>
									<input type="number" name="bruksareal" value="<?php if(empty(IO::get('bruksareal'))) { echo $annonse_edit->AreaOfUse; } else { echo IO::get('bruksareal'); }  ?>" placeholder="Bruksareal...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Primærrom:</label>
									<input type="number" name="areapeimary" value="<?php if(empty(IO::get('areapeimary'))) { echo $annonse_edit->AreaPrimary; } else { echo IO::get('areapeimary'); }  ?>" placeholder="Primærrom...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Tomt:</label>
									<input type="number" name="plot" value="<?php if(empty(IO::get('plot'))) { echo $annonse_edit->Plot; } else { echo IO::get('plot'); }  ?>" placeholder="Tomt...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Totalt antall rom:</label>
									<input type="number" name="roms" value="<?php if(empty(IO::get('roms'))) { echo $annonse_edit->Roms; } else { echo IO::get('roms'); }  ?>" placeholder="Antall rom...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Soverom:</label>
									<input type="number" name="bedroms" value="<?php if(empty(IO::get('bedroms'))) { echo $annonse_edit->BedRoms; } else { echo IO::get('bedroms'); }  ?>" placeholder="Soverom...." class="form-control">
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="form-group">
									<label>Bad:</label>
									<input type="number" name="bathroms" value="<?php if(empty(IO::get('bathroms'))) { echo $annonse_edit->BathRoms; } else { echo IO::get('bathroms'); }  ?>" placeholder="Bad...." class="form-control">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group bottom-group">
									<input type="hidden" name="update_visning_token" value="<?php echo Token::create(); ?>" />
									<input type="hidden" name="update_id" value="<?php echo $annonse_edit->AnnonseID; ?>" />
									<button class="btn btn-success"> <i class="fa fa-plus"></i> &nbsp; Opprett visning </button>
									<a href="/" class="btn btn-default"> <i class="fa fa-remove"></i> &nbsp; Avbryt </a>
								</div>
							</div>
						</div>

					</form>

				</div>
			</div>
		</div>

	<?php } ?>



<?php include_once('includes/files/botarea.php'); ?> 