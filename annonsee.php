<?php 
	include_once('config/annonse-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>


 	
	<div class="container">
		<div class="inner-container medium-form form-center">



			<!-- Error outputting -->
			<?php 
				if(!empty($errors)) { 
					echo writeerrors($errors, '', 'Feil.'); 
				} 
			?>

			<!-- Flashing sessions with infromation -->
			<?php 
				if(Session::exists('annonse')) {
					if(IO::exists('get')) { $alert = IO::get('alert'); } else { $alert = 'success'; }
					echo writealerts(Session::flash('annonse'), $alert); 
				} 
			?>

			<?php if(IO::exists('get') && !empty(IO::get('id')) && is_numeric(IO::get('id'))) { ?>
				<div class="entry-title">Endre annonse</div>
				<form action="" method="post" enctype="multipart/form-data">

					<div class="page-header">
						<h4>Kunde</h4>
					</div>

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Annonsen er bestilt av: &nbsp; <span class="red">*</span></label>
								<select name="customer" class="form-control">
									<option selected="selected" value="<?php echo $user_info->UserID; ?>"><?php echo ucfirst($user_info->FirstName), ' ', ucfirst($user_info->LastName), ' ( '.$user_info->BirthDay.' )'; ?></option>
									<option value="">&dArr; Velg hvem annonsen er bestilt av &dArr;</option>
									<?php 
										if(!empty($users)) {
											foreach($users as $user) {
												echo '<option value="'. $user->UserID .'">'. ucfirst($user->FirstName) . ' ' . ucfirst($user->LastName) . ' ( '. date("d.m.Y", strtotime($user->BirthDay)) .' )</option>';
											}
										} 

									?>
								</select>
							</div>
						</div>
					</div>

					<div class="page-header">
						<h4>Annonse innformasjon</h4>
					</div>

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Overskrift: &nbsp; <span class="red">*</span></label>
								<input type="text" name="title" value="<?php echo $annonse_edit->Title; ?>" placeholder="Gi annonsen en overskirft" class="form-control">
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Beskrivelse / fritekst: &nbsp; <span class="red">*</span></label>
								<div class="bbcode-controls">
									<ul>
										<li><button onclick="settag('b', 'description')" type="button" class="btn bbcode-btn btn-default">B</button></li>
										<li><button onclick="settag('u', 'description')" type="button" class="btn bbcode-btn btn-default">U</button></li>
										<li><button onclick="settag('i', 'description')" type="button" class="btn bbcode-btn btn-default"><em>i</em></button></li>
										<li><button onclick="settag('url', 'description')" type="button" class="btn bbcode-btn btn-default"><i class="fa fa-link"></i> </button></li>
									</ul>
								</div>
								<textarea name="description" id="description" rows="6" placeholder="Skriv det du måtte ønske relatert til visningen." class="form-control"><?php echo $annonse_edit->Description; ?></textarea>
							</div>
						</div>

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Adresse: &nbsp; <span class="red">*</span></label>
								<input type="text" name="address" value="<?php echo $annonse_edit->Address; ?>" placeholder="Olasgate 55, 0000 Stedsnavn" class="form-control">
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Video: &nbsp; <span class="red">*</span></label>
								<input type="text" name="video" value="<?php echo $annonse_edit->Video; ?>" placeholder="Lim inn lenken til video fra YouTube" class="form-control">
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Kart: &nbsp; <span class="red">*</span></label>
								<input type="text" name="googlemaps" value="<?php echo $annonse_edit->GoogleMaps; ?>" placeholder="Olasgate 55 0000 Stedsnavn (Google Maps)" class="form-control">
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Primært bilde: &nbsp; <span class="red">*</span></label>
								<input type="file" name="image" accept="image/*" />
							</div>
							<br /><br /><br /><br />

							<label>Gjeldende hovedbilde:</label>
							<div class="main-image righthand">
								<?php if(!empty($annonse_img)) { ?>
									<?php foreach ($annonse_img as $img) {  if($img->Prim == 1) { ?>
										<input type="hidden" name="old_image" value="<?php echo $img->Path; ?>" />
										<input type="hidden" name="old_image_id" value="<?php echo $img->ImageID; ?>" />
										<img src="res/uploads/visning/t_<?php echo $img->Path; ?>" class="imgitem">
									<?php } } ?>
								<?php } ?>
								<small class="red">For å endre hovedbilde, velger du ny. Den erstattes automatisk.</small>
								<br /><br /><br /><br />
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Tileggsbilder: &nbsp; <span class="red">*</span></label>
								<input type="file" name="images[]" multiple accept="image/*" />
							</div>
							<br /><br /><br /><br />

							<label>Gjeldende tileggsbilder:</label>
							<div class="visning-images">
								<?php if(!empty($annonse_img)) { ?>
									<?php foreach ($annonse_img as $img) {  if($img->Prim != 1) { ?>
										<div class="img-holder-details imgid_<?php echo $img->ImageID; ?>">
											<img src="res/uploads/visning/t_<?php echo $img->Path; ?>" class="imgitem">
											<div class="the-button delSelImg" data-id="<?php echo $img->ImageID; ?>" data-img="<?php echo $img->Path; ?>" data-thumb="t_<?php echo $img->Path; ?>"><i class="fa fa-remove"></i> </div>
										</div>
									<?php } } ?>
								<?php } ?>
							</div>
						</div>
						
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Nøkkelord for søk: (separeres med mellomrom) &nbsp; <span class="red">*</span></label>
								<input type="text" name="keywords" value="<?php echo $annonse_edit->keywords; ?>" placeholder="olasgata 9055 bynavn bolig severdighet barnehage osv...." class="form-control">
							</div>
						</div>

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<hr />
								<p>Tips: Hvis et felt ikke er relevant, <strong>LA DET STÅ blankt</strong>. Dersom du må velge VELG <strong>Ikke relevant</strong></p>
							<hr />
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Boligtype:</label>
								<select name="buildingtype" class="form-control">
									<option selected="selected" value="<?php echo $annonse_edit->BuildingType; ?>"> <?php echo $annonse_edit->BuildingType; ?> </option>
									<option value="">&dArr; Boligtype &dArr;</option>
									<option value="">Ikke relevant</option>
									<option value="enebolig">Enebolig</option>
									<option value="leilighet">Leilighet</option>
									<option value="rekkehus">Rekkehus</option>
									<option value="borettslag">Borettslag</option>
									<option value="blokk">blokk</option>
								</select>
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Eierform:</label>
								<select name="ownership" class="form-control">
									<option selected="selected" value="<?php echo $annonse_edit->Ownership; ?>"><?php echo $annonse_edit->Ownership; ?></option>
									<option value="">&dArr; Boligtype &dArr;</option>
									<option value="">Ikke relevant</option>
									<option value="selveier">Selveier</option>
									<option value="borettslag">Borettslag</option>
									<option value="andelsleilighet">Andelsleilighet</option>
									<option value="andelsbolig">Andelsbolig</option>
									<option value="aksjebolig">Aksjebolig</option>
								</select>
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Pris:</label>
								<input type="number" name="price" value="<?php echo $annonse_edit->Price; ?>" placeholder="Hva er prisen...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Totalpris:</label>
								<input type="number" name="totalprice" value="<?php echo $annonse_edit->TotalPrice; ?>" placeholder="Hva er total prisen...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Byggeår:</label>
								<input type="number" name="buildyear" value="<?php echo $annonse_edit->BuildYear; ?>" placeholder="Når ble eiendomen laget...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Verditakt:</label>
								<input type="number" name="appraisedvalue" value="<?php echo $annonse_edit->AppraisedValue; ?>" placeholder="Verditakst...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Fellesgjeld:</label>
								<input type="number" name="commondept" value="<?php echo $annonse_edit->CommonDept; ?>" placeholder="Fellesgjeld...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Fellesformue:</label>
								<input type="number" name="commonwealth" value="<?php echo $annonse_edit->CommonWealth; ?>" placeholder="Fellesformue...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Felleskostnader:</label>
								<input type="number" name="commoncosts" value="<?php echo $annonse_edit->SharedCosts; ?>" placeholder="Felleskostnader...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Lånetakst:</label>
								<input type="number" name="loanvalue" value="<?php echo $annonse_edit->LoanValue; ?>" placeholder="Lånetakst...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Areal:</label>
								<input type="number" name="area" value="<?php echo $annonse_edit->Area; ?>" placeholder="Areal...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Bruttoareal:</label>
								<input type="number" name="bruttoareal" value="<?php echo $annonse_edit->AreaOfGross; ?>" placeholder="Bruttoareal...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Bruksareal:</label>
								<input type="number" name="bruksareal" value="<?php echo $annonse_edit->AreaOfUse; ?>" placeholder="Bruksareal...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Primærrom:</label>
								<input type="number" name="areapeimary" value="<?php echo $annonse_edit->AreaPrimary; ?>" placeholder="Primærrom...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Tomt:</label>
								<input type="number" name="plot" value="<?php echo $annonse_edit->Plot; ?>" placeholder="Tomt...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Totalt antall rom:</label>
								<input type="number" name="roms" value="<?php echo $annonse_edit->Roms; ?>" placeholder="Antall rom...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Soverom:</label>
								<input type="number" name="bedroms" value="<?php echo $annonse_edit->BedRoms; ?>" placeholder="Soverom...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Bad:</label>
								<input type="number" name="bathroms" value="<?php echo $annonse_edit->BathRoms; ?>" placeholder="Bad...." class="form-control">
							</div>
						</div>

					</div>
					
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bottom-group">
							<input type="hidden" name="update_annonse_token" value="<?php echo Token::create(); ?>">
							<input type="hidden" name="update_id" value="<?php echo $annonse_edit->AnnonseID; ?>">
							<button class="btn btn-success"> <i class="fa fa-refresh"></i> &nbsp; oppdater annonse </button>
							<a href="/" class="btn btn-default"> <i class="fa fa-remove"></i> &nbsp; Avbryt </a>
						</div>
					</div>
				</form>
			<?php } else { ?>

				<div class="entry-title">ny annonse</div>
				<form action="" method="post" enctype="multipart/form-data">
					<div class="form-information">
						<p>
							<strong>Felt markert med <i class="fa fa-star"></i> kreves. </strong>
							<hr />
						</p>
					</div>

					<div class="page-header">
						<h4>Kunde søk</h4>
					</div>
					<div class="newcontract-form">
						<form accept="" method="post">
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
									<div class="form-group">
										<label>Fornavn: <i class="fa fa-star"></i></label>
										<input type="text" name="firstname" placeholder="Hva heter kunden?" value="<?php echo IO::get('firstname'); ?>" class="form-control" />
									</div>	
								</div>

								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
									<div class="form-group">
										<label>Etternavn: <i class="fa fa-star"></i></label>
										<input type="text" name="lastname" placeholder="Hva er kundesn etternavn ?" value="<?php echo IO::get('lastname'); ?>" class="form-control" />
									</div>	
								</div>

								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
									<div class="form-group">
										<label>Fødselsdato: <i class="fa fa-star"></i></label>
										<input type="date" name="birthday" value="<?php echo IO::get('birthday'); ?>" class="form-control" />
									</div>	
								</div>


								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
									<div class="form-group">
										<input type="hidden" name="search_token" value="<?php echo Token::create(); ?>" />
										<label style="width: 100%;" >&nbsp;</label>
										<button class="btn btn-success btn-block"><i class="fa fa-search"></i> &nbsp; SØK KUNDE</button>
									</div>	
								</div>
							</div>
						</form>
					</div>

					<?php if(!empty($user_infromation)) { ?>
						<div class="page-header">
							<h3>Funnet kunde</h3>
						</div>

						<table class="table table-striped table-hover ">
							<thead>
								<tr class="warning">
									<th><i class="fa fa-user"></i> Navn</th>
									<th><i class="fa fa-envelope-o"></i> E-post</th>
									<th><i class="fa fa-map-marker"></i> Adresse</th>
									<th><i class="fa fa-pencil"></i> Handling</th>
								</tr>
							</thead>
							<tbody>
								<tr class="info">
									<td><?php echo ucfirst($user_infromation->FirstName), ' ', ucfirst($user_infromation->LastName), ' ('. $user_infromation->BirthDay .')'; ?></td>
									<td><?php echo $user_infromation->EmailAddress; ?></td>
									<td><?php echo $user_infromation->Address, ' ', $user_infromation->ZipCode, ' ', $user_infromation->ZipPlace; ?></td>
									<td><a href="/newcontract/new/<?php echo $user_infromation->UserID; ?>/" class="btn btn-xs btn-success"><i class="fa fa-plus-square"></i> &nbsp; Opprett avtale</a></td>
								</tr>
							</tbody>
						</table> 

					<?php } ?>




					

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Annonsen er bestilt av: &nbsp; <span class="red">*</span></label>
								<select name="customer" class="form-control">
									<option selected="selected" value="">&dArr; Velg hvem annonsen er bestilt av &dArr;</option>
									<?php 
										if(!empty($users)) {
											foreach($users as $user) {
												echo '<option value="'. $user->UserID .'">'. ucfirst($user->FirstName) . ' ' . ucfirst($user->LastName) . ' ( '. date("d.m.Y", strtotime($user->BirthDay)) .' )</option>';
											}
										} 

									?>
								</select>
							</div>
						</div>
					</div>

					<div class="page-header">
						<h4>Annonse innformasjon</h4>
					</div>

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Overskrift: &nbsp; <span class="red">*</span></label>
								<input type="text" name="title" value="<?php echo IO::get('title'); ?>" placeholder="Gi annonsen en overskirft" class="form-control">
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Beskrivelse / fritekst: &nbsp; <span class="red">*</span></label>
								<div class="bbcode-controls">
									<ul>
										<li><button onclick="settag('b', 'description')" type="button" class="btn bbcode-btn btn-default">B</button></li>
										<li><button onclick="settag('u', 'description')" type="button" class="btn bbcode-btn btn-default">U</button></li>
										<li><button onclick="settag('i', 'description')" type="button" class="btn bbcode-btn btn-default"><em>i</em></button></li>
										<li><button onclick="settag('url', 'description')" type="button" class="btn bbcode-btn btn-default"><i class="fa fa-link"></i> </button></li>
									</ul>
								</div>
								<textarea name="description" id="description" rows="6" placeholder="Skriv det du måtte ønske relatert til visningen." class="form-control"><?php echo IO::get('description'); ?></textarea>
							</div>
						</div>

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Adresse: &nbsp; <span class="red">*</span></label>
								<input type="text" name="address" value="<?php echo IO::get('address'); ?>" placeholder="Olasgate 55, 0000 Stedsnavn" class="form-control">
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Video: &nbsp; <span class="red">*</span></label>
								<input type="text" name="video" value="<?php echo IO::get('video'); ?>" placeholder="Lim inn lenken til video fra YouTube" class="form-control">
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Kart: &nbsp; <span class="red">*</span></label>
								<input type="text" name="googlemaps" value="<?php echo IO::get('googlemaps'); ?>" placeholder="Olasgate 55 0000 Stedsnavn (Google Maps)" class="form-control">
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Primært bilde: &nbsp; <span class="red">*</span></label>
								<input type="file" name="image" accept="image/*" />
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Tileggsbilder: &nbsp; <span class="red">*</span></label>
								<input type="file" name="images[]" multiple accept="image/*" />
							</div>
						</div>
						
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Nøkkelord for søk: (separeres med mellomrom) &nbsp; <span class="red">*</span></label>
								<input type="text" name="keywords" value="<?php echo IO::get('keywords'); ?>" placeholder="olasgata 9055 bynavn bolig severdighet barnehage osv...." class="form-control">
							</div>
						</div>

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<hr />
								<p>Tips: Hvis et felt ikke er relevant, <strong>LA DET STÅ blankt</strong>. Dersom du må velge VELG <strong>Ikke relevant</strong></p>
							<hr />
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Boligtype:</label>
								<select name="buildingtype" class="form-control">
									<option selected="selected" value="">&dArr; Boligtype &dArr;</option>
									<option value="">Ikke relevant</option>
									<option value="enebolig">Enebolig</option>
									<option value="leilighet">Leilighet</option>
									<option value="rekkehus">Rekkehus</option>
									<option value="borettslag">Borettslag</option>
									<option value="blokk">blokk</option>
								</select>
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Eierform:</label>
								<select name="ownership" class="form-control">
									<option selected="selected" value="">&dArr; Boligtype &dArr;</option>
									<option value="">Ikke relevant</option>
									<option value="selveier">Selveier</option>
									<option value="borettslag">Borettslag</option>
									<option value="andelsleilighet">Andelsleilighet</option>
									<option value="andelsbolig">Andelsbolig</option>
									<option value="aksjebolig">Aksjebolig</option>
								</select>
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Pris:</label>
								<input type="number" name="price" value="<?php echo IO::get('price'); ?>" placeholder="Hva er prisen...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Totalpris:</label>
								<input type="number" name="totalprice" value="<?php echo IO::get('totalprice'); ?>" placeholder="Hva er total prisen...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Byggeår:</label>
								<input type="number" name="buildyear" value="<?php echo IO::get('buildyear'); ?>" placeholder="Når ble eiendomen laget...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Verditakt:</label>
								<input type="number" name="appraisedvalue" value="<?php echo IO::get('appraisedvalue'); ?>" placeholder="Verditakst...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Fellesgjeld:</label>
								<input type="number" name="commondept" value="<?php echo IO::get('commondept'); ?>" placeholder="Fellesgjeld...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Fellesformue:</label>
								<input type="number" name="commonwealth" value="<?php echo IO::get('commonwealth'); ?>" placeholder="Fellesformue...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Felleskostnader:</label>
								<input type="number" name="commoncosts" value="<?php echo IO::get('commoncosts'); ?>" placeholder="Felleskostnader...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Lånetakst:</label>
								<input type="number" name="loanvalue" value="<?php echo IO::get('loanvalue'); ?>" placeholder="Lånetakst...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Areal:</label>
								<input type="number" name="area" value="<?php echo IO::get('area'); ?>" placeholder="Areal...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Bruttoareal:</label>
								<input type="number" name="bruttoareal" value="<?php echo IO::get('bruttoareal'); ?>" placeholder="Bruttoareal...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Bruksareal:</label>
								<input type="number" name="bruksareal" value="<?php echo IO::get('bruksareal'); ?>" placeholder="Bruksareal...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Primærrom:</label>
								<input type="number" name="areapeimary" value="<?php echo IO::get('areapeimary'); ?>" placeholder="Primærrom...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Tomt:</label>
								<input type="number" name="plot" value="<?php echo IO::get('plot'); ?>" placeholder="Tomt...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Totalt antall rom:</label>
								<input type="number" name="roms" value="<?php echo IO::get('roms'); ?>" placeholder="Antall rom...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Soverom:</label>
								<input type="number" name="bedroms" value="<?php echo IO::get('bedroms'); ?>" placeholder="Soverom...." class="form-control">
							</div>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<label>Bad:</label>
								<input type="number" name="bathroms" value="<?php echo IO::get('bathroms'); ?>" placeholder="Bad...." class="form-control">
							</div>
						</div>

					</div>
					
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bottom-group">
							<input type="hidden" name="nyannonse_token" value="<?php // echo Token::create(); ?>">
							<button class="btn btn-success"> <i class="fa fa-plus"></i> &nbsp; Opprett annonse </button>
							<a href="/" class="btn btn-default"> <i class="fa fa-remove"></i> &nbsp; Avbryt </a>
						</div>
					</div>
				</form>

			<?php } ?>

		</div>
	</div>


<?php 
	include_once('includes/files/botarea.php'); 
?>