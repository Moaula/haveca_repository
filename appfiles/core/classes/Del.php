<?php
class Del {

	/*
	-	Creating class variables
	*/
		private $_db;

	/*
	-	Constructing the class
	-	in this process we connect to the database
	-	every time it constructs it reconnects to the database
	*/

		public function __construct() {
			$this->_db = Database::dbinit();
		}

	/*
	-	Deleteing something from the database
	-	and its conetnt on disk
	*/

		public function delete($TABLE, $IDENTIFIER, $FIELDNAME) {
			$query = "DELETE FROM {$TABLE} WHERE {$FIELDNAME} = ?";
			$this->_db->query($query, array($IDENTIFIER));
		}


}