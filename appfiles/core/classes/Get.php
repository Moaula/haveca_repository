<?php
class Get {

	/*
	-	Creating class variables
	*/
		private $_db,
				$_data,
				$_results,
				$_result,
				$_count = 0;

	/*
	-	Constructing the class
	-	in this process we connect to the database
	-	every time it constructs it reconnects to the database
	*/

		public function __construct() {
			$this->_db = Database::dbinit();
		}

	/*
	-	Select all from spesific table
	-	limit the selection to a value
	-	use desc or asc
	*/

		public function get_data($table, $identifier = "ID", $order = "ASC", $limit = 1000) {
			$query = $this->_db->query("SELECT * FROM {$table} ORDER BY {$identifier} {$order} LIMIT {$limit}");
			if($query->count()) {
				$this->_results = $query->results();
				$this->_result  = $query->result();
				return true;
			}
			return false;
		}

		public function get_data_by_id($table, $identifier = "ID", $id, $order = "ASC", $limit = 1000) {
			$query = "SELECT * FROM {$table} WHERE {$identifier} = ? ORDER BY {$identifier} {$order} LIMIT {$limit}";
			$value = array($id);
			$data = $this->_db->query($query, $value);
			if($data->count()) {
				$this->_results = $data->results();
				$this->_result = $data->result();
			}
		}

		public function get_data_msg($table, $identifier = "ID", $id, $order = "ASC", $limit = 1000) {
			$query = "SELECT * FROM {$table} WHERE {$identifier} = ? ORDER BY MessageID {$order} LIMIT {$limit}";
			$value = array($id);
			$data = $this->_db->query($query, $value);
			if($data->count()) {
				$this->_results = $data->results();
				$this->_result = $data->result();
			}
		}
		

	/*
	-	This method will handle the search site
	-	This method shud be used by more then one
	-	dataase.
	*/

		public function search_db($table, $search, $field_one, $field_two) {
			array_walk($search, 'clean_array');
			$search = preg_replace("/[^a-åA-Å0-9]+/", '', $search);


			$i = 0;
			$query_string = "SELECT * FROM {$table} WHERE";

			foreach($search as $key) {
				$i++;
				if($i == 1) {
					$query_string .= " {$field_one} LIKE CONCAT('%{$key}%') OR {$field_two} LIKE CONCAT('%{$key}%')";
				} else {
					$query_string .= " OR {$field_one} LIKE CONCAT('%{$key}%') OR {$field_two} LIKE CONCAT('%{$key}%')";
				}
			}

		
			$datas = $this->_db->query($query_string);
			if($datas->count()) {
				$this->_results = $datas->results();
				$this->_count   = $datas->count();
				return true;
			}
			return false;
		}


		/*
			$values = array($search, $search);
			$query = "SELECT * FROM {$table} WHERE {$field_one} LIKE CONCAT('%',?,'%') OR {$field_two} LIKE CONCAT('%',?,'%')";
		*/

	/*
	-	Return the result or false
	- 	if the methods have results
	*/

		public function results() {
			return $this->_results;
		}

		public function result() {
			return $this->_result;
		}

		public function count() {
			return $this->_count;
		}

}	
