<?php
class Put {
	/*
	-	Creating the class variables
	-	and some constants
	*/
		private $_db,
				$_lastid,
				$_votes;

	/*
	-	Constructing the class
	-	in this process we connect to the database
	-	every time it constructs it reconnects to the database
	*/

		public function __construct() {
			$this->_db = Database::dbinit();
		}

	/*
	-	Create method will create a new row
	-	inn the mysql database.
	*/

		public function create($TABLE, $FIELDS = array()) {
			if(!$this->_db->insert($TABLE, $FIELDS)) {
				throw new Exception("Error: 28.PUT-DIED-HAD-PROBLEMS-CREATING");
			}
		}

	/*
	-	Update data based on id
	-	will update the content in array to database
	-	where the pointers aims.
	*/

		public function update($TABLE, $FIELDS = array(), $IDENTIFIER, $IDENTIFIER_FIELDNAME = "ID") {
			if(!$this->_db->update($TABLE, $FIELDS, $IDENTIFIER, $IDENTIFIER_FIELDNAME)) {
				throw new Exception("Error: 40.PUT-DIED-HAD-PROBLEMS-UPDATEING");
			}
		}

	/*
	-	exists wil check if a row or value exists
	-	in the selected table in the selected database
	*/

		public function exists($TABLE, $FIELDS, $GET) {
			$query = $this->_db->select($TABLE, $FIELDS, $GET);
			if($query->count()) { return true; } return false;
		}

	/*
	-	Last inserted id for return
	*/

		public function lastid() {
			return $this->_db->lastInsertId();
		}
}