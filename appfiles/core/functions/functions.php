<?php 
//	WriteAlerts function:
	function writealerts($string, $type = 'success', $class = 'default-class') {
		return '
		<div class="alert alert-dismissible alert-'. $type .' '. $class .'" role="alert"> 
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">x</span>
				<span class="sr-only">Close</span>
			</button>
			'. $string .'
		</div>';
	}

//	WriteErrors function:
	function writeerrors($errors, $class = 'default-class', $header = 'Vennligst sjekk:') {
		return '
		<div class="alert alert-dismissible alert-danger '. $class .'" role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">x</span>
				<span class="sr-only">Close</span>
			</button>
			<h4>'. $header .'</h4>
			<ul><li>' .	implode('</li><li>', $errors) . '</li></ul>
		</div>';
	}

//	lookup a url if it exists
	function brreg($url)
	{
	    $ch = curl_init($url);
	    curl_setopt($ch, CURLOPT_NOBODY, true);
	    curl_exec($ch);
	    $retCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);

	    if ($retCode == 200) { return true; } 
	    return false; 
	}

//	Thumbs maker
	function create_thumbs($source, $product, $ext, $w, $h) {
		$wanted_width = $w;
		$wanted_height = $h;

		list($orginal_width, $orginal_height) = getimagesize($source);
		$scale = ($orginal_width / $orginal_height);

		if (($wanted_width / $wanted_height) > $scale) {
			$wanted_width = $wanted_height * $scale;
		} else {
			$wanted_height = $wanted_width / $scale;
		}

		$image = '';
		if ($ext == 'gif') {
			$image = imagecreatefromgif($source);
		} else if ($ext == 'png') {
			$image = imagecreatefrompng($source);
		} else {
			$image = imagecreatefromjpeg($source);	
		}

		$blackbox = imagecreatetruecolor($wanted_width, $wanted_height);
		imagealphablending($blackbox, false);
		$col = imagecolorallocatealpha($blackbox, 240, 240, 240, 0);
		imagefilledrectangle($blackbox, 0, 0, $wanted_width, $wanted_height, $col);
		imagealphablending($blackbox, true);

		imagecopyresampled($blackbox, $image, 0, 0, 0, 0, $wanted_width, $wanted_height, $orginal_width, $orginal_height);
		
		if ($ext == 'gif') {
			imagegif($blackbox, $product, 100);
		} else if ($ext == 'png') {
			imagepng($blackbox, $product, 9);
		} else {
			imagejpeg($blackbox, $product, 90);
		}
	}

/*
-	bbcode 
*/
	function gYTid($url) {
		$url_string = parse_url($url, PHP_URL_QUERY);
		parse_str($url_string, $args);
		return isset($args['v']) ? $args['v'] : false;
	}
	
	function bbcode($input){
		$input = strip_tags($input);
		$input = htmlentities($input);

		$search = array(
		'/\[b\](.*?)\[\/b\]/is',
		'/\[i\](.*?)\[\/i\]/is',
		'/\[u\](.*?)\[\/u\]/is',
		'/\[img\](.*?)\[\/img\]/is',
		'/\[url=(.*?)\](.*?)\[\/url\]/is',
		'/\[YouTubeVideo\](.*?)\[\/YouTubeVideo\]/is'
		);

		$v = gYTid($input);

		$replace = array(
		'<b>$1</b>',
		'<i>$1</i>',
		'<u>$1</u>',
		'<img src="$1">',
		'<a href="$1">$2</a>',
		'<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'. substr($v, 0, 11) .'?showinfo=0&amp;controls=0"></iframe></div>'
		);

		return preg_replace($search,$replace,$input);
		}


	// getStars

	function getStars($value = null) {
		switch ($value) {
			case 1:
				return '<i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>';
				break;

			case 2:
				return '<i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>';
				break;

			case 3:
				return '<i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>';
				break;

			case 4:
				return '<i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-o"></i>';
				break;

			case 5:
				return '<i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i>';
				break;
			
			default:
				return '<i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>';
				break;
		}
	}


function clean_array(&$array) {
	return htmlentities(strip_tags($array));	
}