<?php

function sending_activation($reciver, $code, $fname, $lname) {
	$subject = 'Aktiver din konto - Dronevisning.no';
	$body = '<table border="0" align="center" cellpadding="0" cellspacing="0" width="600" style="font-family: verdana; font-size: 12px; ">
				<tr>
					<td align="center" bgcolor="#fff" style="padding: 40px 0 30px 0; border: 1px solid #eee; border-bottom: none;">
					 	<img src="http://sky.htmlnorge.no/images/logos/dronevisning/small-dv.png" alt="Dronevisning Logo" width="20%" style="display: block;" /><br />
					 	<b>www.dronevisning.no</b>
					</td>
				</tr>
				<tr>
					<td bgcolor="#fff" style="padding: 40px 30px 40px 30px; border: 1px solid #eee;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td style="font-family: verdana; font-size: 18px;">
									<b>Din konto hos oss er klar.</b><br />
								</td>
							</tr>
							<tr>
								<td style="padding: 20px 0 30px 0; font-family: verdana; font-size: 14px;">
									Hei kjære <strong>'. ucfirst($fname) . ', ' . ucfirst($lname) . '</strong>. 
									<br /><br /><br />

									Din konto hos Dronevisning.no er nå klar. Du kan aktivere din konto ved å 
									klikke på lenken under. Etter aktivering kan du logge på.

									<br /><br /><br /><br />

									<strong>Aktiverings lenke:</strong> <br />					
									<strong><a style="color: #333;" href="http://dronevisning.no/verify/kunde/'. $reciver .'/'. $code .'/">http://dronevisning.no/aktivering</a></strong>

									<br /><br /><br />
									<hr style="border: none; border-bottom: 1px solid #eee;" />
									<br /><br />

									<strong>MERK!</strong><br />
										Dette er en automatisk generert melding fra Dronevisning.no, du kan ikke svare på denne
										e-posten. Dersom du ønsker noe hjelp eller har spørsmål ta kontakte med oss på 
										<a style="color: #333;" href="mailto:kundeservice@dronevisning.no">kundeservice@dronevisning.no</a>.
									<br /><br />

									<strong>Med vennlig hilsen</strong><br />
									Dronevisning kundeservice.
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td style="background: #ae0404; padding: 30px 30px 30px 30px; border: 1px solid #eee;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="75%" style="color: #fff; font-family: verdana; font-size: 14px;">
								 	<a style="color: #fff;" href="mailto:kundeservice@dronevisning.no">kundeservice@dronevisning.no</a> <br />
								 	Kopirett &copy; 2013-'. date('Y') .', Dronevisning&reg;
								</td>

							</tr>
						</table>
					</td>
				</tr>
			</table>';

	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$headers .= 'To: '.$fname.' <'.$reciver.'>, '.$fname.' <'.$reciver.'>' . "\r\n";
	$headers .= 'From: '. $subject .' <noreply@dronevisning.htmlnorge.no>' . "\r\n";
	mail($reciver, $subject, $body, $headers);
}

function sending_activation_operator($reciver, $code, $fname, $lname, $orgname) {
	$subject = 'Aktiver din konto - Dronevisning.no';
	$body = '<table border="0" align="center" cellpadding="0" cellspacing="0" width="600" style="font-family: verdana; font-size: 12px; ">
				<tr>
					<td align="center" bgcolor="#262626" style="padding: 40px 0 30px 0; border: 1px solid #eee; border-bottom: none;">
					 	<img src="http://sky.htmlnorge.no/images/logos/dronevisning/small-dv.png" alt="Dronevisning Logo" width="20%" style="display: block; border-radius: 100px;" /><br />
					 	<b style="color: #fff; font-size: 20px;">Dronevisning</b>
					</td>
				</tr>
				<tr>
					<td bgcolor="#ddd" style="padding: 40px 30px 40px 30px; border: 1px solid #eee;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td style="font-family: verdana; font-size: 18px;">
									<b>Din konto hos oss er klar.</b><br />
								</td>
							</tr>
							<tr>
								<td style="padding: 20px 0 30px 0; font-family: verdana; font-size: 14px;">
									Hei kjære <strong>'. ucfirst($fname) . ', ' . ucfirst($lname) . '</strong>.<br />
									'. $orgname .' 
									<br /><br /><br />

									Din/deres konto hos Dronevisning.no er nå klar. For å aktivere din/deres konto 
									klikk på lenken under.

									<br /><br /><br />

									<strong>Aktiverings lenke:</strong> <br />					
									<strong><a style="color: #333;" href="http://dronevisning.no/verify/operator/'. $reciver .'/'. $code .'/">http://dronevisning.no/aktivering</a></strong>

									<br /><br /><br />
									<hr style="border: none; border-bottom: 1px solid #eee;" />
									<br /><br />

									<strong>MERK!</strong><br />
										Dette er en automatisk generert melding fra Dronevisning.no, du kan ikke svare på denne
										e-posten. Dersom du ønsker noe hjelp eller har spørsmål ta kontakte med oss på 
										<a style="color: #333;" href="mailto:kundeservice@dronevisning.no">kundeservice@dronevisning.no</a>.

									<br /><br />

									<strong>Med vennlig hilsen</strong><br />
									Dronevisning kundeservice.
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td style="background: #5cb85c; padding: 30px 30px 30px 30px; border: 1px solid #eee;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="75%" style="color: #fff; font-family: verdana; font-size: 14px;">
								 	<a style="color: #fff;" href="mailto:kundeservice@dronevisning.no">kundeservice@dronevisning.no</a> <br />
								 	Kopirett &copy; 2013-'. date('Y') .', Dronevisning&reg;
								</td>

							</tr>
						</table>
					</td>
				</tr>
			</table>';

	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$headers .= 'To: '.$fname.' <'.$reciver.'>, '.$fname.' <'.$reciver.'>' . "\r\n";
	$headers .= 'From: '. $subject .' <noreply@dronevisning.htmlnorge.no>' . "\r\n";
	mail($reciver, $subject, $body, $headers);
}


function recover_username($reciver, $username, $fname, $lname) {
	$subject = 'Ditt brukernavn - Dronevisning.no';
	$body = '<table border="0" align="center" cellpadding="0" cellspacing="0" width="600" style="font-family: verdana; font-size: 12px; ">
				<tr>
					<td align="center" bgcolor="#262626" style="padding: 40px 0 30px 0; border: 1px solid #eee; border-bottom: none;">
					 	<img src="http://sky.htmlnorge.no/images/logos/dronevisning/small-dv.png" alt="Dronevisning Logo" width="20%" style="display: block; border-radius: 100px;" /><br />
					 	<b style="color: #fff; font-size: 20px;">Dronevisning</b>
					</td>
				</tr>
				<tr>
					<td bgcolor="#dfdfdf" style="padding: 40px 30px 40px 30px; border: 1px solid #eee;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td style="font-family: verdana; font-size: 18px;">
									<b>Hei.</b><br />
								</td>
							</tr>
							<tr>
								<td style="padding: 20px 0 30px 0; font-family: verdana; font-size: 14px;">
									<strong>'. ucfirst($fname) . ', ' . ucfirst($lname) . '</strong>.<br />
									<br /><br /><br />

									Ditt passord er: <br /> 
									<strong>'. $username .'</strong>

									<br />
									<br />
									<br />

									<hr style="border: none; border-bottom: 1px solid #eee;" />
									<br /><br />

									<strong>MERK!</strong><br />
										Dette er en automatisk generert melding fra Dronevisning.no, du kan ikke svare på denne
										e-posten. Dersom du ønsker noe hjelp eller har spørsmål ta kontakte med oss på 
										<a style="color: #333;" href="mailto:kundeservice@dronevisning.no">kundeservice@dronevisning.no</a>.

									<br /><br />

									<strong>Med vennlig hilsen</strong><br />
									Dronevisning kundeservice.
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td style="background: #5cb85c; padding: 30px 30px 30px 30px; border: 1px solid #eee;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="75%" style="color: #fff; font-family: verdana; font-size: 14px;">
								 	<a style="color: #fff;" href="mailto:kundeservice@dronevisning.no">kundeservice@dronevisning.no</a> <br />
								 	Kopirett &copy; 2013-'. date('Y') .', Dronevisning&reg;
								</td>

							</tr>
						</table>
					</td>
				</tr>
			</table>';

	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$headers .= 'To: '.$fname.' <'.$reciver.'>, '.$fname.' <'.$reciver.'>' . "\r\n";
	$headers .= 'From: '. $subject .' <noreply@dronevisning.htmlnorge.no>' . "\r\n";
	mail($reciver, $subject, $body, $headers);
}

function recover_password($reciver, $password, $fname, $lname) {
	$subject = 'Ditt nye passord - Dronevisning.no';
	$body = '<table border="0" align="center" cellpadding="0" cellspacing="0" width="600" style="font-family: verdana; font-size: 12px; ">
				<tr>
					<td align="center" bgcolor="#262626" style="padding: 40px 0 30px 0; border: 1px solid #eee; border-bottom: none;">
					 	<img src="http://sky.htmlnorge.no/images/logos/dronevisning/small-dv.png" alt="Dronevisning Logo" width="20%" style="display: block; border-radius: 100px;" /><br />
					 	<b style="color: #fff; font-size: 20px;">Dronevisning</b>
					</td>
				</tr>
				<tr>
					<td bgcolor="#dfdfdf" style="padding: 40px 30px 40px 30px; border: 1px solid #eee;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td style="font-family: verdana; font-size: 18px;">
									<b>Hei.</b><br />
								</td>
							</tr>
							<tr>
								<td style="padding: 20px 0 30px 0; font-family: verdana; font-size: 14px;">
									<strong>'. ucfirst($fname) . ', ' . ucfirst($lname) . '</strong>.<br />
									<br /><br /><br />

									Ditt passord er: <br /> 
									<strong>'. $password .'</strong>

									<br />
									<br />
									<br />

									<hr style="border: none; border-bottom: 1px solid #eee;" />
									<br /><br />

									<strong>MERK!</strong><br />
										Dette er en automatisk generert melding fra Dronevisning.no, du kan ikke svare på denne
										e-posten. Dersom du ønsker noe hjelp eller har spørsmål ta kontakte med oss på 
										<a style="color: #333;" href="mailto:kundeservice@dronevisning.no">kundeservice@dronevisning.no</a>.

									<br /><br />

									<strong>Med vennlig hilsen</strong><br />
									Dronevisning kundeservice.
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td style="background: #5cb85c; padding: 30px 30px 30px 30px; border: 1px solid #eee;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="75%" style="color: #fff; font-family: verdana; font-size: 14px;">
								 	<a style="color: #fff;" href="mailto:kundeservice@dronevisning.no">kundeservice@dronevisning.no</a> <br />
								 	Kopirett &copy; 2013-'. date('Y') .', Dronevisning&reg;
								</td>

							</tr>
						</table>
					</td>
				</tr>
			</table>';

	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$headers .= 'To: '.$fname.' <'.$reciver.'>, '.$fname.' <'.$reciver.'>' . "\r\n";
	$headers .= 'From: '. $subject .' <noreply@dronevisning.htmlnorge.no>' . "\r\n";
	mail($reciver, $subject, $body, $headers);
}


/*
#	Message sendt when a user uses
#	the contact from will be sendt by this 
#	function.
*/

function customerServiceMail($reciver, $message, $subject, $fullname) {
	$subject = 'Melding fra: '. ucfirst($fullname) . ' - ' . $subject;
	$body = '<table border="0" align="center" cellpadding="0" cellspacing="0" width="600" style="font-family: verdana; font-size: 12px; ">
				<tr>
					<td align="center" bgcolor="#262626" style="padding: 40px 0 30px 0; border: 1px solid #eee; border-bottom: none;">
					 	<img src="http://sky.htmlnorge.no/images/logos/dronevisning/small-dv.png" alt="Dronevisning Logo" width="20%" style="display: block; border-radius: 100px;" /><br />
					 	<b style="color: #fff; font-size: 20px;">Dronevisning</b>
					</td>
				</tr>
				<tr>
					<td bgcolor="#ddd" style="padding: 40px 30px 40px 30px; border: 1px solid #eee;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td style="font-family: verdana; font-size: 18px;">
									<b>Melding til Kundeservice.</b><br />
								</td>
							</tr>
							<tr>
								<td style="padding: 20px 0 30px 0; font-family: verdana; font-size: 14px;">
									
									<br /><hr style="border: none; border-bottom: 1px solid #eee;" />
									<br /><br />

									'. nl2br($message) .'

									<br /><br />
									<br /><hr style="border: none; border-bottom: 1px solid #eee;" />
									<br />
								

									<strong>Med vennlig hilsen</strong><br />
									'. ucfirst($fullname) .' <br />
									<a href="mailto:'. ucfirst($reciver) .'">'. ucfirst($reciver) .' </a>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td style="background: #5cb85c; padding: 30px 30px 30px 30px; border: 1px solid #eee;">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="75%" style="color: #fff; font-family: verdana; font-size: 14px;">
								 	<a style="color: #fff;" href="mailto:kundeservice@dronevisning.no">kundeservice@dronevisning.no</a> <br />
								 	Kopirett &copy; 2013-'. date('Y') .', Dronevisning&reg;
								</td>

							</tr>
						</table>
					</td>
				</tr>
			</table>';

	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$headers .= 'To: '.$fname.' <kundeservice@dronevisning.no>' . "\r\n";
	$headers .= 'From: '. $subject .' <noreply@dronevisning.htmlnorge.no>' . "\r\n";
	mail("kundeservice@dronevisning.no", $subject, $body, $headers);
}