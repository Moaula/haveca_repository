<?php

/*
-	Startion the session and setting the error_reporting
-	error_reporting states: Production: 0 and Developement: E_ALL
*/
	session_start();
	error_reporting(E_ALL);
/*
-	Loading the class library and 
-	other php system functions.
*/
	
	spl_autoload_register(function($class) {
		$dirname = dirname(__FILE__);
		require_once $dirname . '/classes/' . $class . '.php';
	});

	$dirname = dirname(__FILE__);
	require_once $dirname . '/functions/functions.php';
	require_once $dirname . '/functions/mailing.php';
	require_once $dirname . '/vendor/autoload.php';
/*
-	get the current document.
-	used for activating buttons and giving page titles.
*/
	$CDoc = $_SERVER['PHP_SELF'];
	$CDoc = explode('/', $CDoc);
	$CDoc = $CDoc[count($CDoc) - 1];
	$CDoc = explode('.', $CDoc);
	$CDoc = $CDoc[count($CDoc) - 2];
/*
-	System "global" variables will
-	be set here:
*/
	$_init_uzer = new User();
	$_init_uzer_data = $_init_uzer->data();
	$httpReferer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "/";
	$httpHost = $_SERVER["HTTP_HOST"];
/*
-	Get if user have new message
-
*/
