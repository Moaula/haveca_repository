function load_classes() {
	var http = window.location.href;
	if ( /registration-options/ .test(http) ) {
		$("body").addClass('reg-ops');
	}
	if ( /signup/ .test(http) ) {
		$("body").addClass('reg-kunde');
	}

	if ( /verify/ .test(http) || /login/ .test(http) || /recovery/ .test(http)) {
		$("body").addClass('verify');
	}
}

function verify_confirm() {
	if( $('.confirm-verify-checkbox').is(":checked") ) {
		$(".confirm-verify").removeClass('disabled');
	} else {
		$(".confirm-verify").addClass('disabled');
	}
}

function settag(tag, textarea, t = false) {
	var txt = document.getElementById(textarea);
	if(document.selection) {
		txt.focus();
		sel = document.selection.createRange();
		sel.text = '[' + tag + ']' + sel.text + '[/' + tag + ']';
	} else if(txt.selectionStart || txt.selectionStart == '0') {	
		txt.value = (txt.value).substring(0, txt.selectionStart) + "["+tag+"]" + (txt.value).substring(txt.selectionStart, txt.selectionEnd) + "[/"+tag+"]" + (txt.value).substring(txt.selectionEnd, txt.textLength);
	} else {
		txt.value = '[' + tag + '][/' + tag + ']';
	}
	return;
}