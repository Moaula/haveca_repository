$(function(){
	load_classes();

	if( $("body").hasClass("verify") ) {
		setInterval(function(){ 
    	   verify_confirm();
		}, 500);
	}
	
	//	Footer Pusher
		setInterval(function(){
			var footerHeight = $('footer').outerHeight() + $('.footer-footer').outerHeight();
			$(document.body).css('padding-bottom', footerHeight);
		}, 100);

	//	Delete Images
		$(".delSelImg").on("click", function(){
			var id, img, thumb;

			id 		= $(this).attr("data-id");
			img 	= $(this).attr("data-img");
			thumb 	= $(this).attr("data-thumb");

			if($.isNumeric(id)) {
				$.post("img_process.php", { id: id, img: img, thumb: thumb }, function(data) {
					if(data == "done") {
						$(".imgid_" + id).addClass("hidden");
					}
				});
			}

		});

	//	Lightbox selector
		$('a.lb').lightBox();

	//	Update pay button
		$('.stripe-button-el').addClass("btn btn-success btn-lg");
		$('.stripe-button-el').html('<i class="fa fa-credit-card"></i> Betal med kort');
		$('.stripe-button-el').removeClass("stripe-button-el");

	    // Acordion
	    $('dd').hide();
	    $('dl').on('click', 'dt', function() {
	        $(this)
	            .next()
	                .toggle(200)
	                .siblings('dd')
	                    .slideUp(200);
	    });
    
});