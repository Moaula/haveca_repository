<?php include_once('config/billing-cfg.php'); include_once('includes/files/toparea.php'); ?>
	
	<?php if(!IO::exists('get')) { ?>
		<div class="container">
		 	<div class="inner-container">

		 		<!-- Flashing sessions with infromation -->
				<?php 
					if(Session::exists('billing')) { 
						echo writealerts(Session::flash('billing'), 'success'); 
					} 
				?>
				
		 		<div class="page-header">
		 			<h2><i class="fa fa-money"></i> Mine faktura</h2>
		 		</div>

		 		<?php if(!empty($my_bills)) { ?>
			 		<table class="table table-striped table-hover ">
						<thead>
							<tr>
								<th># Avtale nummer</th>
								<th># Selger</th>
								<th># Adresse</th>
								<th># Fakturadato</th>
								<th># Forfall </th>
								<th># Status</th>
								<th># Handling</th>
							</tr>
						</thead>
						<tbody>

							<?php if(!empty($my_bills)) { ?>
								<?php foreach ($my_bills as $bill) { 
										$users_info = new User($bill->SellerID);
										$users_info = $users_info->data();
								?>
									<tr class="<?php if($bill->Paid) { echo 'success'; } else { echo 'info'; } ?>" >
										<td><?php echo $bill->ContractID; ?></td>
										<td><?php echo ucfirst($users_info->FirstName), ' ', ucfirst($users_info->LastName); ?></td>
										<td><?php echo ucfirst($users_info->Address), ' ', $users_info->ZipCode, ' ', ucfirst($users_info->ZipPlace) ?></td>
										<td><?php echo date("d/m/Y", strtotime($bill->BillDate)); ?></td>
										<td><?php echo date("d/m/Y", strtotime($bill->BillDate. '+ 14 days')); ?></td>
										<td><?php if($bill->Paid) { echo '<strong>Betalt</strong>'; } else { echo '<strong>Ikke betalt</strong>'; } ?></td>
										<td>
											<a href="/billing/view/<?php echo $bill->ContractID; ?>/" class="btn btn-xs btn-success btn-block"><i class="fa fa-eye"></i> Vis Faktura</a>
										</td>
									</tr>
								<?php } ?>
							<?php } ?>

						</tbody>
					</table> 
				<?php } else { ?>

					<div class="no-bills">
						<h3>Du har ingen faktura(er) eller avtaler enda. </h3>
					</div>

				<?php } ?>
		 	</div>
		 </div>
	<?php } ?>

	<?php if(IO::exists('get') && IO::get('action') == "operator") { ?>
		<div class="container">
		 	<div class="inner-container">

		 		<!-- Flashing sessions with infromation -->
				<?php 
					if(Session::exists('billing')) { 
						echo writealerts(Session::flash('billing'), 'success'); 
					} 
				?>

		 		<div class="page-header">
		 			<h2><i class="fa fa-money"></i> Mine faktureringer </h2>
		 		</div>

		 		<?php if(!empty($seller_bills)) { ?>
			 		<table class="table table-striped table-hover ">
						<thead>
							<tr>
								<th># Avtale nummer</th>
								<th># Kjøper</th>
								<th># Adresse</th>
								<th># Fakturadato</th>
								<th># Forfall </th>
								<th># Status</th>
								<th># Handling</th>
							</tr>
						</thead>
						<tbody>

							<?php if(!empty($seller_bills)) { ?>
								<?php foreach ($seller_bills as $bill) { 
										$users_info = new User($bill->BuyerID);
										$users_info = $users_info->data();
								?>
									<tr class="<?php if($bill->Paid) { echo 'success'; } else { echo 'info'; } ?>" >
										<td><?php echo $bill->ContractID; ?></td>
										<td><?php echo ucfirst($users_info->FirstName), ' ', ucfirst($users_info->LastName); ?></td>
										<td><?php echo ucfirst($users_info->Address), ' ', $users_info->ZipCode, ' ', ucfirst($users_info->ZipPlace) ?></td>
										<td><?php echo date("d/m/Y", strtotime($bill->BillDate)); ?></td>
										<td><?php echo date("d/m/Y", strtotime($bill->BillDate. '+ 14 days')); ?></td>
										<td><?php if($bill->Paid) { echo '<strong>Betalt</strong>'; } else { echo '<strong>Ikke betalt</strong>'; } ?></td>
										<td>
											<a href="/billing/view/<?php echo $bill->ContractID; ?>/" class="btn btn-xs btn-success "><i class="fa fa-eye"></i> Vis</a>
											<a onclick="return confirm('Er du sikker du ønsker å slette denne avtalen ?\nAvtalen blir også slettet for kunde: <?php echo ucfirst($users_info->FirstName), ' ', ucfirst($users_info->LastName); ?>');" href="/billing/delete/<?php echo $bill->ContractID; ?>/" class="btn btn-xs btn-danger "><i class="fa fa-trash"></i> Slett</a>
										</td>
									</tr>
								<?php } ?>
							<?php } ?>

						</tbody>
					</table> 
				<?php } else { ?>

					<div class="no-bills">
						<h3>Du har ikke fakturert eller oprettet avtale med noen enda. </h3>
					</div>

				<?php } ?>
		 	</div>
		 </div>
	<?php } ?>

	<?php if(IO::exists('get')) { ?>
		<?php if(IO::get('action') == "view" && is_numeric(IO::get('id'))) { ?>
			<?php if(!empty($this_bill)) { ?>
			
				<div class="container">
				 	<div class="inner-container">
				 		
			 			<div class="medium-form form-center">
			 				<div class="billing-bill">

			 					<div class="bill-infromation">
			 						<h3 class="float_left">Faktura</h3>
			 						<h3 class="float_right">Betaling til</h3>
			 					</div>

			 					<div class="users-informations">
			 						<div class="customer-info">
			 							<ul>
			 								<li><strong><?php echo ucfirst($buyer->FirstName), ' ', ucfirst($buyer->LastName); ?></strong></li>
			 								<li><?php echo ucfirst($buyer->Address); ?></li>
			 								<li><?php echo $buyer->ZipCode, ' ', ucfirst($buyer->ZipPlace); ?></li>
			 								<li>&nbsp;</li>
			 								<li><?php echo ucfirst($buyer->EmailAddress) ?></li>
			 							</ul>
			 						</div>
			 						<div class="users-info">
			 							<ul>
			 								<li><strong><?php echo ucfirst($seller->FirstName), ' ', ucfirst($seller->LastName); ?></strong></li>
			 								<li><?php echo ucfirst($seller->Address); ?></li>
			 								<li><?php echo $seller->ZipCode, ' ', ucfirst($seller->ZipPlace); ?></li>
			 								<li>&nbsp;</li>
			 								<li><strong>Fakturadato:</strong> <?php echo date("d/m/Y", strtotime($this_bill->BillDate)); ?></li>
			 								<li><strong>Forfall:</strong> <?php echo date("d/m/Y", strtotime($this_bill->BillDate. '+ 14 days')); ?></li>
			 								<li><?php  ?></li>
			 								<li><strong>Betaling:</strong> <strong><?php if($this_bill->Paid) { echo "Betalt"; } else { echo "Ikke betalt"; } ?></strong></li>
			 							</ul>
			 						</div>
			 					</div>

			 					<div class="contract-details">
			 						<h3>Avtale kriterier</h3>
			 						<p>
			 							<?php echo nl2br($this_bill->ContractDetails); ?>
			 							<br /><br />

			 							<strong class="red">Ved å betale denne fakturaen godtar du avtale kriteriene.</strong>
			 						</p>
			 					</div>

			 					<div class="billing-information">
			 						<h3>Artikler</h3>
			 						<p>
			 							<?php echo nl2br($this_bill->BillingDetails); ?>
			 						</p>
			 					</div>

			 					<div class="payment-details">
			 						<div class="price"><?php echo number_format($this_bill->Price, 2, ',', '.'); ?> NOK <?php if($this_bill->Paid) { echo "- Betalt den ". date('d/m/Y', strtotime($this_bill->PayDate)); } ?></div>
			 					</div>

			 					<?php if(!$this_bill->Paid && $this_bill->BuyerID == $_init_uzer_data->UserID) { ?>
				 					<div class="payment-btn">
										<form action="/process_payments.php" method="post">
											<input type="hidden" name="buyer_email" value="<?php echo $buyer->EmailAddress; ?>" />
											<input type="hidden" name="bill_id" value="<?php echo $this_bill->ContractID; ?>" />
											<input type="hidden" name="payment_token" value="<?php echo Token::create(); ?>" />
											<script
												src="https://checkout.stripe.com/checkout.js" class="stripe-button"
												data-key="pk_test_D9ZIniQKJexC8ZHNKxJra16Y"
												data-amount="<?php echo $money; ?>"
												data-email="<?php echo $buyer->EmailAddress; ?>"
												data-name="Dronevisning"
												data-description="Faktura betaling"
												data-image="https://s3.amazonaws.com/stripe-uploads/acct_15WReFF8VxbJdswNmerchant-icon-1430053138135-mainlogo-xs.png"
												data-locale="auto"
												data-currency="nok">
											</script>
										</form>
				 					</div>	
			 					<?php } ?>
				 			</div>
				 		</div>

				 	</div>
				</div>
			<?php } ?>
		<?php } ?>
	<?php } ?>

<?php include_once('includes/files/botarea.php'); ?>