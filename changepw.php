<?php 
	include_once('config/changepw-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	<div class="container">
		<div class="inner-container small-form form-center">

			<!-- Error outputting -->
			<?php 
				if(!empty($errors)) { 
					echo writeerrors($errors, '', 'En feil har oppstått.'); 
				} 
			?>

			<!-- Flashing sessions with infromation -->
			<?php 
				if(Session::exists('changepw')) { 
					echo writealerts(Session::flash('changepw'), 'success'); 
				} 
			?>

			<div class="entry-title"><i class="fa fa-refresh"></i> Endre passord</div>
			<div class="form-information">
				<p>
					<strong>Felt markert med <i class="fa fa-star"></i> kreves. </strong>
					<hr />
				</p>
			</div>


			<form action="" method="post">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<label>Gjeldende passord: <i class="fa fa-star"></i></label>
							<input type="password" name="old_password" class="form-control" placeholder="Hva er ditt passord ?" />
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<label>Nytt passord: <i class="fa fa-star"></i></label>
							<input type="password" name="new_password" class="form-control" placeholder="Lag et nytt passord." />
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<label>Bekreft nytt passord: <i class="fa fa-star"></i></label>
							<input type="password" name="confirm_password" class="form-control" placeholder="Bekreft det nye passordet." />
						</div>
					</div>


					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group bottom-group">
							<input type="hidden" name="update_password_token" value="<?php echo Token::create(); ?>">
							<button class="btn btn-success"><i class="fa fa-refresh"></i> &nbsp; Bytt passord </button>
							<a href="/" class="btn btn-default"><i class="fa fa-remove"></i> &nbsp; Avbryt </a>
						</div>
					</div>
				</div>
			</form>

		</div>
	</div>



<?php 
	include_once('includes/files/botarea.php'); 
?>