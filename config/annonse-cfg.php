<?php

//	Include files:
	include_once('appfiles/core/init.php');

//	Site title:
	$site_title = "Kontrollpanel: Visning";

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if(!$_init_uzer->isOnline()) { Redirect::to("/"); }
    if(!$_init_uzer->hasPerm('operator')) { Redirect::to("/"); }

/*
#	Get the user for adding them
#	to the (visning).
#	A user gets linked to a visning.
*/

	if(IO::exists('get')) {
		if(IO::get('action') == "new" && IO::get('id')) {
			$UserID = (int)IO::get('id');
			$selected_user = new User($UserID);
			$selected_user = $selected_user->data();
		}
	}


/*
# 	Seach and find users with seach terms
# 	for operators.
*/

	if(IO::exists()) {
		if(Token::check(IO::get('search_token'))) {

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'firstname' => array(
					'name'	=> 'Fornavn',	
					'minv'	=> 2,				
					'maxv'	=> 32,				
					'must'	=> true 		
				),
				'lastname'	=> array(
					'name'	=> 'Etternavn',
					'minv'	=> 2,
					'maxv'	=> 32,
					'must'	=> true
				),
				'birthday'	=> array(
					'name'	=> 'Fødselsdato',
					'minv'	=> 10,
					'maxv'	=> 10,
					'must'	=> true
				)
			));

			if ($validation->success()) {

				$firstname = strtolower(trim(strip_tags(IO::get('firstname'))));
				$lastname  = strtolower(trim(strip_tags(IO::get('lastname'))));
				$birthday  = IO::get('birthday');

				$datahandler = new User();
				$datahandler->find_users("*", array(
					"FirstName" => $firstname,
					"LastName"	=> $lastname,
					"BirthDay"	=> $birthday
				));
	
				$user_infromation = $datahandler->first();			

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
		}


		if(Token::check(IO::get('new_visning_token'))) {

			/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'customer' => array(
					'name'	=> 'Kunde',	
					'minv'	=> 1,			
					'maxv'	=> 11,			
					'must'	=> true 
				),
				'title' => array(
					'name'	=> 'Overskirft',
					'minv'	=> 2,			
					'maxv'	=> 128,			
					'must'	=> true 		
				),
				'description' => array(
					'name'	=> 'Beskrivelse',
					'minv'	=> 1,			
					'maxv'	=> 100000,			
					'must'	=> true 		
				),
				'address' => array(
					'name'	=> 'Adresse',
					'minv'	=> 5,			
					'maxv'	=> 128,			
					'must'	=> true 		
				),
				'video' => array(
					'name'	=> 'Adresse',
					'minv'	=> 1,			
					'maxv'	=> 1024,			
					'must'	=> true 		
				),
				'visning_type' => array(
					'name'	=> 'Visingstype',		
					'must'	=> true 		
				)
			));

			/*
			-	Primary image upload
			*/

				if(IO::exists("file") && $validation->success()) {
					if(!empty($_FILES['image']['name'])) {
						$files = $_FILES['image'];
						$allow = array('jpg', 'png', 'jpeg');
						$dataf = "res/uploads/visning/";

						if(!file_exists($dataf)) { 
							mkdir($dataf, 0755);
						}
						
						$file_temp = $files['tmp_name'];
						$file_size = $files['size'];
						$file_erro = $files['error'];
						$file_name = $files['name'];

						$ext = explode('.', $file_name);
						$ext = strtolower(end($ext));

						if (in_array($ext, $allow) === false) {
							$errors[] = 'Det er ingen support for filtypen du har valgt. Bare (JPG, JPEG, PNG) er tilatt.';
						}

						if (empty($errors) === true) {
							$genName = 'IN'.substr(str_shuffle(md5(uniqid().microtime())), 0, 12).'NYC';
							$imgName = $genName.'.'.$ext;
							$imgName_t = "t_" . $imgName; 
							move_uploaded_file($file_temp, $dataf.$imgName);
							create_thumbs($dataf.$imgName, $dataf.$imgName_t, $ext, '720', '428');
						}
					} 
				}

			/*
			-	more images 
			*/
				if(IO::exists("file") && $validation->success()) {
					if (!empty($_FILES['images']['name'][0])) {
						
						// 	Creating Variabels
						$files 				= $_FILES['images'];
						$allowed 			= array("jpg", "jpeg", "png");
						$uploads			= 'res/uploads/visning/';
						$uploaded_files 	= array();
						$completed_files 	= '';

						//	Looping trough the files to upload
						foreach($files['name'] as $key => $name) {		
							//	Creating variables
							$file_temp = $files['tmp_name'][$key];
							$file_size = $files['size'][$key];
							$file_erro = $files['error'][$key];

							//	Getting the file extenesion
							$file_extension = explode('.', $name);
							$file_extension = strtolower(end($file_extension));

							//	Error checking for file extension
							if (in_array($file_extension, $allowed) === false) {
								$errors[] = 'Denne filen: {$name} støttes ikke!';
							}

							//	makeing ready for upload
							if(empty($errors)) {
								$image = 'IN'.substr(str_shuffle(md5(uniqid().microtime())), 0, 12).'NYC';
								$image = $image.'.'.$file_extension;
								$image_thumb = 't_'.$image;

								move_uploaded_file($file_temp, $uploads.$image);
								create_thumbs($uploads.$image, $uploads.$image_thumb, $file_extension, 720, 428);
								$uploaded_files[] = $image;
							}
						}
					}
				}


			/*
			-	Insert details
			*/

			if ($validation->success()) {				
				$handler = new Put();

				$price 			= (empty(IO::get('price'))) ? NULL : IO::get('price');
				$totalprice 	= (empty(IO::get('totalprice'))) ? NULL : IO::get('totalprice');
				$buildyear 		= (empty(IO::get('buildyear'))) ? NULL : IO::get('buildyear');
				$appraisedvalue = (empty(IO::get('appraisedvalue'))) ? NULL : IO::get('appraisedvalue');
				$commondept 	= (empty(IO::get('commondept'))) ? NULL : IO::get('commondept');
				$commonwealth 	= (empty(IO::get('commonwealth'))) ? NULL : IO::get('commonwealth');
				$commoncosts 	= (empty(IO::get('sharedcosts'))) ? NULL : IO::get('sharedcosts');
				$loanvalue 		= (empty(IO::get('loanvalue'))) ? NULL : IO::get('loanvalue');
				$area 			= (empty(IO::get('area'))) ? NULL : IO::get('area');
				$bruttoareal 	= (empty(IO::get('bruttoareal'))) ? NULL : IO::get('bruttoareal');
				$bruksareal 	= (empty(IO::get('bruksareal'))) ? NULL : IO::get('bruksareal');
				$areapeimary 	= (empty(IO::get('areapeimary'))) ? NULL : IO::get('areapeimary');
				$plot 			= (empty(IO::get('plot'))) ? NULL : IO::get('plot');
				$roms 			= (empty(IO::get('roms'))) ? NULL : IO::get('roms');
				$bedroms 		= (empty(IO::get('bedroms'))) ? NULL : IO::get('bedroms');
				$bathroms 		= (empty(IO::get('bathroms'))) ? NULL : IO::get('bathroms');

				try {
					$handler->create('annonse', array(
						"UserID" 		=> $_init_uzer_data->UserID,
						"Type" 			=> IO::get('visning_type'),
						"CustomerID"	=> IO::get('customer'),
						"Title"			=> strip_tags(IO::get('title')),
						"Address"		=> strip_tags(IO::get('address')),
						"Description"	=> strip_tags(IO::get('description')),
						"Video"			=> strip_tags(IO::get('video')),
						"GoogleMaps"	=> strip_tags(IO::get('address')),
						"Keywords"		=> strip_tags(IO::get('address')) . " " .strip_tags(IO::get('title')). " " .strip_tags(IO::get('description')),
						"Price"			=> $price,
						"TotalPrice"	=> $totalprice,
						"BuildYear"		=> $buildyear,
						"AppraisedValue"=> $appraisedvalue,
						"CommonDept"	=> $commondept,
						"CommonWealth"	=> $commonwealth,
						"SharedCosts"	=> $commoncosts,
						"LoanValue"		=> $loanvalue,
						"Area"			=> $area,
						"AreaOfGross"	=> $bruttoareal,
						"AreaOfUse"		=> $bruksareal,
						"AreaPrimary"	=> $areapeimary,
						"Plot"			=> $plot,
						"Roms"			=> $roms,
						"BedRoms"		=> $bedroms,
						"BathRoms"		=> $bathroms,
						"BuildingType"	=> IO::get('buildingtype'),
						"Ownership"		=> IO::get('ownership')
					));

					$AnnonseID = $handler->lastid();

					if(!empty($imgName)) {
						$handler->create('images', array(
							"UserID"	=> $_init_uzer_data->UserID,
							"AnnonseID"	=> $AnnonseID,
							"Path"		=> $imgName,
							"Prim"		=> 1
						));
					}

					if(!empty($uploaded_files)) {
						for ($i=0; $i < sizeof($uploaded_files); $i++) { 
							$handler->create('images', array(
								"UserID"	=> $_init_uzer_data->UserID,
								"AnnonseID"	=> $AnnonseID,
								"Path"		=> $uploaded_files[$i]
							));
						}
					}

					/*
					- adding log of whats done.
					*/

					$LoggText = date("d-m-Y") . " ";
					$LoggText .= $_init_uzer_data->FirstName . " " . $_init_uzer_data->LastName;
					$LoggText .= " Opprettet annonse med id: ". $AnnonseID;

					$handler->create('changelogs', array(
						"LoggText" => $LoggText
					));

					Session::flash('visning', 'Visningen er lagret.');
					Redirect::to('/kampanje/vis/'. $AnnonseID .'/');

				} catch (Exception $e) {
					$errors[] = $e->getMessage();
				}

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
		}

		if(Token::check(IO::get('update_visning_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'customer' => array(
					'name'	=> 'Kunde',	
					'minv'	=> 1,			
					'maxv'	=> 11,			
					'must'	=> true 
				),
				'title' => array(
					'name'	=> 'Overskirft',
					'minv'	=> 2,			
					'maxv'	=> 128,			
					'must'	=> true 		
				),
				'description' => array(
					'name'	=> 'Beskrivelse',
					'minv'	=> 1,			
					'maxv'	=> 100000,			
					'must'	=> true 		
				),
				'address' => array(
					'name'	=> 'Adresse',
					'minv'	=> 5,			
					'maxv'	=> 128,			
					'must'	=> true 		
				),
				'video' => array(
					'name'	=> 'Adresse',
					'minv'	=> 1,			
					'maxv'	=> 1024,			
					'must'	=> true 		
				),
				'visning_type' => array(
					'name'	=> 'Visingstype',		
					'must'	=> true 		
				)
			));

			/*
			-	Primary image upload
			*/

				if(IO::exists("file") && $validation->success()) {
					if(!empty($_FILES['image']['name'])) {
						$files = $_FILES['image'];
						$allow = array('jpg', 'png', 'jpeg');
						$dataf = "res/uploads/visning/";

						if(!empty(IO::get('old_image'))) {
							unlink($dataf.IO::get('old_image'));
							unlink($dataf.'t_'.IO::get('old_image'));
							$del = new Del();
							$del->delete("images", IO::get('old_image_id'), "ImageID");
						}

						
						$file_temp = $files['tmp_name'];
						$file_size = $files['size'];
						$file_erro = $files['error'];
						$file_name = $files['name'];

						$ext = explode('.', $file_name);
						$ext = strtolower(end($ext));

						if (in_array($ext, $allow) === false) {
							$errors[] = 'Det er ingen support for filtypen du har valgt. Bare (JPG, JPEG, PNG) er tilatt.';
						}

						if (empty($errors) === true) {
							$genName = 'IN'.substr(str_shuffle(md5(uniqid().microtime())), 0, 12).'NYC';
							$imgName = $genName.'.'.$ext;
							$imgName_t = "t_" . $imgName; 
							move_uploaded_file($file_temp, $dataf.$imgName);
							create_thumbs($dataf.$imgName, $dataf.$imgName_t, $ext, '720', '428');
						}
					} 
				}

			/*
			-	more images 
			*/
				if(IO::exists("file") && $validation->success()) {
					if (!empty($_FILES['images']['name'][0])) {
						
						// 	Creating Variabels
						$files 				= $_FILES['images'];
						$allowed 			= array("jpg", "jpeg", "png");
						$uploads			= 'res/uploads/visning/';
						$uploaded_files 	= array();
						$completed_files 	= '';

						//	Looping trough the files to upload
						foreach($files['name'] as $key => $name) {		
							//	Creating variables
							$file_temp = $files['tmp_name'][$key];
							$file_size = $files['size'][$key];
							$file_erro = $files['error'][$key];

							//	Getting the file extenesion
							$file_extension = explode('.', $name);
							$file_extension = strtolower(end($file_extension));

							//	Error checking for file extension
							if (in_array($file_extension, $allowed) === false) {
								$errors[] = 'Denne filen: {$name} støttes ikke!';
							}

							//	makeing ready for upload
							if(empty($errors)) {
								$image = 'IN'.substr(str_shuffle(md5(uniqid().microtime())), 0, 12).'NYC';
								$image = $image.'.'.$file_extension;
								$image_thumb = 't_'.$image;

								move_uploaded_file($file_temp, $uploads.$image);
								create_thumbs($uploads.$image, $uploads.$image_thumb, $file_extension, 720, 428);
								$uploaded_files[] = $image;
							}
						}
					}
				}


			/*
			-	Insert details
			*/

			if ($validation->success() && empty($errors)) {
				$handler = new Put();

				$price 			= (empty(IO::get('price'))) ? NULL : IO::get('price');
				$totalprice 	= (empty(IO::get('totalprice'))) ? NULL : IO::get('totalprice');
				$buildyear 		= (empty(IO::get('buildyear'))) ? NULL : IO::get('buildyear');
				$appraisedvalue = (empty(IO::get('appraisedvalue'))) ? NULL : IO::get('appraisedvalue');
				$commondept 	= (empty(IO::get('commondept'))) ? NULL : IO::get('commondept');
				$commonwealth 	= (empty(IO::get('commonwealth'))) ? NULL : IO::get('commonwealth');
				$commoncosts 	= (empty(IO::get('sharedcosts'))) ? NULL : IO::get('sharedcosts');
				$loanvalue 		= (empty(IO::get('loanvalue'))) ? NULL : IO::get('loanvalue');
				$area 			= (empty(IO::get('area'))) ? NULL : IO::get('area');
				$bruttoareal 	= (empty(IO::get('bruttoareal'))) ? NULL : IO::get('bruttoareal');
				$bruksareal 	= (empty(IO::get('bruksareal'))) ? NULL : IO::get('bruksareal');
				$areapeimary 	= (empty(IO::get('areapeimary'))) ? NULL : IO::get('areapeimary');
				$plot 			= (empty(IO::get('plot'))) ? NULL : IO::get('plot');
				$roms 			= (empty(IO::get('roms'))) ? NULL : IO::get('roms');
				$bedroms 		= (empty(IO::get('bedroms'))) ? NULL : IO::get('bedroms');
				$bathroms 		= (empty(IO::get('bathroms'))) ? NULL : IO::get('bathroms');

				try {
					$handler->update('annonse', array(
						"UserID" 		=> $_init_uzer_data->UserID,
						"Type" 			=> IO::get('visning_type'),
						"Title"			=> strip_tags(IO::get('title')),
						"Address"		=> strip_tags(IO::get('address')),
						"Description"	=> strip_tags(IO::get('description')),
						"Video"			=> strip_tags(IO::get('video')),
						"GoogleMaps"	=> strip_tags(IO::get('address')),
						"Keywords"		=> strip_tags(IO::get('address')) . " " .strip_tags(IO::get('title')). " " .strip_tags(IO::get('description')),
						"Price"			=> $price,
						"TotalPrice"	=> $totalprice,
						"BuildYear"		=> $buildyear,
						"AppraisedValue"=> $appraisedvalue,
						"CommonDept"	=> $commondept,
						"CommonWealth"	=> $commonwealth,
						"SharedCosts"	=> $commoncosts,
						"LoanValue"		=> $loanvalue,
						"Area"			=> $area,
						"AreaOfGross"	=> $bruttoareal,
						"AreaOfUse"		=> $bruksareal,
						"AreaPrimary"	=> $areapeimary,
						"Plot"			=> $plot,
						"Roms"			=> $roms,
						"BedRoms"		=> $bedroms,
						"BathRoms"		=> $bathroms,
						"BuildingType"	=> IO::get('buildingtype'),
						"Ownership"		=> IO::get('ownership')
					), IO::get('update_id'), "AnnonseID");


					if(!empty($imgName)) {
						$handler->create('images', array(
							"UserID"	=> $_init_uzer_data->UserID,
							"AnnonseID"	=> IO::get('update_id'),
							"Path"		=> $imgName,
							"Prim"		=> 1
						));
					}

					if(!empty($uploaded_files)) {
						for ($i = 0; $i < count($uploaded_files); $i++) { 
							$handler->create('images', array(
								"UserID"	=> $_init_uzer_data->UserID,
								"AnnonseID"	=> IO::get('update_id'),
								"Path"		=> $uploaded_files[$i]
							));
						}
					}

					$LoggText = date("d-m-Y") . " ";
					$LoggText .= $_init_uzer_data->FirstName . " " . $_init_uzer_data->LastName;
					$LoggText .= " Oppdatert annonse med id: ". IO::get('update_id');

					$handler->create('changelogs', array(
						"LoggText" => $LoggText
					));

					Session::flash('visning', 'Annonse er oppdatert.');
					Redirect::to('/kampanje/vis/'.IO::get('update_id').'/');

				} catch (Exception $e) {
					$errors[] = $e->getMessage();
				}


			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
			
    	}
	}


/*
-	Get info for edit
*/
	if(IO::exists('get')) {
		if(IO::get('action') == "edit") {
			if(!empty(IO::get('id')) && is_numeric(IO::get('id'))) {

				$annonse_edit = new Get();
				$annonse_edit->get_data_by_id("annonse", "AnnonseID", IO::get('id'));
				$annonse_edit = $annonse_edit->result();

				if(empty($annonse_edit)) { Redirect::to("/"); }

				$user_info = new User($annonse_edit->CustomerID);
				$user_info = $user_info->data();

				$annonse_img = new Get();
				$annonse_img->get_data_by_id("images", "AnnonseID", $annonse_edit->AnnonseID);
				$annonse_img = $annonse_img->results();

			} else {
				Redirect::to("/");
			}
		}

		if(IO::get('action') == "delete") {
			if(!empty(IO::get('id')) && is_numeric(IO::get('id'))) {

				$annonse_del = new Get();
				$annonse_del->get_data_by_id("annonse", "AnnonseID", IO::get('id'));
				$annonse_del = $annonse_del->result();

				$annonse_img_del = new Get();
				$annonse_img_del->get_data_by_id("images", "AnnonseID", $annonse_del->AnnonseID);
				$annonse_img_del = $annonse_img_del->results();

				$dataf = "res/uploads/visning/";

				foreach ($annonse_img_del as $imgdel) {
					unlink($dataf.$imgdel->Path);
					unlink($dataf.'t_'.$imgdel->Path);
				}

				
				$LoggText = date("d-m-Y") . " ";
				$LoggText .= $_init_uzer_data->FirstName . " " . $_init_uzer_data->LastName;
				$LoggText .= " Slettet annonse med id: ". $annonse_del->AnnonseID;

				$handler->create('changelogs', array(
					"LoggText" => $LoggText
				));

				$delete = new Del();
				$delete->delete("images", $annonse_del->AnnonseID, "AnnonseID");
				$delete->delete("annonse", $annonse_del->AnnonseID, "AnnonseID");

				Session::flash('index', 'Visningen er slettet.');
				Redirect::to("/?alert=info");
			}
		}
	}

