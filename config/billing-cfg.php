<?php

//	Include files:
	include_once('appfiles/core/init.php');

//	Site title:
	$site_title = "Betaling";

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if(!$_init_uzer->isOnline()) { Redirect::to("/"); }

/*
#	Get info about user.
*/

	if(IO::exists('get')) {
		if(IO::get('action') == "view" && is_numeric(IO::get('id'))) {

			//	Site title:
			$site_title = "Se faktura";

			$datahandler = new Get();
			$datahandler->get_data_by_id("contracts", "ContractID", IO::get('id'));
			$this_bill = $datahandler->result();

			if(!empty($this_bill) && $this_bill->BuyerID == $_init_uzer_data->UserID || $this_bill->SellerID == $_init_uzer_data->UserID) {
				$seller = new User($this_bill->SellerID);
				$seller = $seller->data();

				$buyer = new User($this_bill->BuyerID);
				$buyer = $buyer->data();

				$money = ($this_bill->Price * 100);
			} else {
				Redirect::to('/billing');
			}
		}	
	} 

	if(!IO::exists('get')) {
		$datahandler = new Get();
		$datahandler->get_data_by_id("contracts", "BuyerID", $_init_uzer_data->UserID);
		$my_bills = $datahandler->results();
	}

	if(IO::exists('get')) {
		if(!empty(IO::get('action'))) {
			if(IO::get('action') == "operator") {
				//	Site title:
				$site_title = "Betaling";

				$datahandler = new Get();
				$datahandler->get_data_by_id("contracts", "SellerID", $_init_uzer_data->UserID);
				$seller_bills = $datahandler->results();
			}

			if(IO::get('action') == "delete" && is_numeric(IO::get('id'))) {
				$del = new Del();
				$del->delete("contracts", IO::get('id'), "ContractID");

				$LoggText = date("d-m-Y") . " ";
				$LoggText .= $_init_uzer_data->FirstName . " " . $_init_uzer_data->LastName;
				$LoggText .= " Slettet avtale: ". IO::get('id');

				$handler = new Put();
				$handler->create('changelogs', array(
					"LoggText" => $LoggText
				));

				Session::flash('billing', 'Avtalen er nå slettet.');
				Redirect::to('/billing/operator/');
			}
		}
	}