<?php

//	Include files:
	include_once('appfiles/core/init.php');

//	Site title:
	$site_title = "Kontakt oss";

if(IO::exists()) {
	if(Token::check(IO::get('message_token'))) {

		/*
		#	This will connect to the reCAPTCHA server runned
		#	by www.google.com and check if the user have confrimed
		#	that they are not a bot. We get a response as a json
		#	so we cah check that by our server.
		*/

		$site_url = "https://www.google.com/recaptcha/api/siteverify";
		$site_key = "6LeLZwcUAAAAAOdO3-sNHRgPwMwK4l8sAjRK7AUn";
		$site_unx = $site_url . "?secret=" . $site_key . "&response=" . IO::get('g-recaptcha-response') . "&remoteip=" . $_SERVER['REMOTE_ADDR'];
		$response = file_get_contents($site_unx);
		$data = json_decode($response);

		/*
		#	checking other form validation before sending a message
		# 	form the submitted form.
		*/

		$validate = new Validate();
		$validation = $validate->validator($_POST, array(
			'fullname' => array(
				'name'	=> 'Fulltnavn',	
				'minv'	=> 4,				
				'maxv'	=> 128,				
				'must'	=> true 		
			),
			'emailaddress'	=> array(
				'name'	=> 'E-post adresse',
				'minv'	=> 7,
				'maxv'	=> 128,
				'must'	=> true
			),
			'subject'	=> array(
				'name'	=> 'Emne',
				'minv'	=> 7,
				'maxv'	=> 128,
				'must'	=> true
			),
			'message'	=> array(
				'name'	=> 'Melding',
				'minv'	=> 20,
				'maxv'	=> 8192,
				'must'	=> true
			)
		));

		if ($validation->success()) {

			if($data->success) {

				$reciver = strip_tags(IO::get('emailaddress'));
				$message = strip_tags(IO::get('message'));
				$subject = strip_tags(IO::get('subject'));
				$fullname = strip_tags(IO::get('fullname'));

				$LoggText = date("d-m-Y") . " Person: ";
				$LoggText .= $fullname;
				$LoggText .= " Sendte en melding til kundeservice: ";

				$handler = new Put();
				$handler->create('changelogs', array(
					"LoggText" => $LoggText
				));

				customerServiceMail($reciver, $message, $subject, $fullname);
				Redirect::to("/contact/success/");
			
			} else {
				$errors[] = "Du må bekrefte at du ikke er en robot.";
			}

		} else {
			foreach($validation->errors() as $err) {
				$errors[] = $err;
			}
		}

	}
}