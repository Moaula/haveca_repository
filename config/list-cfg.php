<?php

//	Include files:
	include_once('appfiles/core/init.php');


//	Site title:
	$site_title = "Mine visninger";

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if(!$_init_uzer->isOnline()) { Redirect::to("/"); }
    if(!$_init_uzer->hasPerm('operator')) { Redirect::to("/"); }

/*
-	Load users items from databse
*/
	if(IO::exists('get')) {
		if(!empty(IO::get('action')) && IO::get('action') == "visning") {
			$visninger = new Get();
			$visninger->get_data_by_id("annonse", "UserID", $_init_uzer_data->UserID, "DESC");
			$visninger = $visninger->results();
		}
	}
	