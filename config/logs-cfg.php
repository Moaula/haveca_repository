<?php

//	Include files:
	include_once('appfiles/core/init.php');

//	Site title:
	$site_title = "Handlings logg";

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if(!$_init_uzer->isOnline()) { Redirect::to("/"); }
    if(!$_init_uzer->hasPerm('admin')) { Redirect::to("/"); }

/*
-	Get Logfiles
*/
	$loggdata = new Get();
	$loggdata->get_data("changelogs", "LogID", "DESC", 10000);
	$loggdata = $loggdata->results();