<?php

//	Include files:
	include_once('appfiles/core/init.php');

//	Site title:
	$site_title = "Meldinger";

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if(!$_init_uzer->isOnline()) { Redirect::to("/login"); }


	if(IO::exists('get')) {
		if(IO::get('action') == "connect" && is_numeric(IO::get('id'))) { 
			$get_user = new User(IO::get('id'));
			$get_user = $get_user->data();
		}

		if(IO::get('action') == "delete" && is_numeric(IO::get('id'))) {
			if($_init_uzer->check_conversations(IO::get('id'))) {
				$_init_uzer->delete_conversations(IO::get('id'));
			} else {
				Redirect::to("/messagesa");
			}
		}

		if(IO::get('action') == "view" && is_numeric(IO::get('id'))) {
			if($_init_uzer->check_conversations(IO::get('id'))) {
				$dataMessages = new Get();
				$dataMessages->get_data_msg("conversations_message", "ConversationID", IO::get('id'), "DESC");
				$dataMessages = $dataMessages->results();

				$dataConversation = new Get();
				$dataConversation->get_data_by_id("conversations", "ConversationID", IO::get('id'));
				$dataConversation = $dataConversation->result();

				$_init_uzer->messages_read_update(IO::get('id'));
			}
		}

	}

	if(!IO::exists('get')) {
		$conversations = new User();
		$conversations->conversations();
		$conversations = $conversations->results();
	}


/*
-	Now we will go trough the posted input values and validate them with the validate method.
-	First we will check if there is any input posted, then we will check if the input is posted by the client or 
-	if some one is trying to post the data on behaf of someone else wish is not allowed. Therefor we have the token
-	to check for CSRF (cross site request forgery).
*/

	if(IO::exists()) {
    	if(Token::check(IO::get('msg_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'reciver' => array(
					'name'	=> 'Mottaker',		// if any errors display this name.
					'minv'	=> 1,				// the field required at least 1 characters.
					'maxv'	=> 11,				// the field can not hold more than 11 characters.
					'must'	=> true 			// the field can not be empty or blank.
				),
				'message'	=> array(
					'name'	=> 'Melding',
					'minv'	=> 1,
					'maxv'	=> 4096,
					'must'	=> true
				),
				'subject'	=> array(
					'name'	=> 'Emne',
					'minv'	=> 1,
					'maxv'	=> 128,
					'must'	=> true
				)
			));

			
			if ($validation->success()) {

				$datahandler = new Put();
				try {

					$datahandler->create("conversations", array(
						"Tema"		=> strip_tags(IO::get('subject')),
						"Reciver"	=> (int)IO::get('reciver'),
						"Sender"	=> $_init_uzer_data->UserID,
						"Date"		=> date('Y-m-d h:i:s', time())
					));
					
					$lastid = $datahandler->lastid();

					$datahandler->create("conversations_message", array(
						"ConversationID"	=> $lastid,
						"Sender"			=> $_init_uzer_data->UserID,
						"Date"				=> date('Y-m-d h:i:s', time()),
						"Content"			=> strip_tags(IO::get('message'))
					));

					$datahandler->create("conversations_users", array(
						"ConversationID"	=> $lastid,
						"UserID"			=> $_init_uzer_data->UserID,
						"Deleted"			=> 0,
						"URead"				=> date('Y-m-d h:i:s', time())
					));

					$datahandler->create("conversations_users", array(
						"ConversationID"	=> $lastid,
						"UserID"			=> IO::get('reciver'),
						"Deleted"			=> 0,
						"URead"				=> date('Y-m-d h:i:s', time())
					));

					$LoggText = date("d-m-Y") . " Bruker: ";
					$LoggText .= $_init_uzer_data->FirstName . " " . $_init_uzer_data->LastName;
					$LoggText .= " opprettet samtale bruker id: ". IO::get('reciver');

					$handler = new Put();
					$handler->create('changelogs', array(
						"LoggText" => $LoggText
					));


					Redirect::to("/messages");

				} catch (Exception $e) {	
					$errors[] = $e->getMessage();
				}


			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}

    	if(Token::check(IO::get('reply_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'message'	=> array(
					'name'	=> 'Melding',
					'minv'	=> 1,
					'maxv'	=> 4096,
					'must'	=> true
				)
			));

			
			if ($validation->success()) {

				$datahandler = new Put();
				try {

					$datahandler->create("conversations_message", array(
						"ConversationID"	=> IO::get('convid'),
						"Sender"			=> $_init_uzer_data->UserID,
						"Date"				=> date('Y-m-d h:i:s', time()),
						"Content"			=> strip_tags(IO::get('message'))
					));

					$LoggText = date("d-m-Y") . " Bruker: ";
					$LoggText .= $_init_uzer_data->FirstName . " " . $_init_uzer_data->LastName;
					$LoggText .= " sendte melding i samtale id: ". IO::get('convid');

					$handler = new Put();
					$handler->create('changelogs', array(
						"LoggText" => $LoggText
					));


					Redirect::to("/messages/view/". IO::get('convid') ."/");

				} catch (Exception $e) {	
					$errors[] = $e->getMessage();
				}

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}
    }