<?php

//	Include files:
	include_once('appfiles/core/init.php');

//	Site title:
	$site_title = "Opprett avtale";

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if(!$_init_uzer->isOnline()) { Redirect::to("/"); }
    if(!$_init_uzer->hasPerm('operator')) { Redirect::to("/"); }

/*
#	Get info about user.
*/

	if(IO::exists('get')) {
		if(IO::get('action') == "new" && is_numeric(IO::get('id'))) {
			$userid = (int)IO::get('id');
			$userdata = new User($userid);
			$userdata = $userdata->data();
		}	
	} 

/*
# 	Seach and find users with seach terms
# 	for operators.
*/

	if(IO::exists()) {
		if(Token::check(IO::get('search_token'))) {

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'firstname' => array(
					'name'	=> 'Fornavn',	
					'minv'	=> 2,				
					'maxv'	=> 32,				
					'must'	=> true 		
				),
				'lastname'	=> array(
					'name'	=> 'Etternavn',
					'minv'	=> 2,
					'maxv'	=> 32,
					'must'	=> true
				),
				'birthday'	=> array(
					'name'	=> 'Fødselsdato',
					'minv'	=> 10,
					'maxv'	=> 10,
					'must'	=> true
				)
			));

			if ($validation->success()) {

				$firstname = strtolower(trim(strip_tags(IO::get('firstname'))));
				$lastname  = strtolower(trim(strip_tags(IO::get('lastname'))));
				$birthday  = IO::get('birthday');

				$datahandler = new User();
				$datahandler->find_users("*", array(
					"FirstName" => $firstname,
					"LastName"	=> $lastname,
					"BirthDay"	=> $birthday
				));
	
				$user_infromation = $datahandler->first();				

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
		}

		if(Token::check(IO::get('create_bill_token'))) {

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'contract_description' => array(
					'name'	=> 'Avtale Beskrivelse',	
					'minv'	=> 50,				
					'maxv'	=> 8192,				
					'must'	=> true 		
				),
				'billing_contract_description'	=> array(
					'name'	=> 'Faktura Beskrivelse',
					'minv'	=> 10,
					'maxv'	=> 2048,
					'must'	=> true
				),
				'price'	=> array(
					'name'	=> 'Pris',
					'minv'	=> 1,
					'maxv'	=> 11,
					'must'	=> true
				)
			));

			if ($validation->success()) {
				$datahandler = new Put();
				$price = (int)IO::get('price');
				try {
					$datahandler->create("contracts", array(
						"SellerID"	=> $_init_uzer_data->UserID,
						"BuyerID"	=> $userdata->UserID,
						"ContractDetails" => strip_tags(IO::get('contract_description')),
						"BillingDetails" => strip_tags(IO::get('billing_contract_description')),
						"Price" => $price,
						"BillDate" => date('Y-m-d')
					));

					$LoggText = date("d-m-Y") . " Bruker: ";
					$LoggText .= $_init_uzer_data->FirstName . " " . $_init_uzer_data->LastName;
					$LoggText .= " opprettet avtale for bruker id: ". $userdata->UserID;

					$handler = new Put();
					$handler->create('changelogs', array(
						"LoggText" => $LoggText
					));

					Session::flash('billing', "Fakturaen er opprettet.");
					Redirect::to('/billing/operator/');

				} catch (Exception $e) {
					$errors[] = $e->getMessage();
				}

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
		}
	}