<?php

//	Include files:
	include_once('appfiles/core/init.php');

/*
-	Get data to show the operator selected
-	check what form a user needs to show dat as.
*/

	if(IO::exists('get')) {
		if(IO::get('action') === "show") {
			if(is_numeric(IO::get('id'))) {

				$show_data = new User(IO::get('id'));
				$show_data = $show_data->data();

				if(empty($show_data)) { Redirect::to('/'); }
				if($show_data->UserGroup < 2) { Redirect::to('/'); }	
				if($show_data->Active !== "1") { Redirect::to('/'); }

				//	Site title:
				$site_title = "Dronefotograf - ". ucfirst($show_data->FirstName) . ' ' . ucfirst($show_data->LastName);

				$show_presentations = new Get();
				$show_presentations->get_data_by_id('annonse', $identifier = "UserID", $show_data->UserID, $order = "DESC", $limit = 3);
				$show_presentations = $show_presentations->results();

				/*
				-	Get data for what a dronephotographer can do
				*/
					$geth = new Get();
					$geth->get_data_by_id('skills', "UserID", $show_data->UserID);
					$checkdata = $geth->result();
			}
		}
	}