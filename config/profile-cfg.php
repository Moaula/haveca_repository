<?php

//	Include files:
	include_once('appfiles/core/init.php');
//	Site title:
	$site_title = "Min profil";

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if(!$_init_uzer->isOnline()) { Redirect::to("/"); }


/*
-	Now we will go trough the posted 
-	input values and validate them
-	with the validate method.
-	First we will check if there is any input posted, then we will check if the input is posted by the client or 
-	if some one is trying to post the data on behaf of someone else wish is not allowed. Therefor we have the token
-	to check for CSRF (cross site request forgery).
*/
	
    if(IO::exists()) {
    	if(Token::check(IO::get('update_data_operator'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'firstname' => array(
					'name'	=> 'Fornavn',	// if any errors display this name.
					'minv'	=> 2,			// the field required at least 2 characters.
					'maxv'	=> 32,			// the field can not hold more than 32 characters.
					'must'	=> true 		// the field can not be empty or blank.
				),
				'lastname'	=> array(
					'name'	=> 'Etternavn',
					'minv'	=> 2,
					'maxv'	=> 32,
					'must'	=> true
				),
				'phonenumber'	=> array(
					'name'	=> 'Telefonnummer',
					'minv'	=> 8,
					'maxv'	=> 12,
					'must'	=> true
				),
				'address'	=> array(
					'name'	=> 'Adresse',
					'must'	=> true
				),
				'ZipPlace'	=> array(
					'name'	=> 'Poststed',
					'must'	=> true
				),
				'ZipCode'	=> array(
					'name'	=> 'Postnummer',
					'minv'	=> 4,
					'maxv'	=> 4,
					'must'	=> true
				),
				'orgnumber'	=> array(
					'name'	=> 'Org.Nummer',
					'minv'	=> 9,
					'maxv'	=> 20,
					'must'	=> true
				)
			));

			if(IO::exists("file")) {
				if(!empty($_FILES['image']['name'])) {
					$files = $_FILES['image'];
					$allow = array('jpg', 'png', 'jpeg');
					$dataf = "res/uploads/";

					if(!file_exists($dataf)) { 
						mkdir($dataf, 0755);
					}

					if(!empty(IO::get('old_image'))) {
						unlink($dataf.IO::get('old_image'));
						unlink($dataf.'t_'.IO::get('old_image'));
					}
					
					$file_temp = $files['tmp_name'];
					$file_size = $files['size'];
					$file_erro = $files['error'];
					$file_name = $files['name'];

					$ext = explode('.', $file_name);
					$ext = strtolower(end($ext));

					if (in_array($ext, $allow) === false) {
						$errors[] = 'Det er ingen support for fil typen du har valgt. Bare (JPG, JPEG, PNG) er tilatt.';
					}

					if (empty($errors) === true) {
						$genName = 'IN'.rand(1111111, 9999999).'NYC';
						$imgName = $genName.'.'.$ext;
						$imgName_t = "t_" . $imgName; 
						move_uploaded_file($file_temp, $dataf.$imgName);
						create_thumbs($dataf.$imgName, $dataf.$imgName_t, $ext, '720', '428');
					}
				} else {
					$imgName = IO::get('old_image');
				}
			} else {
				$imgName = IO::get('old_image');
			}


			/*
			-	if the validation process is successfull
			-	then we can continue to addint the information 
			-	to the database. 
			-	But however first we check if the user have agreed to 
			-	Terms Of Service
			*/

			
			if ($validation->success()) {

				if(Encrypt::make(IO::get('password'), $_init_uzer_data->SLS) === $_init_uzer_data->Password) {
			
					/*
					-	-------------------------
					-	Connecting to the database
					-	and getteing ready for insertion.
					*/
						$user = new User();
						$puth = new Put();

					/*
					-	commit - db changes.
					*/
						try {
							$user->update(array(
								'FirstName'		=> strip_tags(mb_strtolower(IO::get('firstname'), 'UTF-8')),
								'LastName'		=> strip_tags(mb_strtolower(IO::get('lastname'), 'UTF-8')),
								'Address'		=> strip_tags(mb_strtolower(IO::get('address'), 'UTF-8')),
								'ZipPlace'		=> strip_tags(mb_strtolower(IO::get('zipplace'), 'UTF-8')),
								'ZipCode'		=> IO::get('zipcode'),
								'OrgNumber'		=> IO::get('orgnumber'),
								'UserBio'		=> strip_tags(mb_strtolower(IO::get('userbio'), 'UTF-8')),
								'PhoneNumber'	=> IO::get('phonenumber'),
								'Picture'		=> $imgName
							));


							$videos = (IO::get('videos') == "on") ? true : false;
							$images = (IO::get('images') == "on") ? true : false;
							$raw_videos = (IO::get('raw_videos') == "on") ? true : false;
							$raw_images = (IO::get('raw_images') == "on") ? true : false;
							$edited_videos = (IO::get('edited_videos') == "on") ? true : false;
							$edited_images = (IO::get('edited_images') == "on") ? true : false;

							if($puth->exists('skills', array('UserID' => $_init_uzer_data->UserID), "UserID")) {
								$puth->update('skills', array(
									'videos' => $videos,
									'images' => $images,
									'raw_videos' => $raw_videos,
									'raw_images' => $raw_images,
									'edited_videos' => $edited_videos,
									'edited_images' => $edited_images
								), $_init_uzer_data->UserID, "UserID");
							} else {
								$puth->create('skills', array(
									'UserID' => $_init_uzer_data->UserID,
									'videos' => $videos,
									'images' => $images,
									'raw_videos' => $raw_videos,
									'raw_images' => $raw_images,
									'edited_videos' => $edited_videos,
									'edited_images' => $edited_images
								));
							}

							$LoggText = date("d-m-Y") . " Bruker: ";
							$LoggText .= $_init_uzer_data->FirstName . " " . $_init_uzer_data->LastName;
							$LoggText .= " opprettet sin profil ";

							$handler = new Put();
							$handler->create('changelogs', array(
								"LoggText" => $LoggText
							));

							/*
							-	Redirect the user.
							*/
								Session::flash('profile', 'Dine detaljer er oppdatert.');
								Redirect::to('profile');

						} catch (Exception $e) {
							$errors[] = $e->getMessage();
						}

				} else {
					$errors[] = "Du må skrive inn ditt passord for å oppdatere detaljene.";
				}

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}

    	if(Token::check(IO::get('update_data_kunde'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'firstname' => array(
					'name'	=> 'Fornavn',	// if any errors display this name.
					'minv'	=> 2,			// the field required at least 2 characters.
					'maxv'	=> 32,			// the field can not hold more than 32 characters.
					'must'	=> true 		// the field can not be empty or blank.
				),
				'lastname'	=> array(
					'name'	=> 'Etternavn',
					'minv'	=> 2,
					'maxv'	=> 32,
					'must'	=> true
				),
				'phonenumber'	=> array(
					'name'	=> 'Telefonnummer',
					'minv'	=> 8,
					'maxv'	=> 12,
					'must'	=> true
				),
				'address'	=> array(
					'name'	=> 'Adresse',
					'must'	=> true
				),
				'ZipPlace'	=> array(
					'name'	=> 'Poststed',
					'must'	=> true
				),
				'ZipCode'	=> array(
					'name'	=> 'Postnummer',
					'minv'	=> 4,
					'maxv'	=> 4,
					'must'	=> true
				)
			));

			/*
			-	if the validation process is successfull
			-	then we can continue to addint the information 
			-	to the database. 
			-	But however first we check if the user have agreed to 
			-	Terms Of Service
			*/

			
			if ($validation->success()) {

				if(Encrypt::make(IO::get('password'), $_init_uzer_data->SLS) === $_init_uzer_data->Password) {
			
					/*
					-	-------------------------
					-	Connecting to the database
					-	and getteing ready for insertion.
					*/
						$user = new User();

					/*
					-	commit - db changes.
					*/
						try {
							$user->update(array(
								'FirstName'		=> strip_tags(mb_strtolower(IO::get('firstname'), 'UTF-8')),
								'LastName'		=> strip_tags(mb_strtolower(IO::get('lastname'), 'UTF-8')),
								'Address'		=> strip_tags(mb_strtolower(IO::get('address'), 'UTF-8')),
								'ZipPlace'		=> strip_tags(mb_strtolower(IO::get('zipplace'), 'UTF-8')),
								'ZipCode'		=> IO::get('zipcode'),
								'PhoneNumber'	=> IO::get('phonenumber')
							));


							$LoggText = date("d-m-Y") . " Bruker: ";
							$LoggText .= $_init_uzer_data->FirstName . " " . $_init_uzer_data->LastName;
							$LoggText .= " opprettet sin profil ";

							$handler = new Put();
							$handler->create('changelogs', array(
								"LoggText" => $LoggText
							));

							/*
							-	Redirect the user.
							*/
								Session::flash('profile', 'Dine detaljer er oppdatert.');
								Redirect::to('profile');

						} catch (Exception $e) {
							$errors[] = $e->getMessage();
						}

				} else {
					$errors[] = "Du må skrive inn ditt passord for å oppdatere detaljene.";
				}

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}
    }

/*
-	Get data for what a dronephotographer can do
*/
	$geth = new Get();
	$geth->get_data_by_id('skills', "UserID", $_init_uzer_data->UserID);
	$checkdata = $geth->result();
