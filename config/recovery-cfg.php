<?php

//	Include files:
	include_once('appfiles/core/init.php');
//	Site title:
	$site_title = "Hjelp: Gjenoppretting";

/*
-	Checking for input to continue the request
-	of recovering username or password.
*/

	if(IO::exists('get')){
		if(Token::check(IO::get('recover_username_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'firstname' => array(
					'name'	=> 'Fornavn',	// if any errors display this name.
					'minv'	=> 2,				// the field required at least 6 characters.
					'maxv'	=> 128,				// the field can not hold more than 128 characters.
					'must'	=> true 			// the field can not be empty or blank.
				),
				'lastname'	=> array(
					'name'	=> 'Etternavn',
					'minv'	=> 2,
					'maxv'	=> 128,
					'must'	=> true
				),
				'birthday'	=> array(
					'name'	=> 'Fødseslsdato',
					'minv'	=> 10,
					'maxv'	=> 10,
					'must'	=> true
				)
			));

			/*
			-	
			*/

			
			if ($validation->success()) {

				$LoggText = date("d-m-Y") . " Bruker: ";
				$LoggText .= IO::get('firstname'). " " . IO::get('lastname');
				$LoggText .= " forsøker å gjenopprette brukernavn ";

				$handler = new Put();
				$handler->create('changelogs', array(
					"LoggText" => $LoggText
				));
				
				$user = new User();
				$user->recovery(IO::get('firstname'), IO::get('lastname'), IO::get('birthday'), IO::get('emailaddress'));

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}

    	if(Token::check(IO::get('recover_password_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'emailaddress' => array(
					'name'	=> 'E-post adresse',		// if any errors display this name.
					'minv'	=> 7,				// the field required at least 6 characters.
					'maxv'	=> 128,				// the field can not hold more than 128 characters.
					'must'	=> true 			// the field can not be empty or blank.
				)
			));

			/*
			-	
			*/

			
			if ($validation->success()) {

				$LoggText = date("d-m-Y") . " Bruker: ";
				$LoggText .= IO::get('firstname'). " " . IO::get('lastname');
				$LoggText .= " forsøker å gjenopprette passord ";

				$handler = new Put();
				$handler->create('changelogs', array(
					"LoggText" => $LoggText
				));

				$user = new User();
				$user->pwrecovery(IO::get('emailaddress'));

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}
	}