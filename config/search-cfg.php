<?php

//	Include files:
	include_once('appfiles/core/init.php');
//	Site title:
	$site_title = "Søk";

/*
-	Do the search
*/
	if(IO::exists('get')) {
		$query = IO::get('qry');
		$query = explode(" ", $query);

		$search = new Get();
		$search->search_db("annonse", $query, "keywords", "Description");
		$visninger = $search->results();
		$visninger_count = $search->count();

		$search_operators = new Get();
		$search_operators->search_db("users", $query, "FirstName", "LastName");
		$operators = $search_operators->results();
		$operators_count = $search_operators->count();
	}