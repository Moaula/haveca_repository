<?php

//	Include files:
	include_once('appfiles/core/init.php');
//	Site title:
	$site_title = "Registering";

/*
-	Now we will go trough the posted 
-	input values and validate them
-	with the validate method.
-	First we will check if there is any input posted, then we will check if the input is posted by the client or 
-	if some one is trying to post the data on behaf of someone else wish is not allowed. Therefor we have the token
-	to check for CSRF (cross site request forgery).
*/
	
    if(IO::exists()) {
    	if(Token::check(IO::get('reg_kunde_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'firstname' => array(
					'name'	=> 'Fornavn',	// if any errors display this name.
					'minv'	=> 2,			// the field required at least 2 characters.
					'maxv'	=> 32,			// the field can not hold more than 32 characters.
					'must'	=> true 		// the field can not be empty or blank.
				),
				'lastname'	=> array(
					'name'	=> 'Etternavn',
					'minv'	=> 2,
					'maxv'	=> 32,
					'must'	=> true
				),
				'day' 	=> array(
					'name'	=> 'Dag',
					'must'	=> true
				),
				'month'	=> array(
					'name'	=> 'Måned',
					'must'	=> true
				),
				'year'	=> array(
					'name'	=> 'År',
					'must'	=> true
				),
				'phonenumber'	=> array(
					'name'	=> 'Telefonnummer',
					'minv'	=> 8,
					'maxv'	=> 12,
					'must'	=> true
				),
				'address'	=> array(
					'name'	=> 'Adresse',
					'must'	=> true
				),
				'ZipPlace'	=> array(
					'name'	=> 'Poststed',
					'must'	=> true
				),
				'ZipCode'	=> array(
					'name'	=> 'Postnummer',
					'minv'	=> 4,
					'maxv'	=> 4,
					'must'	=> true
				),
				'emailaddress'	=> array(
					'name'	=> 'E-post adresse',
					'minv'	=> 6,
					'maxv'	=> 128,
					'prim'	=> 'users',
					'must'	=> true
				),
				'password'	=> array(
					'name'	=> 'Passord',
					'minv'	=> 8,
					'maxv'	=> 32,
					'must'	=> true
				),
				'passwordr'	=> array(
					'name'	=> 'Bekreft passord',
					'same'	=> 'password',
					'must'	=> true
				)

			));

			/*
			-	if the validation process is successfull
			-	then we can continue to addint the information 
			-	to the database. 
			-	But however first we check if the user have agreed to 
			-	Terms Of Service
			*/

			
			if ($validation->success()) {
				if(IO::get('termsofservice') == 'on') {

					/*
					-	Making the passwords and 
					-	encryption required.
					-	-------------------------
					-	Connecting to the database
					-	and getteing ready for insertion.
					*/
						$sls = Encrypt::salt();
						$pass = Encrypt::make(IO::get('password'), $sls);
						$encryption = Encrypt::make(IO::get('password').IO::get('emailaddress'), $sls);
						$user = new User();

					/*
					-	commit - db changes.
					*/
						try {
							$user->create(array(
								'FirstName'		=> strip_tags(strtolower(IO::get('firstname'))),
								'LastName'		=> strip_tags(strtolower(IO::get('lastname'))),
								'BirthDay'		=> IO::get('year') . '-' . IO::get('month') . '-' . IO::get('day'),
								'Address'		=> strip_tags(strtolower(IO::get('address'))),
								'ZipPlace'		=> strip_tags(strtolower(IO::get('zipplace'))),
								'ZipCode'		=> IO::get('zipcode'),
								'EmailAddress'	=> strip_tags(strtolower(IO::get('emailaddress'))),
								'PhoneNumber'	=> IO::get('phonenumber'),
								'SLS'			=> $sls,
								'Password'		=> $pass,
								'OrgNumber'		=> NULL,
								'OrgName'		=> NULL,
								'Active'		=> $encryption,
								'UserGroup'		=> 1
							));

							/*
							-	Send confirmation email to user.
							*/
								sending_activation(IO::get('emailaddress'), $encryption, IO::get('firstname'), IO::get('lastname'));

								$LoggText = date("d-m-Y") . " Bruker: ";
								$LoggText .= IO::get('emailaddress'). " " . IO::get('firstname') . " " . IO::get('lastname');
								$LoggText .= " har registrert seg. ";

								$handler = new Put();
								$handler->create('changelogs', array(
									"LoggText" => $LoggText
								));

							/*
							-	Redirect the user.
							*/
								Session::flash('signup', 'Du er nesten ferdig, sjekk din e-post for å aktivere din konto. ('. IO::get('emailaddress') .')');
								Redirect::to('/signup/kunde/success/');

						} catch (Exception $e) {
							$errors[] = $e->getMessage();
						}

				} else {
					$errors[] = "Vennligst godta <strong>Vilkår</strong> og <strong>Personverns lover</strong> for å fortsette.";
				}
			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}

    	if(Token::check(IO::get('reg_operator_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'firstname' => array(
					'name'	=> 'Fornavn',	// if any errors display this name.
					'minv'	=> 2,			// the field required at least 2 characters.
					'maxv'	=> 32,			// the field can not hold more than 32 characters.
					'must'	=> true 		// the field can not be empty or blank.
				),
				'lastname'	=> array(
					'name'	=> 'Etternavn',
					'minv'	=> 2,
					'maxv'	=> 32,
					'must'	=> true
				),
				'day' 	=> array(
					'name'	=> 'Dag',
					'must'	=> true
				),
				'month'	=> array(
					'name'	=> 'Måned',
					'must'	=> true
				),
				'year'	=> array(
					'name'	=> 'År',
					'must'	=> true
				),
				'phonenumber'	=> array(
					'name'	=> 'Telefonnummer',
					'minv'	=> 8,
					'maxv'	=> 12,
					'must'	=> true
				),
				'address'	=> array(
					'name'	=> 'Adresse',
					'must'	=> true
				),
				'ZipPlace'	=> array(
					'name'	=> 'Poststed',
					'must'	=> true
				),
				'ZipCode'	=> array(
					'name'	=> 'Postnummer',
					'minv'	=> 4,
					'maxv'	=> 4,
					'must'	=> true
				),
				'orgnum'	=> array(
					'name'	=> 'Organisasjonsnummer',
					'minv'	=> 9,
					'maxv'	=> 9,
					'must'	=> true
				),
				'emailaddress'	=> array(
					'name'	=> 'E-post adresse',
					'minv'	=> 6,
					'maxv'	=> 128,
					'must'	=> true
				),
				'password'	=> array(
					'name'	=> 'Passord',
					'minv'	=> 8,
					'maxv'	=> 32,
					'must'	=> true
				),
				'passwordr'	=> array(
					'name'	=> 'Bekreft passord',
					'same'	=> 'password',
					'must'	=> true
				)

			));

			/*
			-	if the validation process is successfull
			-	then we can continue to addint the information 
			-	to the database. 
			-	But however first we check if the user have agreed to 
			-	Terms Of Service
			*/

			
			if ($validation->success()) {
				if(IO::get('termsofservice') == 'on') {

					/*
					-	Checking foretakregistret for legitimate
					-	orgnumbers.
					*/	

					if (brreg("http://data.brreg.no/enhetsregisteret/enhet/". IO::get('orgnum') .".json")) {

							$org = file_get_contents("http://data.brreg.no/enhetsregisteret/enhet/". IO::get('orgnum') .".json");
							$org = json_decode($org);

						/*
						-	Making the passwords and 
						-	encryption required.
						-	-------------------------
						-	Connecting to the database
						-	and getteing ready for insertion.
						*/
							$sls = Encrypt::salt();
							$pass = Encrypt::make(IO::get('password'), $sls);
							$encryption = Encrypt::make(IO::get('password').IO::get('emailaddress'), $sls);
							$user = new User();
							$datahandler = new Put();

						/*
						-	commit - db changes.
						*/
							try {
								$user->create(array(
									'FirstName'		=> strip_tags(strtolower(IO::get('firstname'))),
									'LastName'		=> strip_tags(strtolower(IO::get('lastname'))),
									'BirthDay'		=> IO::get('year') . '-' . IO::get('month') . '-' . IO::get('day'),
									'Address'		=> strip_tags(strtolower(IO::get('address'))),
									'ZipPlace'		=> strip_tags(strtolower(IO::get('zipplace'))),
									'ZipCode'		=> IO::get('zipcode'),
									'EmailAddress'	=> strip_tags(strtolower(IO::get('emailaddress'))),
									'PhoneNumber'	=> IO::get('phonenumber'),
									'SLS'			=> $sls,
									'Password'		=> $pass,
									'OrgNumber'		=> IO::get('orgnum'),
									'OrgName'		=> $org->navn,
									'Active'		=> $encryption,
									'UserGroup'		=> 2
								));

								$datahandler->create('skills', array(
									'UserID' => $user->lastid()
								));

								/*
								-	Send confirmation email to user.
								*/
									sending_activation_operator(IO::get('emailaddress'), $encryption, IO::get('firstname'), IO::get('lastname'), $org->navn);

								/*
								-	Redirect the user.
								*/
									Session::flash('signup', 'Du er nesten ferdig, sjekk din e-post for å aktivere din konto. ('. IO::get('emailaddress') .')');
									Redirect::to('/signup/operator/success/');

							} catch (Exception $e) {
								$errors[] = $e->getMessage();
							}

						} else {
							$errors[] = "Organisasjonsnummer eksisterer ikke i brønnøysundregisteret.";
						}
						
				} else {
					$errors[] = "Vennligst godta <strong>Vilkår</strong> og <strong>Personverns lover</strong> for å fortsette.";
				}
			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}
    }