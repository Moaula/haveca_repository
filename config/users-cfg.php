<?php

//	Include files:
	include_once('appfiles/core/init.php');

//	Site title:
	$site_title = "Bruker list";

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if(!$_init_uzer->isOnline()) { Redirect::to("/"); }
    if(!$_init_uzer->hasPerm('admin')) { Redirect::to("/"); }

/*
-	User update;
*/

	if(IO::exists('get')) {
		if(!empty(IO::get('action')) && IO::get('action') == "open" && is_numeric(IO::get('id'))) {
			$user = new User();
			$user->update(array(
				"Active" => 1
			), IO::get('id'));


			$LoggText = date("d-m-Y") . " Bruker: ";
			$LoggText .= ucfirst($_init_uzer_data->FirstName) . " " . ucfirst($_init_uzer_data->LastName);
			$LoggText .= " Aktiverte konto til bruker id: ". IO::get('id');

			$handler = new Put();
			$handler->create('changelogs', array(
				"LoggText" => $LoggText
			));

			Session::flash('users', 'Brukeren er åpnet.');
			Redirect::to('/users');
		}

		if(!empty(IO::get('action')) && IO::get('action') == "ban" && is_numeric(IO::get('id'))) {
			$user = new User();
			$user->update(array(
				"Active" => "Banned"
			), IO::get('id'));

			$LoggText = date("d-m-Y") . " Bruker: ";
			$LoggText .= ucfirst($_init_uzer_data->FirstName) . " " . ucfirst($_init_uzer_data->LastName);
			$LoggText .= " Stengte kontoen til bruker id: ". IO::get('id');

			$handler = new Put();
			$handler->create('changelogs', array(
				"LoggText" => $LoggText
			));
			
			Session::flash('users', 'Brukeren er blokkert.');
			Redirect::to('/users');
		}
	}


/*
-	Get all users from db
*/
	$users = new Get();
	$users->get_data('users', 'UserID');
	$users = $users->results();