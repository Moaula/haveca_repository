<?php

//	Include files:
	include_once('appfiles/core/init.php');

//	Site title:
	$site_title = "E-post verifisering";

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if($_init_uzer->isOnline()) { Redirect::to("/"); }

/*
-	Activating users:
*/

	if(IO::exists('get')) {

		/*
		-	activating user type: KUNDE
		*/
			if(!empty(IO::get('action')) && IO::get('action') == "kunde") {
				if(!empty(IO::get('ue')) && !empty(IO::get('uc'))) {
					$user_emailaddress 		= strip_tags(IO::get('ue'));
					$user_encryptioncode 	= strip_tags(IO::get('uc'));

					try {
						$user = new User();
						$user->activate($user_emailaddress, $user_encryptioncode);

						$LoggText = date("d-m-Y") . " En bruker aktiverte kontoen: ";
						// $LoggText .= ucfirst($_init_uzer_data->FirstName) . " " . ucfirst($_init_uzer_data->LastName);
						// $LoggText .= " Stengte kontoen til bruker id: ". IO::get('id');

						$handler = new Put();
						$handler->create('changelogs', array(
							"LoggText" => $LoggText
						));

						Session::flash('login', "Du kan nå logge på. Din konto er aktivert.");
						Redirect::to("/login");
					} catch (Exception $e) {
						$errors[] = $e->getMessage();
					}
				}
			}

		/*
		-	Activating Operators.
		*/
			if(!empty(IO::get('action')) && IO::get('action') == "operator") {
				if(!empty(IO::get('ue')) && !empty(IO::get('uc'))) {
					if(!empty(IO::get('confirm')) && IO::get('confirm') == "confirmed") {
						$user_emailaddress 		= strip_tags(IO::get('ue'));
						$user_encryptioncode 	= strip_tags(IO::get('uc'));

						try {
							$user = new User();
							$user->activate($user_emailaddress, $user_encryptioncode);

							$LoggText = date("d-m-Y") . " En opratør har aktivert kontoen sin: ";
							// $LoggText .= ucfirst($_init_uzer_data->FirstName) . " " . ucfirst($_init_uzer_data->LastName);
							// $LoggText .= " Stengte kontoen til bruker id: ". IO::get('id');

							$handler = new Put();
							$handler->create('changelogs', array(
								"LoggText" => $LoggText
							));

							Session::flash('login', "Du kan nå logge på. Din konto er aktivert.");
							Redirect::to("/login");

						} catch (Exception $e) {
							$errors[] = $e->getMessage();
						}
					}
				}
			}

	}