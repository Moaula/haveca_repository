<?php

//	Include files:
	include_once('appfiles/core/init.php');

//	Site title:
	$site_title = "Visning";

/*
-	Get data to show the operator selected
-	check what form a user needs to show dat as.
*/

	if(IO::exists('get')) {
		if(IO::get('action') == "vis") {


			if(is_numeric(IO::get('id'))) {
				$handler = new Get();
				$handler->get_data_by_id("annonse", "AnnonseID", IO::get("id"));
				$datahandler = $handler->result();

				if(empty($datahandler)) { Redirect::to('/'); }

				$datahandler_images = $handler->get_data_by_id("images", "AnnonseID", IO::get("id"));
				$datahandler_images = $handler->results();

				$views = $datahandler->Views + 1;

				$handler = new Put();
				$handler->update("annonse", array("Views" => $views), IO::get('id'), "AnnonseID");
			}
		}
	}
	