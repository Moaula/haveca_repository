<?php include_once('config/contact-cfg.php'); include_once('includes/files/toparea.php');  ?>
	
	<!-- ===================================
	#	contactschema site.
	=====================================-->

	<section class="contactschema">
		<div class="medium-form form-center">
			<div class="page-header">
				<h1><i class="fa fa-envelope-o"></i> Kontaktskjema</h1>
			</div>

			<?php if(!IO::exists('get') && empty(IO::get('action'))) { ?>
				<div class="form-information">
					<p>
						<strong>Felt markert med <i class="fa fa-star"></i> kreves. </strong>
						<hr />
					</p>
				</div>

				<!-- Error outputting -->
				<?php 
					if(!empty($errors)) { 
						echo writeerrors($errors, '', 'Feil.'); 
					} 
				?>

				<div class="contact-form-container_e">
					<form accept="" method="post">
						
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>Fulltnavn: <i class="fa fa-star"></i></label>
									<input type="text" name="fullname" placeholder="Skriv inn ditt fulltnavn" value="<?php echo IO::get('fullname'); ?>" class="form-control" />
								</div>	
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label>E-post adresse: <i class="fa fa-star"></i></label>
									<input type="email" name="emailaddress" placeholder="Skriv inn din e-post adresse" value="<?php echo IO::get('emailaddress'); ?>" class="form-control" />
								</div>	
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label>Emne: <i class="fa fa-star"></i> </label>
									<input type="text" name="subject" placeholder="Skriv kort hva det gjelder" value="<?php echo IO::get('subject'); ?>" class="form-control" />
								</div>	
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label>Melding: <i class="fa fa-star"></i> </label>
									<textarea name="message" placeholder="Skriv utfyllende hva du lurer på" class="form-control" rows="6" ><?php echo IO::get('message'); ?></textarea>
								</div>	
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<div class="g-recaptcha" data-sitekey="6LeLZwcUAAAAABL_r-avtZupqWh743Ni1XBgUt0M"></div>
								</div>	
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="hidden" name="message_token" value="<?php echo Token::create(); ?>" />
									<button class="btn btn-success btn-lg btn-block"><i class="fa fa-send"></i> &nbsp; Send melding </button>
								</div>	
							</div>
						</div>
					</form>
				</div>
			<?php } ?>
			<?php if(IO::exists('get') && IO::get('action') == "success") { ?>
		
				<div class="message-response-icon">
					<i class="fa fa-check-circle-o green"></i>
				</div>
				<div class="message-response">
					<div class="main-word">Takk</div>
					<div class="second-word">Vi svarer deg innen kort tid.</div>

					<a href="/" class="btn btn-success"><i class="fa fa-home"></i> Til hjemmesiden</a>
				</div>				

			<?php } ?>

		</div>
	</section>



<?php include_once('includes/files/botarea.php'); ?>