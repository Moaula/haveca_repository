-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 12. Mai, 2016 23:10 p.m.
-- Server-versjon: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dronevisning`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `annonse`
--

CREATE TABLE IF NOT EXISTS `annonse` (
  `AnnonseID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `Title` varchar(128) NOT NULL,
  `Address` varchar(128) NOT NULL,
  `Description` text NOT NULL,
  `BuildingType` varchar(32) DEFAULT NULL,
  `Ownership` varchar(32) DEFAULT NULL,
  `Video` text NOT NULL,
  `GoogleMaps` text NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `Price` int(11) DEFAULT NULL,
  `TotalPrice` int(11) DEFAULT NULL,
  `BuildYear` int(11) DEFAULT NULL,
  `AppraisedValue` int(11) DEFAULT NULL,
  `CommonDept` int(11) DEFAULT NULL,
  `CommonWealth` int(11) DEFAULT NULL,
  `SharedCosts` int(11) DEFAULT NULL,
  `LoanValue` int(11) DEFAULT NULL,
  `Area` int(11) DEFAULT NULL,
  `AreaOfGross` int(11) DEFAULT NULL,
  `AreaOfUse` int(11) DEFAULT NULL,
  `AreaPrimary` int(11) DEFAULT NULL,
  `Plot` int(11) DEFAULT NULL,
  `Roms` int(11) DEFAULT NULL,
  `BedRoms` int(11) DEFAULT NULL,
  `BathRoms` int(11) DEFAULT NULL,
  PRIMARY KEY (`AnnonseID`),
  KEY `users_annonse_fk` (`UserID`),
  KEY `customer_annonse_fk` (`CustomerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `conversations`
--

CREATE TABLE IF NOT EXISTS `conversations` (
  `ConversationID` int(11) NOT NULL AUTO_INCREMENT,
  `Tema` varchar(32) NOT NULL,
  `Sender` int(11) NOT NULL,
  `Reciver` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  PRIMARY KEY (`ConversationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `conversations_message`
--

CREATE TABLE IF NOT EXISTS `conversations_message` (
  `MessageID` int(11) NOT NULL AUTO_INCREMENT,
  `ConversationID` int(11) NOT NULL,
  `Sender` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `Content` text NOT NULL,
  PRIMARY KEY (`MessageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `conversations_users`
--

CREATE TABLE IF NOT EXISTS `conversations_users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ConversationID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Deleted` int(1) NOT NULL,
  `URead` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `facilities`
--

CREATE TABLE IF NOT EXISTS `facilities` (
  `FID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT ' ',
  PRIMARY KEY (`FID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `facilities_annonse`
--

CREATE TABLE IF NOT EXISTS `facilities_annonse` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AnnonseID` int(11) NOT NULL,
  `FID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `AnnonseID` (`AnnonseID`),
  KEY `FID` (`FID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `ImageID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `AnnonseID` int(11) NOT NULL,
  `Path` varchar(255) NOT NULL,
  `Prim` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ImageID`),
  KEY `UserID` (`UserID`),
  KEY `AnnonseID` (`AnnonseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `sessionscookies`
--

CREATE TABLE IF NOT EXISTS `sessionscookies` (
  `SessionID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `SessionName` varchar(64) NOT NULL,
  PRIMARY KEY (`SessionID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `site`
--

CREATE TABLE IF NOT EXISTS `site` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Location` varchar(128) NOT NULL,
  `Content` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dataark for tabell `site`
--

INSERT INTO `site` (`ID`, `Location`, `Content`) VALUES
(6, 'FRONTPAGE_BADGE_1_ICON', '<i class="fa fa-money"></i>'),
(7, 'FRONTPAGE_BADGE_2_ICON', '<i class="fa fa-bank"></i>'),
(8, 'FRONTPAGE_BADGE_3_ICON', '<i class="fa fa-book"></i>'),
(9, 'FRONTPAGE_BADGE_1_TXT', 'Selg dine tjenester fra drone til private og næringsdrivende som etterspør din kompetanse'),
(10, 'FRONTPAGE_BADGE_2_TXT', 'Gratis innmelding og markedsføring. Du beholder 80% av hvert salg du gjennomfører. Ingen skjulte avgifter.'),
(11, 'FRONTPAGE_BADGE_3_TXT', 'Velg hvilke tjenester du tilbyr fra drone. Bekreft at du kjenner og følger gjeldende regelverk.');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `SkillID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `videos` int(1) NOT NULL DEFAULT '0',
  `images` int(1) NOT NULL DEFAULT '0',
  `raw_videos` int(1) NOT NULL DEFAULT '0',
  `raw_images` int(1) NOT NULL DEFAULT '0',
  `edited_videos` int(1) NOT NULL DEFAULT '0',
  `edited_images` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SkillID`),
  UNIQUE KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `usergroups`
--

CREATE TABLE IF NOT EXISTS `usergroups` (
  `UserGroupID` int(11) NOT NULL AUTO_INCREMENT,
  `Permissions` varchar(64) NOT NULL,
  `Name` varchar(32) NOT NULL,
  `UserDesc` varchar(32) NOT NULL,
  PRIMARY KEY (`UserGroupID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dataark for tabell `usergroups`
--

INSERT INTO `usergroups` (`UserGroupID`, `Permissions`, `Name`, `UserDesc`) VALUES
(1, '{"operator":0, "moderator":0, "admin":0, "sysadmin":0}', 'U_STD', 'KUNDE'),
(2, '{"operator":1, "moderator":0, "admin":0, "sysadmin":0}', 'U_OPERATOR', 'OPERATØR'),
(3, '{"operator":1, "moderator":1, "admin":0, "sysadmin":0}', 'U_MOD', 'MODERATOR'),
(4, '{"operator":1, "moderator":1, "admin":1, "sysadmin":0}', 'U_ADMIN', 'ADMINISTRATOR'),
(6, '{"operator":1, "moderator":1, "admin":1, "sysadmin":1}', 'U_SYSADMIN', 'SYSTEM ADMINISTRATOR');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(32) NOT NULL,
  `LastName` varchar(32) NOT NULL,
  `BirthDay` date NOT NULL,
  `Address` varchar(128) NOT NULL,
  `ZipPlace` varchar(64) NOT NULL,
  `ZipCode` int(11) NOT NULL,
  `EmailAddress` varchar(128) NOT NULL,
  `UserBio` text NOT NULL,
  `Picture` varchar(64) NOT NULL,
  `PhoneNumber` varchar(12) NOT NULL,
  `SLS` varchar(32) NOT NULL,
  `Password` varchar(64) NOT NULL,
  `OrgNumber` int(11) DEFAULT NULL,
  `OrgName` varchar(128) DEFAULT NULL,
  `Active` varchar(255) NOT NULL DEFAULT '0',
  `UserGroup` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `EmailAddress` (`EmailAddress`),
  KEY `users_ibfk_1` (`UserGroup`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `annonse`
--
ALTER TABLE `annonse`
  ADD CONSTRAINT `customer_annonse_fk` FOREIGN KEY (`CustomerID`) REFERENCES `users` (`UserID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `users_annonse_fk` FOREIGN KEY (`UserID`) REFERENCES `users` (`UserID`) ON UPDATE CASCADE;

--
-- Begrensninger for tabell `facilities_annonse`
--
ALTER TABLE `facilities_annonse`
  ADD CONSTRAINT `facilities_annonse_ibfk_1` FOREIGN KEY (`AnnonseID`) REFERENCES `annonse` (`AnnonseID`),
  ADD CONSTRAINT `facilities_annonse_ibfk_2` FOREIGN KEY (`FID`) REFERENCES `facilities` (`FID`);

--
-- Begrensninger for tabell `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`UserID`),
  ADD CONSTRAINT `images_ibfk_2` FOREIGN KEY (`AnnonseID`) REFERENCES `annonse` (`AnnonseID`);

--
-- Begrensninger for tabell `sessionscookies`
--
ALTER TABLE `sessionscookies`
  ADD CONSTRAINT `sessionscookies_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`UserID`);

--
-- Begrensninger for tabell `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`UserGroup`) REFERENCES `usergroups` (`UserGroupID`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
