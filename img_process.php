<?php

//	Include files:
	include_once('appfiles/core/init.php');

	if(IO::exists()) {
		if(is_numeric(IO::get('id')) && !empty(IO::get('img')) && !empty(IO::get('thumb'))) {
			$del = new Del();
			$del->delete("images", IO::get("id"), "ImageID");
			$dataf = "res/uploads/visning/";
			unlink($dataf.IO::get('img'));
			unlink($dataf.IO::get('thumb'));
			echo "done";

			$LoggText = date("d-m-Y") . " Et bilde er slettet: ";
			// $LoggText .= ucfirst($_init_uzer_data->FirstName) . " " . ucfirst($_init_uzer_data->LastName);
			// $LoggText .= " Stengte kontoen til bruker id: ". IO::get('id');

			$handler = new Put();
			$handler->create('changelogs', array(
				"LoggText" => $LoggText
			));

		}
	}