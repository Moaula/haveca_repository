<footer>
<div class="container">
	<div class="col-md-3 col-sm-6">
		<div class="footer-item">
			<div class="footer-item-title">
				<i class="fa fa-building-o"></i> &nbsp;  Firma
			</div>
			<ul>
				<li> 
					Kopirett &copy; 2015-<?php echo date('Y'); ?>	 Dronevisning  Innholdet er beskyttet etter åndsverksloven. 
					<br /><br />
					Org.nr: 916337973 MVA
				</li>
			</ul>
		</div>
	</div>

	<div class="col-md-3 col-sm-6"> 
		<div class="footer-item">
			<div class="footer-item-title">
				<i class="fa fa-envelope-o"></i> &nbsp; Kontakt
			</div>
			<ul>
				<li>
					Du kan kontakte oss på hei@dronevisning.no eller 
					via &nbsp; <br> <a href="/contact" title="Kontakt oss via kontaktskjema"><i class="fa fa-external-link"></i> Kontaktskjema</a>
				</li>
			</ul>
		</div>
	</div>

	<!-- div class="col-md-3 col-sm-6">
		<div class="footer-item">
			<div class="footer-item-title">
				<i class="fa fa-question-circle"></i> &nbsp; Hjelp
			</div>
			<ul>
				<li><a href="#" title="Les igjennom våre vilkår og personvernsregler" target="_blank"><i class="fa fa-external-link"></i> &nbsp; Personvern &amp; Vilkår </a></li>
				<li><a href="#" title="Les om informasjonskapsler" target="_blank"><i class="fa fa-external-link"></i> &nbsp; Informasjonskapsler </a></li>
			</ul>
		</div>
	</div -->

	<div class="col-md-3 col-sm-6">
		<div class="footer-item">
			<div class="footer-item-title">
				<i class="fa fa-share-alt"></i> &nbsp; Media
			</div>
			<ul>
				<li>
					<a class="share-suqare" href="https://www.facebook.com/dronevisning/" title="Besøk oss på Facebook.com" target="_blank"><i class="fa fa-facebook-square"></i></a>
					<a class="share-suqare" href="https://twitter.com/dronevisning" title="Besøk oss på Twitter.com" target="_blank"><i class="fa fa-twitter-square"></i></a>
					<a class="share-suqare" href="https://www.youtube.com/channel/UCcLIqYSBMPk8t6-pFPLRSAA" title="Besøk oss på Youtube.com" target="_blank"><i class="fa fa-youtube-square"></i></a>
				</li>
			</ul>
		</div>
	</div>
</div>
</footer>
<div class="footer-footer">
	Nettsiden er levert av <a href="http://Htmlnorge.no">Htmlnorge</a>
</div>

</body>
</html>