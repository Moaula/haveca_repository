<!DOCTYPE html>
<html lang="nb"><head>
      <!-- Base -->
      <base href="http://<?php echo $httpHost; ?>/">

      <!-- Website title -->
      <title> Dronevisning - <?php echo $site_title; ?> </title>

      <!-- Favorite Icon -->
      <link rel="icon" type="image/x-icon" href="res/images/dv.png">

      <!-- Meta -->
      <meta charset="utf-8">
      <meta name="description" content="Profesjonelle foto-og filmpresentasjoner fra drone">
      <meta name="keywords" content="profesjonell, filming, eiendom, video, visning, drone, foto, bolig, salg, boligsalg, dronevisning, visning, dronevisning.no, boligvisning">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <?php if($CDoc == "visning") { ?>
            <!-- Sharing Meta -->
            <meta property="og:title" content="Dronevisning.no: <?php echo $datahandler->Title; ?>">
            <meta property="og:site_name" content="Dronevisning">
            <meta property="og:url" content="http://dronevisning.no/kampnaje/vis/<?php echo $datahandler->AnnonseID; ?>/">
            <meta property="og:description" content="<?php echo substr($datahandler->Description, 0, 100); ?>.">
            <meta property="og:type" content="website">
            <?php 
                foreach ($datahandler_images as $img) { 
                    if ($img->Prim == 1) {
                        echo '<meta property="og:image" content="http://dronevisning.no/res/uploads/visning/'. $img->Path .'">';
                    } 
                }
            ?>
            
      <?php } else { ?>
            <!-- Sharing Meta -->
            <meta property="og:title" content="Dronevisning.no - Profesjonelle foto-og filmpresentasjoner fra drone" />
            <meta property="og:site_name" content="Dronevisning" />
            <meta property="og:url" content="http://dronevisning.no/" />
            <meta property="og:description" content="Profesjonelle foto-og filmpresentasjoner fra drone. Et unikt valg." />
            <meta property="og:type" content="website" />
            <meta property="og:image" content="http://dronevisning.no/res/presentation.jpg" />
      <?php } ?>

      <!-- css -->
      <link rel="stylesheet" type="text/css" href="appfiles/css/plugins/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="appfiles/css/plugins/font-awesome.css">
      <link rel="stylesheet" type="text/css" href="appfiles/css/plugins/lightbox.css">
      <link rel="stylesheet" type="text/css" href="appfiles/css/main.css">

      <!-- Script -->
      <script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
      <script type="text/javascript" src="appfiles/js/plugins/bootstrap.min.js"></script>
      <script type="text/javascript" src="appfiles/js/plugins/lb.js"></script>
      <script type="text/javascript" src="appfiles/js/functions.js"></script>
      <script type="text/javascript" src="appfiles/js/main.js"></script>
      <script src='https://www.google.com/recaptcha/api.js'></script>
</head><body>