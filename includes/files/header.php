<nav class="navbar navbar-default  navbar-fixed-top">
    <div class="container-fluid">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Meny</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img alt="Brand" src="res/images/dv.png" class="brand-logo">
                <div class="brand-logo-text">Dronevisning</div>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">

                <!-- Main Links -->
                

                <li>
                    <a href="/" <?php if($CDoc == "index") { echo 'class="red-circle"'; } ?>>
                        <i class="fa fa-home"></i> &nbsp; Hjem
                    </a>
                </li>

                <li>
                    <a href="/visninger" <?php if($CDoc == "visninger") { echo 'class="red-circle"'; } ?>> 
                        &nbsp; <i class="fa fa-television"></i> Visning &nbsp;
                    </a>
                </li>

                <li>
                    <a href="/contact" <?php if($CDoc == "contact") { echo 'class="red-circle"'; } ?>>
                        <i class="fa fa-envelope-o"></i> &nbsp; Kontakt
                    </a>
                </li>

                <!-- ======================================
                    Filtering what to display based on
                    if the user is logged inn or not. 
                ======================================= -->
                

                <?php if(0) { ?>

                    <!-- ===============================
                        When user is not logged inn 
                        show the options of register
                        or login
                    =================================-->
                    

                    <li>
                        <a href="/registration-options" <?php if($CDoc == "registration-options") { echo 'class="red-circle"'; } ?>>
                            <i class="fa fa-user-plus"></i> &nbsp; Registrer
                        </a>
                    </li>
                    
                    <li>
                        <a href="/login" <?php if($CDoc == "login") { echo 'class="red-circle"'; } ?>>
                            <i class="fa fa-sign-in"></i> &nbsp; Logg på
                        </a>
                    </li>
                
                <?php } else if($_init_uzer->isOnline()) { ?>

                    <!-- ===============================
                        When user is logged inn 
                        show the the user panel for
                        the users.
                    =================================-->

                   
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                            <i class="fa fa-user"></i> &nbsp; 
                            Velkommen, <?php echo ucfirst($_init_uzer_data->FirstName), ' ', ucfirst($_init_uzer_data->LastName); ?> 
                            <span class="caret"></span>
                        </a>
                        
                        <ul class="dropdown-menu">

                            <!-- Menus that is displayed for all users -->
                            
                            <li><a href="/profile"><i class="fa fa-user"></i> &nbsp; Min profil</a></li>
                            <li><a href="/changepw"><i class="fa fa-pencil"></i> &nbsp; Endre passord</a></li>


                            <?php if(!$_init_uzer->hasPerm('operator')) { ?>
                                
                                <!-- Menus that is displaued only for NON-operators -->
                                <li><a href="/billing"><i class="fa fa-dollar"></i> &nbsp; Mine faktura</a></li>
                                <li role="separator" class="divider"></li>


                            <?php } else { ?>

                                <!-- Menus that is displaud to operators-ONLY -->
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="/operators/show/<?php echo $_init_uzer_data->UserID; ?>">
                                        <i class="fa fa-file-text"></i> &nbsp; Min side &nbsp; (<i class="fa fa-globe"></i>) 
                                    </a>
                                </li>

                                <li role="separator" class="divider"></li>
                                <li><a href="/billing"><i class="fa fa-file"></i> &nbsp; Mine faktura</a></li>
                                <li><a href="/billing/operator/"><i class="fa fa-file"></i> &nbsp; Fakturering</a></li>
                                <li>
                                    <a href="/newcontract">
                                        <i class="fa fa-plus"></i> &nbsp; Opprett avtale 
                                    </a>
                                </li>

                                <li role="separator" class="divider"></li>

                                <li><a href="/annonse"><i class="fa fa-tv"></i> &nbsp; Opprett visning </a></li>
                                <li><a href="/list/visning/"><i class="fa fa-list"></i> &nbsp; Mine Visninger &nbsp; </a></li>
                            
                            <?php } ?>
                            <?php if($_init_uzer->hasPerm('admin')) { ?>
                                <li role="separator" class="divider"></li>
                                
                                <li><a href="/logs"><i class="fa fa-file"></i> &nbsp; Endrings logg</a></li>
                                <li><a href="/users"><i class="fa fa-users"></i> &nbsp; Administrere brukere</a></li>
                            <?php } ?>

                            <!-- bottom menus options - for all users. -->
                            <li role="separator" class="divider"></li>    

                            <li><a href="/logout"><i class="fa fa-power-off"></i> &nbsp; Logg ut</a></li>
                        </ul>
                    </li>

                <?php } ?>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>  