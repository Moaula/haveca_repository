<?php 
	include_once('config/index-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	<section class="intro-video">
		<div class="intro-video-holder">
			<video class="intro-video-view" preload autoplay="autoplay" loop="loop" muted>
				<source src="res/videos/intro/intro.mp4" type="video/mp4" />
				<source src="res/videos/intro/intro.ogg" type="video/ogg" />
				<source src="res/videos/intro/intro.webm" type="video/webm" />
			</video>
		</div>

		<div class="intro-overlay">
			<div class="intro-main-word">Skreddersydde</div>
			<div class="intro-secondary-word">Dronetjenester</div>
			<a href="/visninger" class="btn btn-success call-to-action"><i class="fa fa-camera"></i> Presentasjoner</a>
		</div>


	</section>

	<section>
		<!-- Flashing sessions with infromation -->
		<?php 
			if(Session::exists('index')) {
				if(IO::exists('get')) { $alert = IO::get('alert'); } else { $alert = 'success'; }
				echo writealerts(Session::flash('index'), $alert); 
			} 
		?>
	</section>

	<?php if(0): ?>
		<section class="search-bar">
			<div class="container">
				<div class="medium-form form-center">
					<form action="search" method="get">
						<div class="col-md-10 col-sm-10 col-xs-10 nopadding">
							<input type="text" name="qry" placeholder="SØK MED POSTNR, STED, NAVN ELLER INNHOLD I VISNINGER" class="form-control search-bar-bar">
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2 nopadding">
							<button class="btn btn-success btn-lg btn-block"><i class="fa fa-search"></i> &nbsp; Søk</button>
						</div>
					</form>
				</div>
			</div>
		</section>
	<?php endif ?>

	<section class="intro-steps">
		<div class="container">
			<div class="row">	
				<div class="col-md-4 col-sm-6">
					<div class="intro-step">
						<div class="intro-step-icon"><i class="fa fa-send-o"></i> </div>
						<p>
							Jeg tilbyr skreddersydde dronevisninger som blir lagt merke til.
						</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="intro-step">
						<div class="intro-step-icon"><i class="fa fa-cog"></i> </div>
						<p>
							Dronevisning drives av Kristian Solberg, 2849 Kapp. 
							Se <a href="/visninger">presentasjoner</a>. 
						</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="intro-step">
						<div class="intro-step-icon"><i class="fa fa-envelope-o"></i> </div>
						<p>
							Spørsmål ? <br> Kontakt meg på <a href="mailto:hei@dronevisning.no">hei@dronevisning.no</a> eller via <a href="/contact">kontaktskjema</a>.
						</p>
					</div>
				</div>
			</div>
		</div>	
	</section>

	<?php if(!empty(1)) { ?>
		<section class="dark">
			<div class="container">
				<div class="page-header">
					<h1>Visninger</h1>
				</div>
				
				<div class="row">
					<?php foreach ($visninger as $visning) { ?>
						<?php  
							$images = new Get();
							$images->get_data_by_id("images", "AnnonseID", $visning->AnnonseID);
							$images = $images->results();
						?>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="visning-block" title="<?php echo $visning->Title; ?>">
								<a href="/kampanje/vis/<?php echo $visning->AnnonseID; ?>/" class="block-link">
									<div class="visning-bilde">
										<?php foreach ($images as $img) {  ?>
											<?php if($img->Prim == 1) { ?>
												<img src="res/uploads/visning/t_<?php echo $img->Path; ?>" title="<?php echo $visning->Title; ?>" alt="<?php echo $visning->Title; ?>">
											<?php } ?>
										<?php } ?>
										<div class="play-button"><i class="fa fa-play"></i> </div>
									</div>
									<div class="visning-title">
										<h3><?php echo $visning->Title; ?></h3>
										<p><?php echo nl2br(bbcode(substr($visning->Description, 0, 140))); ?>...</p>
									</div>
								</a>
								<div class="visning-controls">
									<a href="/kampanje/vis/<?php echo $visning->AnnonseID; ?>/" class="btn btn-success btn-block"><i class="fa fa-eye"></i> &nbsp; Detaljer </a>
								</div>
							</div>
						</div>
					<?php } ?>

				</div>

			</div>
		</section>
	<?php } ?>


		<section class="light">
			<div class="container">
				<div class="page-header">
					<h1><i class="fa fa-envelope-o"></i> Kontakt</h1>
				</div>
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<dl class="acordion-list">
							<dt>Hvilke oppdrag tar dronevisning?</dt>
							<dd><p>
									Mye kan presenteres på en mer spennende og interessant måte med film fra luften.
									<br /><br />
									Vi presenterer næringseiendom, tomter, campingområder, turistmål, boliger m.m. 

								</p>
							</dd>
							<dt>Hva koster en ferdig filmpresentasjon?</dt>
							<dd><p>
									Dronevisning inkluderer logo, tekst, musikk og eventuelt annet materiale i presentasjonen i 
									samarbeid med den enkelte kunde. <br /><br /> 

									Produktet ferdigstilles når kunden er fornøyd med resultatet og 
									publiseres på nett og i sosiale medier av Dronevisning. <br /><br />

									En ferdig redigert filmpresentasjon på 45 
									sekunder har en pris på 8500.- pluss moms.  <br /><br />

									Presentasjoner kan være kortere eller lengre, alt etter 
									hva kunden ønsker. 
								</p>
							</dd>
							<dt>Hvordan bestiller jeg?</dt>
							<dd><p>
								Send meg en mail så kontakter jeg deg. <br /><br />
								Ønsker du heller å bestille på telefon, vennligst oppgi telefonnummeret ditt ved henvendelse
							</p></dd>
							<dt>Hvordan betaler jeg?</dt>
							<dd><p>
								Du mottar faktura fra vår samarbeidspartner sendregning.no
							</p></dd>
							<dt>Har dronevisning tillatelse til å filme fra drone?</dt>
							<dd><p>
								Ja, dronevisning har lisens fra Luftfartstilsynet  
								til å foreta filming og fotografering fra luftfartøy over norsk jord.
							</p></dd>
							<dt>Hvor kan dere ta oppdrag?</dt>
							<dd><p>
								Vi dekker primært Østre og Vestre Toten kommune, Gjøvik, Hamar og Lillehammer- området. <br /><br />
								Bor du et annet sted? Ta kontakt med oss.
							</p></dd>
							<dt>Hvorfor skal jeg velge dronevisning?</dt>
							<dd><p>
								En dronevisning gir deg og dine kunder en visuell og annerledes totalopplevelse av det du ønsker å presentere. <br /><br />
								Økt oppmerksomhet kan gi deg den effekten du ønsker, enten det er riktig pris for et objekt, 
								bedre reklame eller flere kunder. <br /><br /> Ditt objekt får egen hjemmeside på dronevisning, samt 
								publisering på Facebook og Youtube.
							</p></dd>
							<dt>Send oss en mail.</dt>
							<dd><p>
								Du treffer oss på hei@dronevisning.no
							</p></dd>
						</dl>
					</div>
					
					<div class="col-md-8 col-sm-8 col-xs-12">

						<div class="contact-form-container">
							<form action="contact" method="post">
								
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<label>Fullt navn: <i class="fa fa-star"></i></label>
											<input type="text" name="fullname" placeholder="Skriv inn ditt fulltnavn" value="<?php echo IO::get('fullname'); ?>" class="form-control" />
										</div>	
									</div>

									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<label>E-postadresse: <i class="fa fa-star"></i></label>
											<input type="email" name="emailaddress" placeholder="Skriv inn din e-postadresse" value="<?php echo IO::get('emailaddress'); ?>" class="form-control" />
										</div>	
									</div>

									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="form-group">
											<label>Emne: <i class="fa fa-star"></i> </label>
											<input type="text" name="subject" placeholder="Skriv kort hva det gjelder" value="<?php echo IO::get('subject'); ?>" class="form-control" />
										</div>	
									</div>

									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="form-group">
											<label>Melding: <i class="fa fa-star"></i> </label>
											<textarea name="message" placeholder="Melding" class="form-control" rows="6" ><?php echo IO::get('message'); ?></textarea>
										</div>	
									</div>

									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="form-group">
											<div class="g-recaptcha" data-sitekey="6LeLZwcUAAAAABL_r-avtZupqWh743Ni1XBgUt0M"></div>
										</div>	
									</div>

									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="form-group">
											<input type="hidden" name="message_token" value="<?php echo Token::create(); ?>" />
											<button class="btn btn-success btn-lg btn-block"><i class="fa fa-send"></i> &nbsp; Send melding </button>
										</div>	
									</div>
								</div>
							</form>
						</div>

					</div>
	
				</div>
			</div>	
		</section>



<?php 
	include_once('includes/files/botarea.php'); 
?>