<?php 
	include_once('config/list-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	
	<?php 
		if(IO::exists('get')) {
			if(!empty(IO::get('action')) && IO::get('action') == "visning") {
	?>

			 <div class="container">
			 	<div class="inner-container">
			 		
			 		<div class="page-header">
			 			<h2><i class="fa fa-tv"></i> Mine visninger</h2>
			 		</div>

			 		<?php if(!empty($visninger)) { ?>
				 		<table class="table table-striped table-hover ">
							<thead>
								<tr>
									<th># ID</th>
									<th>Tittel</th>
									<th>Adresse</th>
									<th>Om</th>
									<th>Visninger</th>
								</tr>
							</thead>
							<tbody>

								<?php foreach ($visninger as $vis) { ?>
									<tr>
										<td><a href="/kampanje/vis/<?php echo $vis->AnnonseID; ?>/"><?php echo $vis->AnnonseID; ?></a></td>
										<td><?php echo $vis->Title; ?></td>
										<td><?php echo $vis->Address; ?></td>
										<td><?php echo substr($vis->Description, 0, 30); ?>...</td>
										<td><?php echo $vis->Views; ?></td>
									</tr>
								<?php } ?>

							</tbody>
						</table> 
					<?php } else { ?>
						<div class="no-shows">
							<h3>Du har ikke lagt inn noen visninger enda. </h3>
						</div>
					<?php } ?>
			 	</div>
			 </div>
				

			 <?php
			}
		}
	?>


<?php 
	include_once('includes/files/botarea.php'); 
?>