<?php 
	include_once('config/logs-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>


	 <div class="container">
	 	<div class="inner-container">

			<!-- Flashing sessions with infromation -->
			<?php 
				if(Session::exists('users')) { 
					echo writealerts(Session::flash('users'), 'success'); 
				} 
			?>

	 		<div class="page-header">
	 			<h2><i class="fa fa-cog"></i> Logg</h2>
	 		</div>

	 		<?php if(!empty($loggdata)) { ?>
		 		<table class="table table-striped table-hover ">
					<thead>
						<tr>
							<th>#ID</th>
							<th><i class="fa fa-info-circle"></i> Hva er gjort:</th>
						</tr>
					</thead>
					<tbody>

						<?php foreach ($loggdata as $log) { ?>
							<tr>
								<td><?php echo $log->LogID; ?></td>
								<td><?php echo $log->LoggText; ?></td>
							</tr>
						<?php } ?>

					</tbody>
				</table> 
			<?php } else { ?>
				<div class="no-shows">
					<h3>Du har ikke lagt inn noen visninger enda. </h3>
				</div>
			<?php } ?>


	 	</div>
	 </div>
				



<?php 
	include_once('includes/files/botarea.php'); 
?>