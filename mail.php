<html>
<head>
	<title></title>
	<meta charset="utf-8">
</head>
<body>


<table border="0" align="center" cellpadding="0" cellspacing="0" width="600" style="font-family: verdana; font-size: 12px; ">
	<tr>
		<td align="center" bgcolor="#262626" style="padding: 40px 0 30px 0; border: 1px solid #eee; border-bottom: none;">
		 	<img src="http://code.htmlnorge.no/img/small-dronelogo-dv.png" alt="Dronevisning Logo" width="20%" style="display: block; border-radius: 100px;" /><br />
		 	<b style="color: #fff; font-size: 20px;">Dronevisning</b>
		</td>
	</tr>
	<tr>
		<td bgcolor="#ddd" style="padding: 40px 30px 40px 30px; border: 1px solid #eee;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td style="font-family: verdana; font-size: 18px;">
						<b>Melding til Kundeservice.</b><br />
					</td>
				</tr>
				<tr>
					<td style="padding: 20px 0 30px 0; font-family: verdana; font-size: 14px;">
						
						<br /><hr style="border: none; border-bottom: 1px solid #eee;" />
						<br /><br />

						'. nl2br($message) .'

						<br /><br />
						<br /><hr style="border: none; border-bottom: 1px solid #eee;" />
						<br />
					

						<strong>Med vennlig hilsen</strong><br />
						'. ucfirst($firstname) .' '. ucfirst($lastname) .' <br />
						'. ucfirst($email) .' 
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td style="background: #5cb85c; padding: 30px 30px 30px 30px; border: 1px solid #eee;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="75%" style="color: #fff; font-family: verdana; font-size: 14px;">
					 	<a style="color: #fff;" href="mailto:kundeservice@dronevisning.no">kundeservice@dronevisning.no</a> <br />
					 	Kopirett &copy; 2013-'. date('Y') .', Dronevisning&reg;
					</td>

				</tr>
			</table>
		</td>
	</tr>
</table>

</body>
</html>