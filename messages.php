<?php 
	include_once('config/messages-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>


	<section>
		<div class="container">
			<div class="inner-container">

				<div class="medium-form form-center">
					<!-- Error outputting -->
					<?php 
						if(!empty($errors)) { 
							echo writeerrors($errors, '', 'En feil har oppstått.'); 
						} 
					?>

					<!-- Flashing sessions with infromation -->
					<?php 
						if(Session::exists('profile')) { 
							echo writealerts(Session::flash('profile'), 'success'); 
						} 
					?>
				</div>

				<?php if(!IO::exists('get')) { ?>
					<div class="page-header">
						<h1>Mine meldinger</h1>
					</div>

					<div class="page-header">
						<h4>INNBOX</h4>
					</div>
					<?php if(!empty($conversations)) { ?>
					<table class="table table-striped table-hover ">
						<thead>
							<tr>
								<th>Avsender/Mottaker</th>
								<th>E-post</th>
								<th>Meldings emne</th>
								<th>Opprettet</th>
								<th>Siste melding</th>
								<th>Lese/Svare</th>
							</tr>
						</thead>
						<tbody>

							
								<?php foreach ($conversations as $conversation) { 
									if($conversation->Sender == $_init_uzer_data->UserID) {
										$user_reciver = new User($conversation->Reciver);
									} else {
										$user_reciver = new User($conversation->Sender);
									}
								?>
									<tr <?php if($conversation->message_read == 1) { echo 'class="info"';} else { echo 'class="warning"'; } ?>>
										<td class="capitalized"><?php echo $user_reciver->data()->FirstName, ' ', $user_reciver->data()->LastName; ?></td>
										<td><?php echo $user_reciver->data()->EmailAddress; ?></td>
										<td><?php echo $conversation->Tema; ?></td>
										<td><?php echo $conversation->MeldingStart; ?></td> 
										<td><?php echo $conversation->last_reply; ?></td> 
										<td>
											<a href="/messages/view/<?php echo $conversation->ConversationID; ?>/" class="btn btn-xs btn-success"><i class="fa fa-folder-open-o"></i> Åpne</a>
											<a href="/messages/delete/<?php echo $conversation->ConversationID; ?>/" onclick="return confirm('Er du sikker du vil slette denne meldingen ?');" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Slett</a>
										</td>
									</tr>
								<?php } ?>
							
						</tbody>
					</table> 
					<?php } else { ?>
						<div class="container">
							<h2 class="information_empty_site">Inboksen er tom.</h2>
						</div>
					<?php } ?>
				<?php } ?>

				<?php if(IO::exists('get')) { ?>
					<?php if(IO::get('action') == "connect" && is_numeric(IO::get('id'))) { ?>
						

						<div class="messages-form medium-form form-center">
							<div class="page-header">
								<h1>Ny melding</h1>
							</div>
							<form action="" method="post">
								<div class="form-group">
									<label>Mottaker:</label>
									<input type="text" name="dummy" disabled value="<?php echo $get_user->EmailAddress; echo " (". $get_user->FirstName, ' ', $get_user->LastName .")"; ?>" class="form-control" />
									<input type="hidden" name="reciver" value="<?php echo $get_user->UserID; ?>" />
								</div>

								<div class="form-group">
									<label>Emne:</label>
									<input type="text" name="subject" value="<?php echo IO::get('subject'); ?>" placeholder="Skriv inn et emne"  class="form-control" />
								</div>

								<div class="form-group">
									<label>Melding:</label>
									<div class="bbcode-controls">
										<ul>
											<li><button onclick="settag('b', 'description')" type="button" class="btn bbcode-btn btn-default">B</button></li>
											<li><button onclick="settag('u', 'description')" type="button" class="btn bbcode-btn btn-default">U</button></li>
											<li><button onclick="settag('i', 'description')" type="button" class="btn bbcode-btn btn-default"><em>i</em></button></li>
											<li><button onclick="settag('url', 'description')" type="button" class="btn bbcode-btn btn-default"><i class="fa fa-link"></i> </button></li>
										</ul>
									</div>
									<textarea name="message" id="description" rows="6" placeholder="Melding" class="form-control"><?php echo IO::get('message'); ?></textarea>
								</div>

								<div class="form-group">
									<input type="hidden" name="msg_token" value="<?php echo Token::create(); ?>">
									<button class="btn btn-success"><i class="fa fa-send"></i> &nbsp; Send</button>
								</div>
							</form>
						</div>

					<?php } ?>
				<?php } ?>	

				<?php if(IO::exists('get')) { ?>
					<?php if(IO::get('action') == "view" && is_numeric(IO::get('id'))) { ?>
						<?php if(!empty($dataConversation)) { 
								$user_one = new User($dataConversation->Sender);
								$user_one = $user_one->data();
								$user_two = new User($dataConversation->Reciver);
								$user_two = $user_two->data();
						?>

							<div class="page-header">
								<h1><?php echo $dataConversation->Tema; ?></h1>
							</div>
							<ol class="breadcrumb">
								<strong>Personer i Samtalen &nbsp; | &nbsp; </strong>
								<li class="capitalized"><?php echo ucfirst($user_one->FirstName), ' ', ucfirst($user_one->LastName), " (". $user_one->EmailAddress .")"; ?></li>
								<li><?php echo ucfirst($user_two->FirstName), ' ', ucfirst($user_two->LastName), " (". $user_two->EmailAddress .")"; ?></li>
							</ol>

							<div class="answer-form">
								<form action="" method="post">
									<div class="form-group">
										<label>Melding:</label>
										<div class="bbcode-controls">
											<ul>
												<li><button onclick="settag('b', 'description')" type="button" class="btn bbcode-btn btn-default">B</button></li>
												<li><button onclick="settag('u', 'description')" type="button" class="btn bbcode-btn btn-default">U</button></li>
												<li><button onclick="settag('i', 'description')" type="button" class="btn bbcode-btn btn-default"><em>i</em></button></li>
												<li><button onclick="settag('url', 'description')" type="button" class="btn bbcode-btn btn-default"><i class="fa fa-link"></i> </button></li>
											</ul>
										</div>
										<textarea name="message" id="description" rows="6" placeholder="Melding" class="form-control"><?php echo IO::get('description'); ?></textarea>
									</div>

									<input type="hidden" name="reply_token" value="<?php echo Token::create(); ?>">
									<input type="hidden" name="convid" value="<?php echo $dataConversation->ConversationID; ?>">
									<button class="btn btn-success btn-block"><i class="fa fa-send"></i> &nbsp; Svar </button>
								</form>
							</div>

							<hr />

							<?php if(!empty($dataMessages)) { ?>
								<?php foreach($dataMessages as $msg) { 
										$user_msg = new User($msg->Sender);
										$user_msg = $user_msg->data();
								?>
									<div class="row paddin-20">
										<div class="col-md-6 col-sm-6 col-xs-12 message-bubble <?php if($msg->Sender != $_init_uzer_data->UserID) { echo "other"; } ?>">
											<?php echo ucfirst($user_msg->FirstName), ' ', ucfirst($user_msg->LastName), ' / ', $msg->Date; ?> <hr>
											<?php echo nl2br(bbcode($msg->Content)); ?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				<?php } ?>

			</div>
		</div>	
	</section>


<?php 
	include_once('includes/files/botarea.php'); 
?>