<?php 
	include_once('config/operators-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	<div class="container">
		<div class="inner-container ">

			<!-- Error outputting -->
			<?php 
				if(!empty($errors)) { 
					echo writeerrors($errors, '', 'En feil har oppstått.'); 
				} 
			?>

			<!-- Flashing sessions with infromation -->
			<?php 
				if(Session::exists('changepw')) { 
					echo writealerts(Session::flash('changepw'), 'success'); 
				} 
			?>

			
			<div class="operator">
				<div class="operator-header">
					<div class="row">
						<div class="col-md-4 col-sm-4">
							<div class="operator-picture-box">
								<div class="operator-name">
									<?php echo $show_data->FirstName, ' ', $show_data->LastName; ?>
								</div>
								<div class="operator-img">
									<img src="res/uploads/<?php if($show_data->Picture) { echo "t_" . $show_data->Picture; } else { echo "default.jpg";} ?>" alt="operator">
								</div>
								<div class="rating-box">
									<?php echo getStars(0); ?>
								</div>
							</div>
						</div>

						<div class="col-md-8 col-sm-8">
							<div class="operator-skills-box">
								<div class="col-md-6 col-sm-6">
									<?php 
										if(!empty($checkdata)) {
											if ($checkdata->videos) { 
												echo '<span class="green"><i class="fa fa-check"></i></span> &nbsp; Video';
											} else {
												echo '<span class="red"><i class="fa fa-remove"></i></span> &nbsp; Video';
											}
										}
									?>
									
								</div>
								<div class="col-md-6 col-sm-6">
									<?php 
										if(!empty($checkdata)) {
											if ($checkdata->images) { 
												echo '<span class="green"><i class="fa fa-check"></i></span> &nbsp; Bilde';
											} else {
												echo '<span class="red"><i class="fa fa-remove"></i></span> &nbsp; Bilde';
											}
										}
									?> 
								</div>
								<div class="col-md-6 col-sm-6">
									<?php 
										if(!empty($checkdata)) {
											if ($checkdata->raw_videos) { 
												echo '<span class="green"><i class="fa fa-check"></i></span> &nbsp; Råfilm';
											} else {
												echo '<span class="red"><i class="fa fa-remove"></i></span> &nbsp; Råfilm';
											}
										}
									?> 
								</div>
								<div class="col-md-6 col-sm-6">
									<?php 
										if(!empty($checkdata)) {
											if ($checkdata->raw_images) { 
												echo '<span class="green"><i class="fa fa-check"></i></span> &nbsp; Råfoto';
											} else {
												echo '<span class="red"><i class="fa fa-remove"></i></span> &nbsp; Råfoto';
											}
										}
									?> 
								</div>
								<div class="col-md-6 col-sm-6">
									<?php 
										if(!empty($checkdata)) {
											if ($checkdata->edited_videos) { 
												echo '<span class="green"><i class="fa fa-check"></i></span> &nbsp; Redigert video';
											} else {
												echo '<span class="red"><i class="fa fa-remove"></i></span> &nbsp; Redigert video';
											}
										}
									?> 
								</div>
								<div class="col-md-6 col-sm-6">
									<?php 
										if(!empty($checkdata)) {
											if ($checkdata->edited_images) { 
												echo '<span class="green"><i class="fa fa-check"></i></span> &nbsp; Redigert foto';
											} else {
												echo '<span class="red"><i class="fa fa-remove"></i></span> &nbsp; Redigert foto';
											}
										}
									?> 
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="contact">
					<div class="row no-border">
						<div class="col-md-3 col-sm-6">
							<i class="fa fa-phone"></i> &nbsp; <?php echo $show_data->PhoneNumber; ?>
						</div>

						<div class="col-md-3 col-sm-6">
							<i class="fa fa-envelope-o "></i> &nbsp; <?php echo $show_data->EmailAddress; ?>
						</div>

						<div class="col-md-3 col-sm-6">
							<i class="fa fa-check"></i> &nbsp; Verifisert
						</div>
						<div class="col-md-3 col-sm-6 no-border">
							<a href="messages/connect/<?php echo $show_data->UserID; ?>/" class="btn btn-success"><i class="fa fa-send "></i> &nbsp; Send melding</a>
						</div>

					</div>
				</div>


				<div class="operator-content">
					<div class="operator-info">
						<div class="operator-title">Detaljer</div>
						<div class="operator-info-text">
							<?php echo nl2br($show_data->UserBio); ?>
						</div>
					</div>
					<div class="operator-presnetations">
						<div class="operator-title">Visninger</div>
						<div class="presentations">
							<div class="row">
								<?php if(!empty($show_presentations)) { ?>
									

									<?php foreach ($show_presentations as $visning) { ?>
										<?php  
											$images = new Get();
											$images->get_data_by_id("images", "AnnonseID", $visning->AnnonseID);
											$images = $images->results();
										?>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="visning-block" title="<?php echo $visning->Title; ?>">
												<a href="/kampanje/vis/<?php echo $visning->AnnonseID; ?>/" class="block-link">
													<div class="visning-bilde">
														<?php foreach ($images as $img) {  ?>
															<?php if($img->Prim == 1) { ?>
																<img src="res/uploads/visning/t_<?php echo $img->Path; ?>" title="<?php echo $visning->Title; ?>" alt="<?php echo $visning->Title; ?>">
															<?php } ?>
														<?php } ?>
														<div class="play-button"><i class="fa fa-play"></i> </div>
													</div>
													<div class="visning-title">
														<h3><?php echo $visning->Title; ?></h3>
														<p><?php echo nl2br(bbcode(substr($visning->Description, 0, 140))); ?>...</p>
													</div>
												</a>
												<div class="visning-controls">
													<a href="/kampanje/vis/<?php echo $visning->AnnonseID; ?>/" class="btn btn-success btn-block"><i class="fa fa-eye"></i> &nbsp; Detaljer </a>
												</div>
											</div>
										</div>
									<?php } ?>


								<?php } else { ?>
									<div class="col-md-12 col-sm-12">
										Vi fant ingen visninger av denne dronefotografen.
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>



<?php 
	include_once('includes/files/botarea.php'); 
?>