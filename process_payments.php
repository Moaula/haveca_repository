<?php

//	Include files:
	include_once('appfiles/core/init.php');

	if(IO::exists()) {
		if(Token::check(IO::get('payment_token'))) {

			$ID = (int)IO::get('bill_id');
			$billing = new Get();
			$billing->get_data_by_id("contracts", "ContractID", $ID);
			$billing = $billing->result();
			
			$buyer_info = new User($billing->BuyerID);
			$buyer_info = $buyer_info->data();

			$selle_info = new User($billing->SellerID);
			$selle_info = $selle_info->data();

			$payment_amount = ($billing->Price * 100);
			$dataUpdate = new Put();

			$string = ucfirst($buyer_info->FirstName);
			$string .= ' ' .ucfirst($buyer_info->LastName);
			$string .= ' betaler kr '.$billing->Price;  
			$string .= ' til '.ucfirst($selle_info->FirstName);
			$string .= ' ' .ucfirst($selle_info->LastName);
			$string .= ' e-post ' .ucfirst($selle_info->EmailAddress); 
					  

			if(isset($_POST['stripeToken'])) {

				$token = trim(IO::get('stripeToken'));
				\Stripe\Stripe::setApiKey("sk_test_0D5Upnpj88zQGsRkBnfvZIs0");

				try {
				
					\Stripe\Charge::create(array(
						"amount" => $payment_amount,
						"currency" => "nok",
						"source" => $token, // obtained with Stripe.js
						"description" => $string
					));

					$dataUpdate->update("contracts", array(
						"Paid"	=> 1,
						"Token" => $token,
						"PayDate" => date('Y-m-d')
					), $billing->ContractID, "ContractID");

					$LoggText = date("d-m-Y") . " En betaling er gjennomført: ";
					// $LoggText .= ucfirst($_init_uzer_data->FirstName) . " " . ucfirst($_init_uzer_data->LastName);
					// $LoggText .= " Stengte kontoen til bruker id: ". IO::get('id');

					$handler = new Put();
					$handler->create('changelogs', array(
						"LoggText" => $LoggText
					));

					Session::flash('billig',  'Fakturaen er nå betalt.');
					Redirect::to('/billing');


				} catch(\Stripe\Error\Card $e) {
					// Since it's a decline, \Stripe\Error\Card will be caught
					$body = $e->getJsonBody();
					$err  = $body['error'];

					print('Status is:' . $e->getHttpStatus() . "\n");
					print('Type is:' . $err['type'] . "\n");
					print('Code is:' . $err['code'] . "\n");
					
					// param is '' in this case
					print('Param is:' . $err['param'] . "\n");
					print('Message is:' . $err['message'] . "\n");

					$errors[] = $e->getMessage();

				} catch (\Stripe\Error\RateLimit $e) {
					$errors[] = $e->getMessage();
				} catch (\Stripe\Error\Authentication $e) {
				  // Authentication with Stripe's API failed
				  // (maybe you changed API keys recently)
					$errors[] = $e->getMessage();
				} catch (\Stripe\Error\ApiConnection $e) {
				  // Network communication with Stripe failed
					$errors[] = $e->getMessage();
				} catch (\Stripe\Error\Base $e) {
				  // Display a very generic error to the user, and maybe send
				  // yourself an email
					$errors[] = $e->getMessage();
				} catch (Exception $e) {
					// Something else happened, completely unrelated to Stripe
					$errors[] = $e->getMessage();
				}
			}
		}
	}

	if(!empty($errors)) {
		print_r($errors);
	}