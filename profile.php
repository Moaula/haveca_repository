<?php 
	include_once('config/profile-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	<div class="container">
		<div class="inner-container medium-form form-center">

						<!-- Error outputting -->
			<?php 
				if(!empty($errors)) { 
					echo writeerrors($errors, '', 'En feil har oppstått.'); 
				} 
			?>

			<!-- Flashing sessions with infromation -->
			<?php 
				if(Session::exists('profile')) { 
					echo writealerts(Session::flash('profile'), 'success'); 
				} 
			?>

			<div class="entry-title">Min info</div>
			<div class="form-information">
				<p>
					<strong>Felt markert med <i class="fa fa-star"></i> kreves. </strong>
					<hr />
				</p>
			</div>

			<?php if($_init_uzer->hasPerm('operator')) { ?>

				<form action="" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>Fornavn: <i class="fa fa-star"></i></label>
								<input type="text" name="firstname" class="form-control" value="<?php echo ucfirst($_init_uzer_data->FirstName); ?>" />
							</div>
						</div>
						
						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>Etternavn: <i class="fa fa-star"></i></label>
								<input type="text" name="lastname" class="form-control" value="<?php echo ucfirst($_init_uzer_data->LastName); ?>" />
							</div>
						</div>

						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>Fødselsdato: <i class="fa fa-star"></i></label>
								<input type="date" name="BirthDay" class="form-control" value="<?php echo ucfirst($_init_uzer_data->BirthDay); ?>" disabled />
							</div>
						</div>

						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>E-post adresse: <i class="fa fa-star"></i></label>
								<input type="text" name="EmailAddress" class="form-control" value="<?php echo ucfirst($_init_uzer_data->EmailAddress); ?>" disabled />
							</div>
						</div>

						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>Adresse: <i class="fa fa-star"></i></label>
								<input type="text" name="address" class="form-control" value="<?php echo ucfirst($_init_uzer_data->Address); ?>" />
							</div>
						</div>

						<div class="col-md-3 col-sm-3 col-lg-3">
							<div class="form-group">
								<label>Poststed: <i class="fa fa-star"></i></label>
								<input type="text" name="zipplace" class="form-control" value="<?php echo ucfirst($_init_uzer_data->ZipPlace); ?>" />
							</div>
						</div>

						<div class="col-md-3 col-sm-3 col-lg-3">
							<div class="form-group">
								<label>Postnummer: <i class="fa fa-star"></i></label>
								<input type="number" name="zipcode" class="form-control" value="<?php echo ucfirst($_init_uzer_data->ZipCode); ?>" />
							</div>
						</div>

						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>Telefonnummer: <i class="fa fa-star"></i></label>
								<input type="number" name="phonenumber" class="form-control" value="<?php echo ucfirst($_init_uzer_data->PhoneNumber); ?>" />
							</div>
						</div>

						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>Organisasjonsnummer: <i class="fa fa-star"></i></label>
								<input type="text" name="orgnumber" class="form-control" value="<?php echo ucfirst($_init_uzer_data->OrgNumber); ?>" />
							</div>
						</div>

						<div class="col-md-12 col-sm-12 col-lg-12">
							<div class="form-group">
								<label>Beskrivelse</label>
								<textarea name="userbio" class="form-control" rows="6"><?php echo ucfirst($_init_uzer_data->UserBio); ?></textarea>
							</div>
						</div>
						
						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>Bilde</label>
								<input type="file" name="image"  />
							</div>
						</div>

						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group formimg">
								<label>Gjeldende bilde:</label><br />
								<?php if($_init_uzer_data->Picture) { ?>
									<img src="res/uploads/t_<?php echo $_init_uzer_data->Picture; ?>" alt="Gjeldende bilde">
									<input type="hidden" name="old_image" value="<?php echo $_init_uzer_data->Picture; ?>" />
								<?php } else { ?>
									Ingen bilde er valgt tidligere.
								<?php } ?>
							</div>
						</div>

						<div class="col-md-12 col-sm-12 col-lg-12">
							<div class="form-information">
								<p>
									<hr />
									<h3><strong>Tilbyr</strong></h3>
									<hr />
								</p>
							</div>
						</div>

						<div class="col-md-3 col-sm-3 col-lg-3">
							<div class="form-group btn btn-default btn-sm check_btn">
								<label><input type="checkbox" name="videos" <?php if(!empty($checkdata)) { if ($checkdata->videos) { echo "checked"; } } ?>  /> &nbsp; Dronevideo</label>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-lg-3">
							<div class="form-group btn btn-default btn-sm check_btn">
								<label><input type="checkbox" name="images" <?php  if(!empty($checkdata)) { if ($checkdata->images) { echo "checked"; }} ?> /> &nbsp; Dronefoto</label>
							</div>
						</div>

						<div class="col-md-3 col-sm-3 col-lg-3">
							<div class="form-group btn btn-default btn-sm check_btn">
								<label><input type="checkbox" name="raw_videos" <?php if(!empty($checkdata)) { if ($checkdata->raw_videos) { echo "checked"; }} ?> /> &nbsp; Råfilm</label>
							</div>
						</div>

						<div class="col-md-3 col-sm-3 col-lg-3">
							<div class="form-group btn btn-default btn-sm check_btn">
								<label><input type="checkbox" name="raw_images" <?php if(!empty($checkdata)) { if ($checkdata->raw_images) { echo "checked"; }} ?>  /> &nbsp; Råfoto</label>
							</div>
						</div>

						<div class="col-md-3 col-sm-3 col-lg-3">
							<div class="form-group btn btn-default btn-sm check_btn">
								<label><input type="checkbox" name="edited_videos" <?php if(!empty($checkdata)) { if ($checkdata->edited_videos) { echo "checked"; }} ?> /> &nbsp; Redigert video</label>
							</div>
						</div>

						<div class="col-md-3 col-sm-3 col-lg-3">
							<div class="form-group btn btn-default btn-sm check_btn">
								<label><input type="checkbox" name="edited_images" <?php if(!empty($checkdata)) {  if ($checkdata->edited_images) { echo "checked"; }} ?> /> &nbsp; Redigert foto</label>
							</div>
						</div>


						<div class="col-md-12 col-sm-12 col-lg-12">
							<hr />
							<div class="form-group">
								<label>Skriv inn passord for å oppdatere: <i class="fa fa-star"></i></label>
								<input type="password" name="password" class="form-control" placeholder="Skriv inn passordet ditt for å oppdatere info..." />
							</div>
						</div>

						

						<div class="col-md-12 col-sm-12 col-lg-12">
							<hr />
							<input type="hidden" name="update_data_operator" value="<?php echo Token::create(); ?>">
							<button class="btn btn-success"><i class="fa fa-refresh"></i> &nbsp; Oppdater </button>
							<a href="/" class="btn btn-default"><i class="fa fa-remove"></i> &nbsp; Avbryt </a>
						</div>
					</div>
				</form>

			<?php } else { ?>

				<form action="" method="post">
					<div class="row">
						
						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>Fornavn</label>
								<input name="firstname" class="form-control" value="<?php echo ucfirst($_init_uzer_data->FirstName); ?>" />
							</div>
						</div>
						
						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>Etternavn</label>
								<input name="lastname" class="form-control" value="<?php echo ucfirst($_init_uzer_data->LastName); ?>" />
							</div>
						</div>
						
						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>Fødselsdato</label>
								<input name="BirthDay" class="form-control" value="<?php echo ucfirst($_init_uzer_data->BirthDay); ?>" disabled />
							</div>
						</div>
						
						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>E-post adresse</label>
								<input name="EmailAddress" class="form-control" value="<?php echo ucfirst($_init_uzer_data->EmailAddress); ?>" disabled />
							</div>
						</div>
						
						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>Adresse</label>
								<input name="address" class="form-control" value="<?php echo ucfirst($_init_uzer_data->Address); ?>" />
							</div>
						</div>
						
						<div class="col-md-3 col-sm-3 col-lg-3">
							<div class="form-group">
								<label>Poststed</label>
								<input name="zipplace" class="form-control" value="<?php echo ucfirst($_init_uzer_data->ZipPlace); ?>" />
							</div>
						</div>
						
						<div class="col-md-3 col-sm-3 col-lg-3">
							<div class="form-group">
								<label>Postnummer</label>
								<input name="zipcode" class="form-control" value="<?php echo ucfirst($_init_uzer_data->ZipCode); ?>" />
							</div>
						</div>

						<div class="col-md-6 col-sm-6 col-lg-6">
							<div class="form-group">
								<label>Telefonnummer</label>
								<input name="phonenumber" class="form-control" value="<?php echo ucfirst($_init_uzer_data->PhoneNumber); ?>" />
							</div>
						</div>

						<div class="col-md-12 col-sm-12 col-lg-12">
							<div class="form-group">
								<label>Skriv inn passord for å oppdatere</label>
								<input name="password" class="form-control" placeholder="Skriv inn passordet ditt for å oppdatere info..." />
							</div>
						</div>

						<div class="col-md-12 col-sm-12 col-lg-12">
							<input type="hidden" name="update_data_kunde" value="<?php echo Token::create(); ?>">
							<button class="btn btn-success"><i class="fa fa-refresh"></i> &nbsp; Oppdater </button>
							<a href="/" class="btn btn-default"><i class="fa fa-remove"></i> &nbsp; Avbryt </a>
						</div>
					</div>
				</form>

			<?php } ?>
		</div>
	</div>



<?php 
	include_once('includes/files/botarea.php'); 
?>