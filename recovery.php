<?php 
	include_once('config/recovery-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>
	<!-- Container for options panel -->
	<div class="container">
		<div class="inner-container login-container medium-form form-center">
			<!-- Error outputting -->
			<?php 
				if(!empty($errors)) { 
					echo writeerrors($errors, '', 'En feil har oppstått.'); 
				} 
			?>

			<?php 
				if(IO::exists('get')) { 
					if(IO::get('action') == "username") {
			?>

						<div class="entry-title">Gjenopprett brukernavn</div>
						<form action="" method="post" enctype="multipart/form-data" class="form-body" role="signup">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6">
									<div class="form-group">
										<label>Fornavn</label>
										<input type="text" name="firstname" class="form-control" value="<?php echo IO::get('firstname'); ?>" placeholder="Hva er ditt fornavn" />
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
									<div class="form-group">
										<label>Etternavn</label>
										<input type="text" name="lastname" class="form-control" value="<?php echo IO::get('lastname'); ?>" placeholder="Hva er ditt etternavn" />
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
									<div class="form-group">
										<label>Fødselsdato</label>
										<input type="date" name="birthday" class="form-control" value="<?php echo IO::get('birthday'); ?>" placeholder="Hva er din fødselsdato" />
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
									<div class="form-group">
										<label>E-post adresse</label>
										<input type="email" name="emailaddress" class="form-control" value="<?php echo IO::get('emailaddress'); ?>" placeholder="Hvor kan vi kontakte deg?" />
									</div>
								</div>
							</div>


							<div class="form-group">
								<input type="hidden" name="recover_username_token" value="<?php echo Token::create(); ?>">
								<button class="btn btn-success"><i class="fa fa-refresh"></i> &nbsp; Gjenopprett brukernavn  </button>
							</div>
						</form>

			<?php 
					}
					if(IO::get('action') == "password") { 
			?>

						<div class="entry-title">Gjenopprett passord</div>
						<form action="" method="post" enctype="multipart/form-data" class="form-body" role="recover">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12">
									<div class="form-group">
										<label>E-post adresse:</label>
										<input type="email" name="emailaddress" class="form-control" placeholder="Skriv inn din registrerte e-post adresse" value="<?php echo IO::get('emailaddress'); ?>" />
									</div>
								</div>
							</div>


							<div class="form-group">
								<input type="hidden" name="recover_password_token" value="<?php echo Token::create(); ?>">
								<button class="btn btn-success"><i class="fa fa-send"></i> &nbsp; Send nytt passord  </button>
							</div>
						</form>


			<?php 
					}
				}
			?>
			


		</div>
	</div>




<?php 
	include_once('includes/files/botarea.php'); 
?>