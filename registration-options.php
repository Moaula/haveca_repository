<?php 
	include_once('config/registration-options-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>
	<!-- Container for options panel -->
	<div class="container">
		<div class="inner-container">
			<!-- registration options -->
			<div class="registration-selection">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 border-right">
						
						<div class="selection-title">
							BRUKER
						</div>
						<div class="selection-info">

							<div class="list-items">
								<div class="li-left"><i class="fa fa-check"></i> </div>
								<div class="li-right">
									Kontakt en sertifisert dronefotograf og avtal innhold og pris for din presentasjon.
								</div>
							</div>

							<div class="list-items">
								<div class="li-left"><i class="fa fa-check"></i> </div>
								<div class="li-right">
									Din dronefotograf filmer og redigerer din presentasjon.
								</div>
							</div>

							<div class="list-items">
								<div class="li-left"><i class="fa fa-check"></i> </div>
								<div class="li-right">
									Du betaler når du har mottatt din presentasjon. Din presentasjon publiseres på dronevisning og i sosiale medier.
								</div>
							</div>

						</div>
						<div class="selection-action">
							<a href="/signup/kunde/" class="btn btn-success btn-lg btn-block">REGISTRER MEG</a>
						</div>

					</div>	

					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="selection-title">
							Dronefotograf
						</div>
						<div class="selection-info">

							<div class="list-items">
								<div class="li-left"><i class="fa fa-check"></i> </div>
								<div class="li-right">
									Enkelt. Beskriv hva du tilbyr og si noe om deg selv. Presenter film og foto som viser hva du kan.
								</div>
							</div>

							<div class="list-items">
								<div class="li-left"><i class="fa fa-check"></i> </div>
								<div class="li-right">
									Vi viser deg fram. Dine tjenester blir synlige for kunder over hele landet.
								</div>
							</div>

							<div class="list-items">
								<div class="li-left"><i class="fa fa-check"></i> </div>
								<div class="li-right">
									Du leverer. Du kommuniserer med kunder, avtaler pris og innhold og leverer.
								</div>
							</div>

						</div>	
						<div class="selection-action">
							<a href="/signup/operator/" class="btn btn-success btn-lg btn-block">BLI DRONEFOTOGRAF</a>
						</div>									
					</div>	
				</div>
			</div>
		</div>

	</div>

<?php 
	include_once('includes/files/botarea.php'); 
?>