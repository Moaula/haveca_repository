<?php 
	include_once('config/search-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>
	<section class="search-bar">
		<div class="container">
			<div class="medium-form form-center">
				<form action="search" method="get">
					<div class="col-md-10 col-sm-10 col-xs-10 nopadding">
						<input type="text" name="qry" value="<?php echo IO::get('qry'); ?>" placeholder="SØK MED POSTNR, STED, NAVN ELLER INNHOLD I VISNINGER" class="form-control search-bar-bar">
					</div>
					<div class="col-md-2 col-sm-2 col-xs-2 nopadding">
						<button class="btn btn-success btn-lg btn-block"><i class="fa fa-search"></i> &nbsp; Søk</button>
					</div>
				</form>
			</div>
		</div>
	</section>


	<?php if(!empty($visninger)) { ?>
		<section class="visninger">
			<div class="container">
				<div class="page-header">
					<h1>Resulter i visninger (<?php if($visninger_count) { echo $visninger_count; } else { echo "0"; } ?>)</h1>
				</div>
				
				<div class="row">
					<?php foreach ($visninger as $visning) { ?>
						<?php  
							$images = new Get();
							$images->get_data_by_id("images", "AnnonseID", $visning->AnnonseID);
							$images = $images->results();
						?>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="visning-block">
								<a href="/kampanje/vis/<?php echo $visning->AnnonseID; ?>/" class="block-link">
									<div class="visning-bilde">
										<?php foreach ($images as $img) {  ?>
											<?php if($img->Prim == 1) { ?>
												<img src="res/uploads/visning/t_<?php echo $img->Path; ?>">
											<?php } ?>
										<?php } ?>
										<div class="play-button"><i class="fa fa-play"></i> </div>
									</div>
									<div class="visning-title">
										<h3><?php echo $visning->Title; ?></h3>
										<p><?php echo nl2br(bbcode(substr($visning->Description, 0, 140))); ?>...</p>
									</div>
								</a>
								<div class="visning-controls">
									<a href="/kampanje/vis/<?php echo $visning->AnnonseID; ?>/" class="btn btn-success btn-block"><i class="fa fa-eye"></i> &nbsp; Detaljer </a>
								</div>
							</div>
						</div>
					<?php } ?>

				</div>

			</div>
		</section>
	<?php } ?>

	<?php if(0) { ?>
		<section class="dark">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="page-header">
							<h2>Leverandører</h2>
						</div>
					</div>
					<?php foreach ($operators as $operator) { ?>
						<div class="col-md-3 col-sm-4">
							<div class="operator-block">
								<div class="operator-picture-box">
									<div class="operator-img">
										<img title="<?php echo $operator->FirstName, ' ', $operator->LastName; ?>" src="res/uploads/<?php if($operator->Picture) { echo "t_" . $operator->Picture; } else { echo "default.jpg"; } ?> " alt="operator">
									</div>
									<div class="disp-name"><?php echo $operator->FirstName, ' ', $operator->LastName; ?></div>
									<div class="rating-box">
										<?php echo getStars(0); ?>
									</div>
								</div>
								<div class="operator-contact">
									<a href="/operators/show/<?php echo $operator->UserID; ?>/" class="btn btn-info btn-block"><i class="fa fa-eye"></i> Vis profil</a>
									<a href="/messages/connect/<?php echo $operator->UserID; ?>/" class="btn btn-success btn-block"><i class="fa fa-envelope-o"></i> Ta kontakt</a>
								</div>
							</div>
						</div>
					<?php } ?>		
				</div>
			</div>	
		</section>
	<?php } ?>


<?php 
	include_once('includes/files/botarea.php'); 
?>