<?php include_once('config/signup-cfg.php'); include_once('includes/files/toparea.php'); ?>
	
	<!-- Container for options panel -->
	<div class="container">
		<div class="inner-container">		
			<?php if(IO::exists('get') && IO::get('action') == 'kunde' && IO::get('alt') == 'success') { ?>

				<div class="registration-from small-form form-center">
					<div class="form-title">Gratulerer</div>

					<!-- Flashing sessions with infromation -->
					<?php  if(Session::exists('signup')) { echo writealerts(Session::flash('signup'), 'success'); } ?>

					<a href="/" class="btn btn-default btn-block">
						<i class="fa fa-home"></i> &nbsp; Tilbake til hjemmesiden 
					</a>
				</div>

			<?php } ?>

			
			<?php if(IO::exists('get') && IO::get('action') == 'kunde' && empty(IO::get('alt'))) { ?>
				
				<!-- Registrering av kunde -->
				<div class="registration-from">

					<!-- Error outputting -->
					<?php if(!empty($errors)) { echo writeerrors($errors); } ?>

					<div class="form-title">bruker registrering</div>
					<form action="" method="post" enctype="multipart/form-data" class="form-body" role="signup">
						
						<div class="row">
							
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group">
									<label>Fornavn</label>
									<input type="text" name="firstname" placeholder="Hva er ditt navn?" class="form-control" value="<?php echo IO::get('firstname'); ?>" />
								</div>

								<div class="form-group">
									<label>Etternavn</label>
									<input type="text" name="lastname" placeholder="Hva er ditt etternavn?" class="form-control" value="<?php echo IO::get('lastname'); ?>" />
								</div>

								<div class="form-group">
									<label>Fødselsdato</label>
									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<select name="day" class="form-control">
												<?php if(empty(IO::get('day'))) { echo '<option selected value="">Dag</option>'; } else { echo '<option selected value="'. IO::get('day') .'">'. IO::get('day') .'</option>'; } ?>
												<?php 
													for ($x=1; $x <= 31; $x++) { 
														echo "<option value=". $x .">". $x ."</option>";
													}
												?>
											</select>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<select name="month" class="form-control">
												<?php if(empty(IO::get('month'))) { echo '<option selected value="">Måned</option>'; } else { echo '<option selected value="'. IO::get('month') .'">'. IO::get('month') .'</option>'; } ?>
												<?php 
													for ($x=1; $x <= 12; $x++) { 
														echo "<option value=". $x .">". $x ."</option>";
													}
												?>
											</select>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<select name="year" class="form-control">
												<?php if(empty(IO::get('year'))) { echo '<option selected value="">År</option>'; } else { echo '<option selected value="'. IO::get('year') .'">'. IO::get('year') .'</option>'; } ?>
												<?php 
													for ($x= date('Y') - 18; $x >= 1945; $x--) { 
														echo "<option value=". $x .">". $x ."</option>";
													}
												?>
											</select>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label>Telefonnummer (8 siffer)</label>
									<input type="number" name="phonenumber" placeholder="Hva er ditt telefonnummer?" class="form-control" value="<?php echo IO::get('phonenumber'); ?>" />
								</div>

								<div class="row">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group">
											<label>Adresse</label>
											<input type="text" name="address" placeholder="Hans Gate 17" class="form-control" value="<?php echo IO::get('address'); ?>" />
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
										<div class="form-group">
											<label>Postnummer</label>
											<input type="number" name="zipcode" placeholder="9080" class="form-control" value="<?php echo IO::get('zipcode'); ?>" />
										</div>
									</div>
								</div>
							</div><!-- end left half div -->

							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group">
									<label>Poststed</label>
									<input type="text" name="zipplace" placeholder="Lillehammer" class="form-control" value="<?php echo IO::get('zipplace'); ?>" />
								</div>

								<div class="form-group">
									<label>E-post adresse</label>
									<input type="email" name="emailaddress" placeholder="Hva er din e-post adresse?" class="form-control" value="<?php echo IO::get('emailaddress'); ?>" />
								</div>

								<div class="form-group">
									<label>Ønsket passord (minst 8 tegn)</label>
									<input type="password" name="password" placeholder="Skriv inn ønsket passord" class="form-control" />
								</div>

								<div class="form-group">
									<label>Bekreft passord</label>
									<input type="password" name="passwordr" placeholder="Gjenta ønsket passord" class="form-control" />
								</div>

								<div class="form-group bottom-group">
									<label>
										<input name="termsofservice" type="checkbox" /> Jeg har lest og godtatt 
										<a href="#" target="_blank">Vilkår</a> og 
										<a href="#" target="_blank">Personverns lover</a>.
									</label>
									<input type="hidden" name="reg_kunde_token" value="<?php echo Token::create(); ?>">
									<button class="btn btn-success btn-block">REGISTRER MEG</button>
								</div>
							</div> <!-- end right half div -->
						</div><!-- end row div -->
					</form><!-- end kunde registering div -->
				</div>
			<?php } ?><!-- end if kunde -->


			<?php if(IO::exists('get') && IO::get('action') == 'operator' && IO::get('alt') == 'success') { ?>
				
				<div class="registration-from small-form form-center">
					
					<div class="form-title">Gratulerer</div>

					<!-- Flashing sessions with infromation -->
					<?php if(Session::exists('signup')) { echo writealerts(Session::flash('signup'), 'success'); } ?>

					<a href="/" class="btn btn-default btn-block">
						<i class="fa fa-home"></i> &nbsp; Tilbake til hjemmesiden 
					</a>
				</div>

			<?php } ?>

			<?php if(IO::exists('get') && IO::get('action') == 'operator' && empty(IO::get('alt'))) { ?>
				<div class="registration-from">

					<!-- Error outputting -->
					<?php if(!empty($errors)) { echo writeerrors($errors); } ?>

					<div class="form-title">Dronefotograf registrering</div>
					<form action="" method="post" enctype="multipart/form-data" class="form-body" role="signup">
						
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group">
									<label>Fornavn</label>
									<input type="text" name="firstname" placeholder="Hva er ditt navn?" class="form-control" value="<?php echo IO::get('firstname'); ?>" />
								</div>

								<div class="form-group">
									<label>Etternavn</label>
									<input type="text" name="lastname" placeholder="Hva er ditt etternavn?" class="form-control" value="<?php echo IO::get('lastname'); ?>" />
								</div>

								<div class="form-group">
									<label>Fødselsdato</label>
									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<select name="day" class="form-control">
												<?php if(empty(IO::get('day'))) { echo '<option selected value="">Dag</option>'; } else { echo '<option selected value="'. IO::get('day') .'">'. IO::get('day') .'</option>'; } ?>
												<?php 
													for ($x=1; $x <= 31; $x++) { 
														echo "<option value=". $x .">". $x ."</option>";
													}
												?>
											</select>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<select name="month" class="form-control">
												<?php if(empty(IO::get('month'))) { echo '<option selected value="">Måned</option>'; } else { echo '<option selected value="'. IO::get('month') .'">'. IO::get('month') .'</option>'; } ?>
												<?php 
													for ($x=1; $x <= 12; $x++) { 
														echo "<option value=". $x .">". $x ."</option>";
													}
												?>
											</select>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<select name="year" class="form-control">
												<?php if(empty(IO::get('year'))) { echo '<option selected value="">År</option>'; } else { echo '<option selected value="'. IO::get('year') .'">'. IO::get('year') .'</option>'; } ?>
												<?php 
													for ($x= date('Y') - 18; $x >= 1945; $x--) { 
														echo "<option value=". $x .">". $x ."</option>";
													}
												?>
											</select>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label>Telefonnummer (8 siffer)</label>
									<input type="number" name="phonenumber" placeholder="Hva er ditt telefonnummer?" class="form-control" value="<?php echo IO::get('phonenumber'); ?>" />
								</div>

								<div class="form-group">
									<label>E-post adresse</label>
									<input type="email" name="emailaddress" placeholder="Hva er din e-post adresse?" class="form-control" value="<?php echo IO::get('emailaddress'); ?>" />
								</div>

								<div class="row">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group">
											<label>Adresse</label>
											<input type="text" name="address" placeholder="Hans Gate 17" class="form-control" value="<?php echo IO::get('address'); ?>" />
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
										<div class="form-group">
											<label>Postnummer</label>
											<input type="number" name="zipcode" placeholder="9080" class="form-control" value="<?php echo IO::get('zipcode'); ?>" />
										</div>
									</div>
								</div>
							</div><!-- end left half div -->

							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group">
									<label>Poststed</label>
									<input type="text" name="zipplace" placeholder="Lillehammer" class="form-control" value="<?php echo IO::get('zipplace'); ?>" />
								</div>
								
								<div class="form-group">
									<label>Organisasjonsnummer</label>
									<input type="number" name="orgnum" placeholder="skirv inn orgnummer?" class="form-control" value="<?php echo IO::get('orgnum'); ?>" />
								</div>

								<div class="form-group">
									<label>Ønsket passord (minst 8 tegn)</label>
									<input type="password" name="password" placeholder="Skriv inn ønsket passord" class="form-control" />
								</div>

								<div class="form-group">
									<label>Bekreft passord</label>
									<input type="password" name="passwordr" placeholder="Gjenta ønsket passord" class="form-control" />
								</div>

								<div class="form-group bottom-group">
									<label>
										<input name="termsofservice" type="checkbox" /> Jeg har lest og godtatt 
										<a href="#" target="_blank">Vilkår</a> og 
										<a href="#" target="_blank">Personverns lover</a>.
									</label>
									<input type="hidden" name="reg_operator_token" value="<?php echo Token::create(); ?>">
									<button class="btn btn-success btn-block">BLI DRONEFOTOGRAF</button>
								</div>
							</div> <!-- end right half div -->
						</div><!-- end row div -->
					</form><!-- end kunde registering div -->
				</div>
			<?php } ?><!-- end if operatør -->
		</div>
	</div>

<?php include_once('includes/files/botarea.php'); ?>