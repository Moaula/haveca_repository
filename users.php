<?php 
	include_once('config/users-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>


	 <div class="container">
	 	<div class="inner-container">

			<!-- Flashing sessions with infromation -->
			<?php 
				if(Session::exists('users')) { 
					echo writealerts(Session::flash('users'), 'success'); 
				} 
			?>

	 		<div class="page-header">
	 			<h2><i class="fa fa-users"></i> Brukere</h2>
	 		</div>

	 		<?php if(!empty($users)) { ?>
		 		<table class="table table-striped table-hover ">
					<thead>
						<tr>
							<th>#ID</th>
							<th><i class="fa fa-user"></i> Navn</th>
							<th><i class="fa fa-envelope-o"></i> E-post</th>
							<th><i class="fa fa-map-marker"></i> Adresse</th>
							<th><i class="fa fa-ban"></i> Utestengt</th>
							<th><i class="fa fa-cogs"></i> Handling</th>
						</tr>
					</thead>
					<tbody>

						<?php foreach ($users as $user) { ?>
							<tr>
								<td><?php echo $user->UserID; ?></td>
								<td><?php echo ucfirst($user->FirstName), ' ', ucfirst($user->LastName), ' (', $user->BirthDay, ')'; ?></td>
								<td><?php echo ucfirst($user->EmailAddress); ?></td>
								<td><?php echo ucfirst($user->Address), ' ', ucfirst($user->ZipCode), ' ', ucfirst($user->ZipPlace); ?></td>
								<td><?php if($user->Active == 1) { echo "NEI"; } else { echo "JA"; } ?></td>
								<td>
									<?php if($user->UserID !== $_init_uzer_data->UserID) { ?>
										<?php if($user->Active == 1) { ?>
											<a href="/users/ban/<?php echo $user->UserID; ?>/" class="btn btn-xs btn-danger btn-block"><i class="fa fa-ban"></i> Bloker </a>
										<?php } else { ?>
											<a href="/users/open/<?php echo $user->UserID; ?>/" class="btn btn-xs btn-success btn-block"><i class="fa fa-key"></i> Åpne </a>
										<?php } ?>
									<?php } else { ?>
										<a href="#" class="btn btn-xs btn-success btn-block disabled"><i class="fa fa-user"></i> Ingen handling </a>
									<?php } ?>
								</td>
							</tr>
						<?php } ?>

					</tbody>
				</table> 
			<?php } else { ?>
				<div class="no-shows">
					<h3>Du har ikke lagt inn noen visninger enda. </h3>
				</div>
			<?php } ?>


	 	</div>
	 </div>
				



<?php 
	include_once('includes/files/botarea.php'); 
?>