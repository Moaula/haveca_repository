<?php 
	include_once('config/verify-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>
	<!-- Container for options panel -->
	<div class="container">
		<div class="inner-container white">
			
			<?php 
				if(IO::exists('get') && IO::get('action') == 'operator') { 
					if(!empty(IO::get('ue')) && !empty(IO::get('uc'))) {
			?>
			
			<div class="entry-title">aktivering av Konto - Dronefotograf</div>
			<div class="verify-content">
				
					<?php if(empty($errors)) { ?>
						
						<div class="verify_iconview">
							<i class="fa fa-check-circle-o green"></i>
						</div>					

						<div class="verify_content">
							For å aktivere <strong><?php echo IO::get('ue'); ?></strong>, 
							les nøye deretter trykk på <strong>bekreft og aktiver</strong>.
							<br /><br />

							Mitt firma oppfyller lovens krav om: 
							<br /><br />
							<ul>
								<li><a href="#" target="_blank" ><i class="fa fa-external-link"></i>  Merking av drone</a></li>
								<li><a href="#" target="_blank" ><i class="fa fa-external-link"></i>  Ansvarforsikring</a></li>
								<li><a href="#" target="_blank" ><i class="fa fa-external-link"></i>  Operatørtillatelse</a></li>
							</ul>

						</div>
						
						<label>
							<input class="confirm-verify-checkbox" name="cb" type="checkbox"> 
							Jeg har lest og forstått kravene.
						</label>

						<a href="/verify/operator/<?php echo IO::get('ue'); ?>/<?php echo IO::get('uc'); ?>/confirmed/" class="btn btn-success btn-block btn-lg disabled confirm-verify">
							Bekreft og aktiver
						</a>
					<?php } else { ?>
						<div class="verify_iconview">
							<i class="fa fa-times-circle-o red"></i>
						</div>	
						<br /><br />
						<?php echo writeerrors($errors); ?>

						<a href="/" class="btn btn-success btn-block btn-lg"><i class="fa fa-home"></i> Til hjemmesiden </a>
					<?php } ?>				
			</div>

			<?php 
					} 
				} 
			?>

			<?php 
				if(IO::exists('get') && IO::get('action') == 'kunde') { 
					if(!empty(IO::get('ue')) && !empty(IO::get('uc'))) {
			?>
			
			<div class="entry-title">aktivering av Konto - Kunde</div>
			<div class="verify-content">
				<?php if(empty($errors)) { ?>
					<center>
						<i class="fa fa-check-circle-o green"></i>
					</center>
					<br />

					Din konto med e-post adresse <strong><?php echo IO::get('ue'); ?></strong> er aktivert.
					<br /><br /><br /><br />
					<a href="/login" class="btn btn-primary btn-block btn-lg">Logg på</a>
				<?php } else { ?>
					<center>
						<i class="fa fa-times-circle-o red"></i>
					</center>
					<br />

					<?php echo writeerrors($errors); ?>
					<br />

					<a href="/" class="btn btn-primary btn-block btn-lg">Til hjemmesiden </a>
				<?php } ?>
			</div>

			<?php 
					} 
				} 
			?>
		</div>		
	</div>

<?php 
	include_once('includes/files/botarea.php'); 
?>