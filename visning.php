<?php 
	include_once('config/visning-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	<div class="container">
		<div class="inner-container ">

			<!-- Error outputting -->
			<?php 
				if(!empty($errors)) { 
					echo writeerrors($errors, '', 'En feil har oppstått.'); 
				} 
			?>

			<!-- Flashing sessions with infromation -->
			<?php 
				if(Session::exists('visning')) { 
					echo writealerts(Session::flash('visning'), 'success'); 
				} 
			?>


			<div class="visning-container">
				<div class="col-md-12">
					<div class="video-title">
						<h1><?php echo ucfirst($datahandler->Title); ?></h1>
					</div>
				</div>

				<div class="col-md-8 lefthand">

					<!-- 16:9 aspect ratio -->
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo gYTid($datahandler->Video); ?>"></iframe>
					</div>
					<div class="video-controls">
						 <div class="col-md-8 col-sm-8 col-xs-12 center"> <i class="fa fa-map-marker"></i> &nbsp; <?php echo ucfirst($datahandler->Address); ?> </div>	
						 <div class="col-md-4 col-sm-4 col-xs-12 center laste"><i class="fa fa-eye"></i> &nbsp; <?php echo $views; ?> </div>	
					</div>

					<div class="information-field">
						<div class="operator-name">
							Detaljer om visningen.
						</div>
						<ul>
							<?php if($datahandler->Price) { echo '<li>Pris: <span class="float-right">'. number_format($datahandler->Price, 0, ',', '.') .' kr</span></li>'; } ?>
							<?php if($datahandler->TotalPrice) { echo '<li>Total: <span class="float-right">'. number_format($datahandler->TotalPrice, 0, ',', '.') .' kr</span></li>'; } ?>
							<?php if($datahandler->BuildYear) { echo '<li>Byggeår: <span class="float-right">'. $datahandler->BuildYear.'</span></li>'; } ?>
							<?php if($datahandler->BuildingType) { echo '<li>Boligtype: <span class="float-right">'. $datahandler->BuildingType.'</span></li>'; } ?>
							<?php if($datahandler->Ownership) { echo '<li>Eierskap: <span class="float-right">'. $datahandler->Ownership.'</span></li>'; } ?>
							<?php if($datahandler->AppraisedValue) { echo '<li>Verditakst: <span class="float-right">'.  number_format($datahandler->AppraisedValue, 0, ',', '.') .' kr</span></li>'; } ?>
							<?php if($datahandler->CommonDept) { echo '<li>Fellesgjeld: <span class="float-right">'.  number_format($datahandler->CommonDept, 0, ',', '.') .' kr</span></li>'; } ?>
							<?php if($datahandler->CommonWealth) { echo '<li>Fellesformue: <span class="float-right">'.  number_format($datahandler->CommonWealth, 0, ',', '.') .' kr</span></li>'; } ?>
							<?php if($datahandler->SharedCosts) { echo '<li>Felleskostn..: <span class="float-right">'.  number_format($datahandler->SharedCosts, 0, ',', '.') .' kr</span></li>'; } ?>
							<?php if($datahandler->LoanValue) { echo '<li>Lånetakst: <span class="float-right">'.  number_format($datahandler->LoanValue, 0, ',', '.') .' kr</span></li>'; } ?>
							<?php if($datahandler->Area) { echo '<li>Areal: <span class="float-right">'.  number_format($datahandler->Area, 0, ',', '.') .'m&sup2;</span></li>'; } ?>
							<?php if($datahandler->AreaOfGross) { echo '<li>Bruttoareal: <span class="float-right">'.  number_format($datahandler->AreaOfGross, 0, ',', '.') .'m&sup2;</span></li>'; } ?>
							<?php if($datahandler->AreaOfUse) { echo '<li>Bruksareal: <span class="float-right">'.  number_format($datahandler->AreaOfUse, 0, ',', '.') .'m&sup2;</span></li>'; } ?>
							<?php if($datahandler->AreaPrimary) { echo '<li>Primaærrom: <span class="float-right">'.  number_format($datahandler->AreaPrimary, 0, ',', '.') .'m&sup2;</span></li>'; } ?>
							<?php if($datahandler->Plot) { echo '<li>Tomt: <span class="float-right">'.  number_format($datahandler->Plot, 0, ',', '.') .'m&sup2;</span></li>'; } ?>
							<?php if($datahandler->Roms) { echo '<li>Rom: <span class="float-right">'. $datahandler->Roms .'</span></li>'; } ?>
							<?php if($datahandler->BedRoms) { echo '<li>Soverom: <span class="float-right">'. $datahandler->BedRoms .'</span></li>'; } ?>
							<?php if($datahandler->BathRoms) { echo '<li>Bad: <span class="float-right">'. $datahandler->BathRoms .'</span></li>'; } ?>
						</ul>

						<div class="operator-name">
							Beskrivelse
						</div>
						<p>
							<?php echo nl2br(bbcode($datahandler->Description)); ?>
						</p>

					</div>





				</div>
				<div class="col-md-4">
					
					<?php if($datahandler->UserID) { 
							$userData = new User($datahandler->UserID);
							$userData = $userData->data();
					?>
					<div class="operator-name">
						AV: &nbsp; <i class="fa fa-user"></i> &nbsp; <a href="operators/show/<?php echo $datahandler->UserID; ?>"><?php echo ucfirst($userData->FirstName), ' ', ucfirst($userData->LastName); ?></a>
					</div>

					<?php } ?>
					<!-- 16:9 aspect ratio -->

					<div class="main-image righthand">
						<?php if(!empty($datahandler_images)) { ?>
							<?php foreach ($datahandler_images as $img) {  if($img->Prim == 1) { ?>
								<a href="res/uploads/visning/<?php echo $img->Path; ?>" class="lb" title="Klikk for å se bilde større">
									<img src="res/uploads/visning/t_<?php echo $img->Path; ?>" class="img_over" alt="Bilde Mangler" />
								</a>
							<?php } } ?>
						<?php } ?>
						
					</div>
					<div class="visning-images">
						<?php if(!empty($datahandler_images)) { ?>
							<?php foreach ($datahandler_images as $img) {  if($img->Prim != 1) { ?>
								<a href="res/uploads/visning/<?php echo $img->Path; ?>" class="lb" title="Klikk for å se bilde større">
									<img src="res/uploads/visning/t_<?php echo $img->Path; ?>" class="img_over imgitem" alt="Bilde Mangler">
								</a>
							<?php } } ?>
						<?php } ?>
						
					</div>

					<?php if($datahandler->Type == "sale") { ?>
						<?php if($datahandler->CustomerID) { 
								$userData = new User($datahandler->CustomerID);
								$userData = $userData->data();
						?>
							<div class="contact-information">
								<div class="operator-name">
									Kontaktinformasjon:
								</div>
								<ul>
									<li><i class="fa fa-user"></i> &nbsp; <?php echo ucfirst($userData->FirstName), ' ', ucfirst($userData->LastName); ?></li>
									<li><i class="fa fa-at"></i> &nbsp; <?php echo ucfirst($userData->EmailAddress); ?></li>
									<li><i class="fa fa-phone"></i> &nbsp; <?php echo ($userData->PhoneNumber); ?></li>
								</ul>
							</div>
						<?php } ?>
					<?php } ?>

					<?php if($_init_uzer->isOnline() && $datahandler->UserID == $_init_uzer_data->UserID || $_init_uzer->isOnline() && $_init_uzer->hasPerm('operator')) { ?>

						<div class="contact-information">
							<div class="operator-name">
								Kontrollpanel valg
							</div>
							<ul>
								<li>
									<a href="annonse/edit/<?php echo $datahandler->AnnonseID; ?>/"><i class="fa fa-pencil"></i> &nbsp; Endre</a> 
									&nbsp; | &nbsp; 
									<a onclick="return confirm('Er du sikker du vil slette denne visningen ?');" href="annonse/delete/<?php echo $datahandler->AnnonseID; ?>/" class="red" onclick="return confirm('Er du sikker du vil slette denne visningen ?');">
										<i class="fa fa-trash"></i> &nbsp; Slett
									</a>
								</li>
							</ul>
						</div>


					<?php } ?>


					<div class="google-maps">
						<iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDqAdy55WnshlFRwMhA1YS3cI7tjqbrI7Y&q=<?php echo $datahandler->GoogleMaps; ?>" allowfullscreen></iframe>
					</div>		

				</div>		

			</div>
			


		</div>
	</div>



<?php 
	include_once('includes/files/botarea.php'); 
?>