<?php 
	include_once('config/visninger-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	<section>
		<!-- Flashing sessions with infromation -->
		<?php 
			if(Session::exists('index')) {
				if(IO::exists('get')) { $alert = IO::get('alert'); } else { $alert = 'success'; }
				echo writealerts(Session::flash('index'), $alert); 
			} 
		?>
	</section>

	<section class="search-bar">
		<div class="container">
			<div class="medium-form form-center">
				<form action="search" method="">
					<div class="col-md-10 col-sm-10 col-xs-10 nopadding">
						<input type="text" name="search" placeholder="SØK MED POSTNR, STED, NAVN ELLER INNHOLD I VISNINGER" class="form-control search-bar-bar">
					</div>
					<div class="col-md-2 col-sm-2 col-xs-2 nopadding">
						<button class="btn btn-success btn-lg btn-block"><i class="fa fa-search"></i> &nbsp; Søk</button>
					</div>
				</form>
			</div>
		</div>
	</section>

	<?php if(!empty($visninger)) { ?>
		<section class="visninger">
			<div class="container">
				<div class="page-header">
					<h1>Ferske visninger</h1>
				</div>
				
				<div class="row">
					<?php foreach ($visninger as $visning) { ?>
						<?php  
							$images = new Get();
							$images->get_data_by_id("images", "AnnonseID", $visning->AnnonseID);
							$images = $images->results();
						?>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="visning-block">
								<a href="/kampanje/vis/<?php echo $visning->AnnonseID; ?>/" class="block-link">
									<div class="visning-bilde">
										<?php foreach ($images as $img) {  ?>
											<?php if($img->Prim == 1) { ?>
												<img src="res/uploads/visning/t_<?php echo $img->Path; ?>">
											<?php } ?>
										<?php } ?>
										<div class="play-button"><i class="fa fa-play"></i> </div>
									</div>
									<div class="visning-title">
										<h3><?php echo $visning->Title; ?></h3>
										<p><?php echo nl2br(bbcode(substr($visning->Description, 0, 140))); ?>...</p>
									</div>
								</a>
								<div class="visning-controls">
									<a href="/kampanje/vis/<?php echo $visning->AnnonseID; ?>/" class="btn btn-success btn-block"><i class="fa fa-eye"></i> &nbsp; Detaljer </a>
								</div>
							</div>
						</div>
					<?php } ?>

				</div>

			</div>
		</section>
	<?php } else { ?>
		<section>
			<div class="container">
				<h1 class="information_empty_site">Visninger ser ut til å være tomt.</h1>
			</div>
		</section>
	<?php } ?>
<?php 
	include_once('includes/files/botarea.php'); 
?>